
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'New User') ?>

<?php  $view['slots']->start('adduser') ?>
              
<?php  
//echo "<pre>";
// var_dump( $form['username']->vars['errors']);
//echo "</pre>";
//die();
?>
<?php echo $view['form']->start($form) ?>

    <div>
        <?php echo $view['form']->label($form['username']) ?>
        
        <?php foreach ($form['username']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['username']) ?>
    </div>

    <div>
        <?php echo $view['form']->label($form['username_canonical']) ?>
        
        <?php foreach ($form['username_canonical']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['username_canonical']) ?>
    </div>


    <div>
        <?php echo $view['form']->label($form['email']) ?>
        
        <?php foreach ($form['email']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['email']) ?>
    </div>

    <div>
        <?php echo $view['form']->label($form['email_canonical']) ?>
        
        <?php foreach ($form['email_canonical']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['email_canonical']) ?>
    </div>


    <div>
        <?php echo $view['form']->label($form['givenname']) ?>
        
        <?php foreach ($form['givenname']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['givenname']) ?>
    </div>

    <div>
        <?php echo $view['form']->label($form['surname']) ?>
        <?php foreach ($form['surname']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        <?php echo $view['form']->widget($form['surname']) ?>
    </div>

    <div>
        <?php echo $view['form']->widget($form['save']) ?>
    </div>

<?php echo $view['form']->end($form) ?>

<?php $view['slots']->stop() ?>
