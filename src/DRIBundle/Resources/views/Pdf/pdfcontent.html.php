<style>
	h2 {
		font-family: Arial;
		color: #165ba8;
		text-align: center;
		font-size: 16px;
	}
	.table {
		border: none !important; 
		padding: 20px 30x 0px 0px;
		width: 700px;
	}
        
        td{
              border: none !important; 
              font-family: Arial;
	      font-size: 10px;
	     // width: 25%;
        }
        
        tr{
           border: none !important; 
        }
        
        .thc {
             border:1px solid black;
             border-collapse:separate;
             text-align: center;
             width: 20%; 
             font-size: 9px;}
        .rowsc{
            border:1px solid black;
            border-collapse:separate;
            width: 20%; 
            font-size: 8px;
        }
</style>
 <?php //var_dump($data);die();?>

<?php if(count($data)):?>
<table style="width:535px">
	<tr >
		<td style="width: 20%;
                           border:none;
                           font-size: 8px;">
                    
               <?php if(isset($societe[0]['societe'])):?>
                    <p>Société : <?php echo $societe[0]['societe'];?></p>
               <?php endif;?>
                    
                    <p>Date départ: <?php 
                    if($data['restitutiondate'])
                       //echo $data['restitutiondate'];
                       echo date("d-m-Y", strtotime($data['restitutiondate']));
                    else
                       echo date("d-m-Y");
                    
                    ?></p>
		</td>
                <td style="width: 16%;">

		</td>
                
		<td style="width: 28%;
                           vertical-align: middle;
                           border:none;
			">
                   <?php //var_dump($data);die();?>
                          
                        <img src="<?php echo 'bundles/dri/images/logo_noz.png';?>" 
                             alt="" style="border:none; padding: 20px;width:134px;height:77px;"/>
                                 
			<h2><?php echo "Fiche de retour"; ?></h2>
                   
		</td>
                
                <td style="width: 16%;">

		</td>
		<td style="width: 20%; 
                           padding-left: 10px; 
                           font-size: 8px;
                           border:none;
                           ">

			  <p><?php echo "Le ".date("d-m-Y");?></p>
					
		</td>
	</tr>
        <tr>
            <?php 
            $nom = isset($data['nom'][0])?$data['nom'][0]:"";
            $prenom = isset($data['prenom'][0])?$data['prenom'][0]:"";
            ?>
            <td style="width: 100%;">
               <p> A l'attention de : <?php echo  $nom ." ".$prenom;?> </p>
            </td>         
        </tr>
        <tr>
            <td style="width: 80%;">
             <p>
            Dans le cadre de vos fonctions, la société met à votre disposition le matériel ci-dessous restant la seule propriété de 
            l'entreprise, ayant les références suivantes :   
             </p>
           </td>
        </tr>
</table>

<br/> <br/>

<table style="width:500px;padding: 5px; ">
        <tr style="page-break-inside:avoid; 
                   page-break-after:auto;">
            <th class="thc"><strong>Statut</strong></th>
            <th class="thc"><strong>Code barre</strong></th>
            <th class="thc"><strong>Fabriquant</strong></th>
            <th class="thc"><strong>Numéro de serie</strong></th>
            <th class="thc"><strong>Désignation</strong></th>
            
        </tr>
        <?php for($i=0; $i<count($data['manufacturer']); $i++):?>
        <tr style="page-break-inside:avoid; 
                   page-break-after:auto;">
            <td class="rowsc" >
               <?php 
                $statut = isset($data['statut'][$i])?$data['statut'][$i]:"";
                
                if($statut == "En fonction"){

                   echo   "<span>";   
                    echo $statut;
                   echo  "</span>";

                }else                                
                   echo  $statut; 
               ?>
            </td>
            <td class="rowsc">
                <?php echo $codebarre = isset($data['codebarre'][$i])?$data['codebarre'][$i]:"";?>
            </td>
            <td class="rowsc">
                <?php echo $manufacturer = isset($data['manufacturer'][$i])?$data['manufacturer'][$i]:"";?>
            </td>
            <td class="rowsc">
                <?php echo $numserie = isset($data['numserie'][$i])?$data['numserie'][$i]:"";?>
            </td>           
            <td class="rowsc">
                <?php echo $designation = isset($data['designation'][$i])?$data['designation'][$i]:"";?>
            </td>

        </tr>
       <?php endfor;?>
</table>


<br/><br/>
<table class="table">
	<tr >
            <td style="width:100%;border:none;"><strong><?php echo "Je déclare avoir rendu le matériel décrit ci-dessus."; ?></strong></td>

	</tr>
        <tr >
	    <td style="width:100%;border:none;"><?php echo "En deux exemplaires, remis au service informatique et à l'utilisateur."; ?></td>

	</tr>
        <tr >
	     <td style="width:100%;border:none;"><?php echo "Faire précéder la signature de la mention manuscrite  ' LU ET APPROUVE, LE '."; ?></td>

	</tr>
</table>

<?php endif;?>


