<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use ChecklistBundle\Entity\PointDeControle;
use ChecklistBundle\Entity\PointDeControleRepository;

class ChecklistType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('nom', 'text', array(
					'required'  => true,
					'constraints' => array(
							new NotBlank(array('message' => 'Le nom ne peut pas être vide!'))
					)
			))
			->add(
					'pointsDeControle',
					'entity',
					array(
							'class' => 'ChecklistBundle\Entity\PointDeControle',
							'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
								return $er->createQueryBuilder('c')
								->where("c.desactive = FALSE");
							},
							'property' => 'nom',
							'multiple' => true,
							'expanded' => false,
							'required' => false,
							'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
								return $er->createQueryBuilder('p')
								->where("p.desactive = false");
							},
							'label' => 'Points de contrôle associés',
							'attr' => array('class' => 'chosen-select duree-update', 'data-placeholder' => 'multiples checkpoints', 'style' => 'width:350px;'),
					)
			)
			->add('save', 'submit', array(
					'label' => 'Enregistrer',
					'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'ChecklistBundle\Entity\Checklist'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'checklist_checklist';
	}
}