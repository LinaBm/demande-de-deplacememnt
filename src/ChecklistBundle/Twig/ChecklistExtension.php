<?php

namespace ChecklistBundle\Twig;

class ChecklistExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('activeMenu', array($this, 'activeMenuFilter')),
        );
    }

    public function activeMenuFilter($item)
    {
		$response = '';
		if (strpos($_SERVER['REQUEST_URI'], $item) !== false) {
			$response = 'active';
		}
        return $response;
    }

    public function getName()
    {
        return 'checklist_extension';
    }
}