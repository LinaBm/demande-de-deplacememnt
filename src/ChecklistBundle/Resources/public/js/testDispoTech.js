$(document).ready(function() {
	$('.formNouvelleVisite').submit(function() {
		var technicien = $('.select-technicien option:selected').val();
		var date = $('.date').val();
		$.ajax({
			dataType: "json",
	        type: "POST",
	  		url: url_verif_dispo,
	        data: { technicien : technicien, date: date},  
	        async:false,
	        cache: false,
	        error: function(jqXHR,textStatus, errorThrown ) {
	        	alert(jqXHR.responseText); return false;
	        },
	        success: function(visite){
	            if (visite == 'pas de visite prevue') {
	            	this.submit();
	            }
	            else {
	            	var reponse = confirm("Ce technicien a déja une visite de prévu à " + visite + " ce jour là. Voulez vous qu\'il fasse plusieurs visites?");
	            	if (reponse){
	            		this.submit();
	            	}
	            }    	
	        }                                                                            
		});
		return false;
	});
});