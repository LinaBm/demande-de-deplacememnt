<?php

namespace ChecklistBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChecklistBundle\Entity\Resultat;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use \DateTime;
use ChecklistBundle\Form\TerminerVisiteType;
use ChecklistBundle\Form\CommentaireVisiteType;
use ChecklistBundle\Form\ResultatType;
use ChecklistBundle\Form\ResultatsType;


class PlanningController extends Controller {

	/**
	 * @Route("/planning", name="checklist_planning_planning", )
	 */
	public function planningAction() {	
		$user=$this->getUser();
		if ($user->isGranted("ROLE_CHK_ADMIN")) {
			$listeInfos = $this->getDoctrine()
				->getRepository('ChecklistBundle:Visites')
				->getListeVisitesManager();			
		} else if ($user->isGranted("ROLE_CHK_TECH")) {
			$listeInfos = $this->getDoctrine()
				->getRepository('ChecklistBundle:Visites')
				->getListeVisitesTechnicien($this->getUser()->getId());
		}
		$listeVisites = array();
		$i = -1;
		foreach ($listeInfos as $info) {
			$gotVisite = $gotChecklist = $gotMagasin = false;
			if (get_class($info) == 'ChecklistBundle\Entity\Visites') {
				$i++;
				$listeVisites[$i] = array('visite' => $info);
				$gotVisite = true;
			} else if (get_class($info) == 'Proxies\__CG__\ChecklistBundle\Entity\Checklist') {
				$listeVisites[$i]['leChecklist'] = array('checklist' => $info);
				$tempChecklist = clone $info;
				$gotChecklist = true;
			} else if (get_class($info) == 'Proxies\__CG__\ChecklistBundle\Entity\Magasin') {
				$listeVisites[$i]['leMagasin'] = array('magasin' => $info);
				$tempMagasin = clone $info;
				$gotMagasin = true;
			}
			if (!$gotMagasin && isset($tempMagasin)) {
				$listeVisites[$i]['leMagasin'] = array('magasin' => $tempMagasin);
			}
			if (!$gotChecklist && isset($tempChecklist)) {
				$listeVisites[$i]['leChecklist'] = array('checklist' => $tempChecklist);
			}
		}			
		return $this->render('ChecklistBundle:planning:planning.html.twig', array('visites' => $listeVisites));
	}

	/**
	 * @Route("/planning/fiche-visite/{id}", name="checklist_planning_fiche_visite")
	 */
	public function ficheVisiteAction($id, Request $request) {
		if (count($this->get('session')->getFlashBag()->get('terminerVisiteModal',array()))){
			$duTerminer = true;
		} else {
			$duTerminer = false;
		}
		$visite = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->find($id);
		if (!$visite) {
			throw $this->createNotFoundException(
				'Trouvée aucune visite pour ' . $id
			);
		}		
		if($this->getUser()->getId() !== $visite->getTechnicien() && !$this->getUser()->isGranted("ROLE_CHK_ADMIN") ){
			return $this->redirect($this->generateUrl('checklist_homepage'));
		}
		$checklist = $visite->getChecklists();
		$pointsDeControle = $checklist->getPointsDeControle();
		$today = new DateTime(date('Y-m-d'));
		$formModal = $this->createForm(new TerminerVisiteType(), $visite);
		if ($visite->getDate() == $today && $visite->getCommencer() && !$visite->getTerminer()) {
			$formModal->handleRequest($request);
			$em = $this->getDoctrine()->getManager();			
			if ($formModal->isValid()) {
				$visite->setHeureDeDepart(new DateTime('now'));
				$verifieeTemspDeInteruption = $this->getDoctrine()
					->getRepository('ChecklistBundle:Visites')
					->verifieeTempsDeInteruption($visite, $formModal['tempsDeInteruption']->getData());
				if (!$verifieeTemspDeInteruption) {
					$message = 'Le temps de pause ne peut pas être plus long que tout le temps!';
					$this->get('session')->getFlashBag()->add('terminerVisite', $message);
					$this->get('session')->getFlashBag()->add('terminerVisiteModal', 'vrais');
					return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $id)));
				}
				$visite->setTerminer(1);
				$reelDuree = new DateTime(date('Y-m-d 00:00:00'));
				$heureDeArivee = $visite->getHeureDeArivee();
				$reelDuree->add($heureDeArivee->diff(new DateTime('now')));
				$tempsDeInteruption = new DateTime(date('1970-01-01 00:00:00'));
				$toSubstract = $formModal["tempsDeInteruption"]->getData()->diff($tempsDeInteruption);
				$reelDuree->add($toSubstract);
				$em->persist($visite);
				$em->flush();
				//on fait la liste des points de controle non renseignés (aucun résultats dans la base) et non valides (il y a des résultats, mais mauvais
				$pointsNonTraites = array();
				$pointsNonValides = array();
				foreach ($visite->getChecklists()->getPointsDeControle() as $pointdecontrole){
					//on récupère les résultat de chaque point de visite
					$listeResultats = $this->getDoctrine()
						->getRepository('ChecklistBundle:Resultat')
						->findBy(array(	'visites' => $id,'pointsDeControle' => $pointdecontrole->getId()));
					if (! $listeResultats){
						$pointsNonTraites[]=$pointdecontrole->getNom();
					} else {
						$pointDejaReleve=false;
						foreach($listeResultats as $resultat){
							if (!$resultat->getValide() and !$pointDejaReleve){
								$pointsNonValides[]=$pointdecontrole->getNom();
								$pointDejaReleve=true;
							}
						}
					}
				}
				

				$url = $this->container->getParameter('url_serveur');
				if ($this->container->get('kernel')->getEnvironment()!='prod'){
					$titre= '[TEST] ';
				} else {
					$titre= '';
				}
				if ($pointsNonTraites or $pointsNonValides){
					$titre= $titre."anomalie dans le rapport de ".$visite->getTechnicien()->getDisplayName();
				} else {
					$titre= $titre."rapport de ".$visite->getTechnicien()->getDisplayName()." recu.";
				}
				$message = \Swift_Message::newInstance()
				->setContentType('text/html')
				->setSubject($titre)
				->setFrom($visite->getTechnicien()->getEmail())
				->setTo(explode(" ", $this->container->getParameter('mailManager')))
				->setCC('poleweb@noz.fr')
				->setBody($this->renderView('ChecklistBundle:email:finVisite.html.twig', array(
						'visite' => $visite,
						'url' => $url,
						'listePointNonTraites' => $pointsNonTraites,
						'listePointNonValides' => $pointsNonValides
				)));
				$reponse=$this->get('mailer')->send($message);
				$messageSuccess = 'La visite a ete finalisee!';
				$this->get('session')->getFlashBag()->add('demareeVisite', $messageSuccess);
				
				return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $id)));
			}
		}
		$formModal = $formModal->createView();
		if ($visite->getCommencer() && !$visite->getTerminer()){
			$form = $this->createForm(new CommentaireVisiteType(), $visite);
			$form->handleRequest($request);
			$em = $this->getDoctrine()->getManager();
			$em->persist($visite);
			$em->flush();
			$form = $form->createView();			
		} else {
			$form = null;
		}
		$reinsegnerArray = array();
		foreach($pointsDeControle as $point) {
			$ajoutOuModifie = $this->getDoctrine()
				->getRepository('ChecklistBundle:Resultat')
				->findBy(
					array(
						'visites' => $id,
						'pointsDeControle' => $point->getId()
					), 
					array('id'=>'ASC')				
				);
			$results = count($ajoutOuModifie);	
			$valide = $this->getDoctrine()
				->getRepository('ChecklistBundle:Resultat')
				->findBy(
					array(
						'visites' => $id,
						'pointsDeControle' => $point->getId(),
						'valide' => 1
					), 
					array('id'=>'ASC')				
				);
			$valides = count($valide);			
			$reinsegnerArray[$point->getId()] = array('resultats' => $results, 'valide' => $valides) ;
		}		
		$notVerifieeValide = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->verifieeValide($visite);
		$notVerifieeValide = count($notVerifieeValide);
		return $this->render('ChecklistBundle:planning:ficheVisite.html.twig', array(
			'today' => $today,
			'encore' => $duTerminer,
			'formModal' => $formModal,
			'notVerifieeValide' => $notVerifieeValide,
			'form' => $form,
			'visite' => $visite,
			'checklist' => $checklist,
			'pointsDeControle' => $pointsDeControle,
			'title' => 'Visite ' . $visite->getId(),
			'reinsegnerArray' => $reinsegnerArray
		));
	}

	/**
	 * @Route("/fiche-visite/modifier/{id}", name="checklist_planning_modifier_visite")
	 */
	public function modifierVisiteAction($id) {
		$visite = $this->getDoctrine()
				->getRepository('ChecklistBundle:Visites')
				->find($id);
		if (!$visite) {
			throw $this->createNotFoundException(
				'Trouvée aucune visite pour ' . $id
			);
		}	
		if($this->getUser()->getId() !== $visite->getTechnicien() && !$this->getUser()->isGranted("ROLE_CHK_ADMIN") ){
			return $this->redirect($this->generateUrl('checklist_homepage'));
		}
		$em = $this->getDoctrine()->getManager();
		if (!$visite->getCommencer() && !$visite->getTerminer()) {
			$visite->setCommencer(1);
			$visite->setHeureDeArivee(new DateTime('now'));
			$message = 'La visite vient de démarrer';
			$this->get('session')->getFlashBag()->add('demareeVisite', $message);
		}
		$em->persist($visite);
		$em->flush();
		return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $id)));
	}

	/**
	 * @Route("planning/fiche-point-de-controle/{id}/{idFicheVisite}", name="checklist_planning_fiche_point_de_controle", )
	 */
	public function fichePointDeControleAction(Request $request, $id, $idFicheVisite) {
		$visite = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->find($idFicheVisite);
		if (!$visite) {
			throw $this->createNotFoundException(
				'Trouvé aucune visite pour id '.$idFicheVisite
			);
		}	
		if($this->getUser()->getId() !== $visite->getTechnicien() && !$this->getUser()->isGranted("ROLE_CHK_ADMIN") ){
			return $this->redirect($this->generateUrl('checklist_homepage'));
		}
		$pointDeControle = $this->getDoctrine()
			->getRepository('ChecklistBundle:PointDeControle')
			->find($id);
		if (!$pointDeControle) {
			throw $this->createNotFoundException(
				'Trouvé aucun point de controle pour id '.$id
			);
		}
		$checklist = $visite->getChecklists();
		$pointsDeControle = $checklist->getPointsDeControle();
		if(!$pointsDeControle->contains($pointDeControle)){
			throw $this->createNotFoundException(
				'Ce point de controle n\'est pas dans le checklist de cette visite'
			);
		}
		$magasin = $visite->getMagasin();
		//on calcule le nombre de formulaire à afficher :
		// le nombre de caisse du magasin s'il concerne les caisses
		//sinon un seul.
		if ($pointDeControle->getCaisses()){
			$nbform =$visite->getMagasin()->getNombredeCaisses();
		} else {
			$nbform=1;
		}		
		$ajoutOuModifie = $this->getDoctrine()
			->getRepository('ChecklistBundle:Resultat')
			->findBy(
				array(
					'visites' => $idFicheVisite,
					'pointsDeControle' => $id
				),
				array('id'=>'ASC')				
			);
		$results = count($ajoutOuModifie);
		if($results !== 0) {
			$nbform = $results;
			$forms=$this->createForm(new ResultatsType(), $ajoutOuModifie);			
		} else {
			$tabResult=array();
			$tabResult['noResults'] = $nbform;
			for ($i=0;$i<$nbform;$i++){
				$uploads =  $this->get('kernel')->getRootDir().'/../web/uploads/';
				$resultat=new Resultat();
				$resultat->setPointsDeControle($pointDeControle);
				$resultat->setVisites($visite);				
				$tabResult[]= $resultat;
			}					
			$forms=$this->createForm(new ResultatsType(), $tabResult);
		}
		$forms->handleRequest($request);
		
		//initialisation : pas d'envoie de mail
		$booEnvoyerMail=false;
		
		
		if ($forms->isValid()) {
			for ($i=0;$i<$nbform;$i++){
				$resultat= $forms["resultat_".$i]->getData();
				if ($results === 0) {
					$resultat->setPointsDeControle($pointDeControle);
					$resultat->setVisites($visite);
					$resultat->setDate(new DateTime('now'));
				}
				$dir = $this->get('kernel')->getRootDir().'/../web/uploads/attachments/checklist/'.$visite->getId().'/'.$pointDeControle->getId().'/';
				$fichier=$forms["resultat_".$i]['fichierUn']->getData();
				if (isset($fichier) && is_object($fichier) && $fichier->isValid() === TRUE) {	
					$origLongName = $fichier->getClientOriginalName();
					$origExtension = $fichier->getClientOriginalExtension();
					$origShortName = substr($origLongName, 0, strlen($origLongName)-strlen($origExtension)-1);
					$finalFileName = $origShortName.rand(1, 99999).'.'.$origExtension;
					$fichier->move($finalFileName);
					$resultat->setFichierUn($finalFileName);
				}
				if (isset($forms["resultat_".$i]['fichierDeux']) &&
					is_object($forms["resultat_".$i]['fichierDeux']->getData()) &&
					$forms["resultat_".$i]['fichierDeux']->getData()->isValid() === TRUE) {
					$origLongNameDeux = $fichierDeux->getClientOriginalName();
					$origExtensionDeux = $fichierDeux->getClientOriginalExtension();
					$origShortName = substr($origLongNameDeux, 0, strlen($origLongNameDeux)-strlen($origExtensionDeux)-1);
					$finalFileNameDeux = $origShortName.rand(1, 99999).'.'.$origExtensionDeux;
					$fichier->move($dir, $finalFileNameDeux);
					$resultat->setFichierDeux($finalFileNameDeux);
				}				
				if($forms->get('finaliser')->isClicked()){					
					$resultat->setValide(1);
					if(!$resultat->verification()){ //on envoie un mail si se sont les resultats finaux et que la réponse n'est pas valide
						$booEnvoyerMail=true;										
//					$this->get('mailer')->send($message);
					}
				}
				$resultat->setEmail($forms['email']->getData());
				$em = $this->getDoctrine()->getManager();
				$em->persist($resultat);
				$em->flush();
			}
			if($booEnvoyerMail || $forms['email']->getData() ){
				$rapport = $this->getDoctrine()
				->getRepository('ChecklistBundle:Resultat')
				->findBy(
						array(
								'visites' => $idFicheVisite,
								'pointsDeControle' => $id
						),
						array('id'=>'ASC')
				);
				if ($this->container->get('kernel')->getEnvironment()!='prod'){
					$titre= '[TEST] ';
				} else {
					$titre= '';
				}
				$message = \Swift_Message::newInstance()
					->setContentType('text/html')
					->setSubject($titre.$visite->getTechnicien()->getDisplayName() . ' - ' . $pointDeControle->getNom() )
					->setFrom($visite->getTechnicien()->getEmail())
					->setTo(explode(" ", $this->container->getParameter('mailManager')))
					->setCC('poleweb@noz.fr')
					->setBody($this->renderView('ChecklistBundle:email:rapport.html.twig', array(
							'rapport' => $rapport,
                )));
							
				$this->get('mailer')->send($message);
			}
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('demareeVisite', $message);
			return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
		}
		return $this->render('ChecklistBundle:planning:resultat.html.twig', array(
				'pointdecontrole' => $pointDeControle,
				'forms' => $forms->createView(),
				'nbResultat' => $nbform,
				'title' => 'Visite ' . $idFicheVisite . ' - Point de contrôle ' . $id,
		));
	}
	
	/**
	 * @Route("planning/unvalider-point-de-controle/{id}/{idFicheVisite}", name="checklist_planning_unvalider_point_de_controle", )
	 */
	public function unvaliderPointDeControleAction(Request $request, $id, $idFicheVisite) {
		$visite = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->find($idFicheVisite);
		if (!$visite) {
			throw $this->createNotFoundException(
				'Trouvé aucune visite pour id '.$idFicheVisite
			);
		}	
		if($visite->getTerminer() == 1){
			$message = 'Il faut Redemarrer la visite!';
			$this->get('session')->getFlashBag()->add('demareeVisite', $message);
			return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
		}
		$pointDeControle = $this->getDoctrine()
			->getRepository('ChecklistBundle:PointDeControle')
			->find($id);
		if (!$pointDeControle) {
			throw $this->createNotFoundException(
				'Trouvé aucun point de controle pour id '.$id
			);
		}
		$checklist = $visite->getChecklists();
		$pointsDeControle = $checklist->getPointsDeControle();
		if(!$pointsDeControle->contains($pointDeControle)){
			throw $this->createNotFoundException(
				'Ce point de controle n\'est pas dans le checklist de cette visite'
			);
		}
		$resultats = $this->getDoctrine()
			->getRepository('ChecklistBundle:Resultat')
			->findBy(
				array(
					'visites' => $idFicheVisite,
					'pointsDeControle' => $id
				),
				array('id'=>'ASC')
			);	
		$em = $this->getDoctrine()->getManager();
		foreach($resultats as $resultat){		
			if($resultat->getValide() == 0){
				$message = 'Le resultat n\'est pas validé!';
				$this->get('session')->getFlashBag()->add('demareeVisite', $message);
				return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
			}	
			$resultat->setValide(0);
			$em->persist($resultat);
			$em->flush();			
		}
		$message = 'L’opération s’est bien déroulée!';
		$this->get('session')->getFlashBag()->add('demareeVisite', $message);
		return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
	}
	
	/**
	 * @Route("planning/redemarrer-visite/{idFicheVisite}", name="checklist_planning_redemarrer_visite", )
	 */
	public function redemarrerVisite(Request $request, $idFicheVisite) {
		$visite = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->find($idFicheVisite);
		if (!$visite) {
			throw $this->createNotFoundException(
				'Trouvé aucune visite pour id '.$idFicheVisite
			);
		}
		if($visite->getTerminer() == 0){
			$message = 'La visite n\'est pas terminé!';
			$this->get('session')->getFlashBag()->add('demareeVisite', $message);
			return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
		}
		$em = $this->getDoctrine()->getManager();
		$visite->setTerminer(0);
		$em->persist($visite);
		$em->flush();	
		$message = 'L’opération s’est bien déroulée!';
		$this->get('session')->getFlashBag()->add('demareeVisite', $message);
		return $this->redirect($this->generateUrl('checklist_planning_fiche_visite', array('id' => $idFicheVisite)));
	}
	
	/**
	 * @Route("planning/visites-jour", name="checklist_planning_visite", )
	 */
	public function visitesDuJour() {
		$visites = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->findBy(array('date' => new DateTime(date('Y-m-d 00:00:00'))));
		return  $this->render('ChecklistBundle:planning:visite.html.twig', array(
				'visites' => $visites,
		));
	}
	
}

