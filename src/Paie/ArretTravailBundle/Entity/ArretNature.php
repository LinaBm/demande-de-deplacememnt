<?php
namespace Paie\ArretTravailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArretTravail
 *
 * @ORM\Entity()
 * @ORM\Table(name="pai_arrets_nature"))
 * @ORM\HasLifecycleCallbacks()
 */
class ArretNature
{

	const AT        = 2;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
     * @ORM\Column(name="libelle", type="string", length=25, nullable=true)
     **/
    private $libelle;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /* fonction ne servant à rien, mais j'ai besoin d'une fonction setId */
    /**
     * Set id
     *
     * @param string $id
     * @return ArretTravail
     */
    public function setId($id)
    {
    }
    
    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
    	return $this->libelle;
    }
    
    public function __toString() {
    	return $this->libelle;
    }
    
}