<?php

namespace Paie\ArretTravailBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ModificationArretRepository extends EntityRepository
{
	public function getModificationsParArret($arret)
	{
		$qb = $this->createQueryBuilder('m')
			->andWhere('m.arret = :arret')
    		->setParameter('arret', $arret->getId())
			->orderBy('m.date', 'DESC');
		return $qb->getQuery()->execute();
                
              /* $qb = $this->createQueryBuilder('m')
                ->select('COALESCE(SUM(d.chargesCPRestante)/8, 0) AS chargesCPRest')
                ->addSelect('COALESCE(SUM(d.chargesDevRestante)/8, 0) AS chargesDevRest')
                ->innerJoin('d.application', 'a',  'd.application = a.id') //Jointure sur les applications
                ->addSelect('a.id')
                ->addSelect('a.name')
                ->groupBy('d.application');
        return $qb ->getQuery()->getResult();*/
	}
}