<?php

namespace Paie\ArretTravailBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ob\HighchartsBundle\Highcharts\Highchart;
use \DateTime;
use Paie\ArretTravailBundle\Entity\ArretTravail;
use Paie\ArretTravailBundle\Form\ArretTravailType;
use Paie\ArretTravailBundle\Form\TraiterArretType;
use Paie\ArretTravailBundle\Form\PiecesJointesType;

class TestController extends Controller {

	/**
	 * @Route("/test/ajoutarret", name="test_ajout_arret")
	 * @Template()
	 */
	public function addArretAction() {
		$em = $this->getDoctrine()->getManager();
		$request = $this->container->get('request');
		$session = $this->getRequest()->getSession();
		$idArret='0';		
		// for test by using the route directly
		$today=new DateTime();
		$contrats = $em->createQuery(
				'SELECT c,s,u
							FROM AdministrationSocieteBundle:PandoreContrat c
							JOIN c.societe s
							JOIN c.user u
							WHERE (c.dateSortie = :p1 and c.typeContrat=:p2)
							OR (c.dateFin = :p3 and c.typeContrat=:p4)'
		)
			->setParameter('p1', '')
			->setParameter('p2', 'CDI')
			->setParameter('p3', $today->format('Ymd'))
			->setParameter('p4', 'CDD')
			->getResult();
		$utilisateurs = array();
		foreach ($contrats as $contrat) {
			$utilisateurs[$contrat->getId()] = $contrat->getUser()->getNom() . ' ' . $contrat->getUser()->getPrenom() . ' - ' . $contrat->getUser()->getNumeroSecu() . ' - ' . $contrat->getSociete()->getId() . ' ' . $contrat->getSociete()->getRaison();
		}		
		$arret = new ArretTravail();
		$form = $this->createForm(new AddArretTravailType($utilisateurs, $em), $arret);
		$formModal = null;
		$formPiecesJointes = null;
		$listeVieillesPJ = array();
		$extensions = array();
		$errors = 0;	
		$form->handleRequest($request);
		$validationModal = 0;		
		$selected = $idArret;
		var_dump($form);
		if ($form->isValid()) {				
			if ($idArret === '0' || $idArret === '') {
				$arret->setDateTraitement(new DateTime('1970-01-01 00:00:00'));
				$arret->setDatePriseEnCompte(new DateTime('1970-01-01 00:00:00'));
				$arret->setCategorie(1);
			}
			$contratId = $form['Societe']->getData();
			$contrat= $this->getDoctrine()
				->getRepository('AdministrationSocieteBundle:PandoreContrat')
				->find($contratId);
			$arret->setNomSalarie($contrat->getUser()->getNom());
			$arret->setPrenomSalarie($contrat->getUser()->getPrenom());
			$arret->setSociete($contrat->getSociete());
			$etablissement = $this->getDoctrine()
			->getRepository('AdministrationSocieteBundle:PandoreEtablissement')
			->findOneBy(array('societe' => $contrat->getSociete(), 'numero' => $contrat->getEtab()));
			$societePaie =  $this->getDoctrine()
			->getRepository('PaieAdministrationBundle:PaieSociete')
			->findOneBy(array('etablissement' => $etablissement));
				
			$gestPaie = $societePaie->getGestionnairePaie() ? $societePaie->getGestionnairePaie() : null;
			$gestRH = $societePaie->getGestionnaireRH() ? $societePaie->getGestionnaireRH() : null;
			$arret->setGestPaie($gestPaie);
			$arret->setGestRH($gestRH);
			$dateDebut = $form['dateDebut']->getData();
			$dateFin = $form['dateFin']->getData();
			$arret->setStatut($dateFin ? (((int) $dateDebut->diff($dateFin)->format('%a') + 1) > 1 ? ArretTravail::EN_ATTENTE : ArretTravail::NON_CONCERNE) : ArretTravail::NON_CONCERNE);
			$em->persist($arret);
			$em->flush();
			$message = 'L’ arret de travail à été enregistré!';
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
			$errors = 0;
			$validationModal = 1;
			$session->set('validationModal', $validationModal);
			$session->set('errors', 0);
			$session->save();
			if ($idArret === '0' || $idArret === '') {
				$form = $this->createForm(new ArretTravailType($pourModifier, $categorieModifier, $em), $arret);
			}
		} else {
			$errors = 1;
			$session->set('errors', 1);
			$session->save();
		}
		return $this->render('PaieArretTravailBundle:default:ajouterArret.html.twig', array(
				'form' => $form->createView(),
				'formModal' => $formModal,
				'formPiecesJointes' => $formPiecesJointes,
				'vieillesPiecesJointes' => $listeVieillesPJ,
				'extensions' => $extensions,
				'arret' => $arret,
				'selected' => $selected,
				'errors' => $errors,
				'arretId' => $arret->getId(),
		));
	}	
}
