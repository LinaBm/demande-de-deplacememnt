<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Administration\SocieteBundle\Entity\PandoreSociete;

/**
 * PandoreContrat
 *
 * @ORM\Table(name="pandore.contrat")
 * @ORM\Entity(repositoryClass="Administration\SocieteBundle\Entity\PandoreContratRepository")
 */
class PandoreContrat
{
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="id", type="string", length=45, nullable=true)
	 * @ORM\Id
	 */
	private $id;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreIndividu")
	 * @ORM\JoinColumn(name="matricule", referencedColumnName="matricule")
	 */
	private $user;
	

	/**
	 * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreSociete")
	 * @ORM\JoinColumn(name="societe", referencedColumnName="id")
	 */
	private $societe;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="etab", type="string")
	 */
	private $etab;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_sortie", type="string")
	 */
	private $dateSortie;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_fincont", type="string")
	 */
	private $dateFin;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_contrat", type="string")
	 */
	private $typeContrat;
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set id
	 *
	 * @param integer $id
	 *
	 * @return DdrcSociete
	 */
	public function setId($id)
	{
		$this->id = $siret;
	
		return $this;
	}
	
	
    /**
     * Set etab
     *
     * @param string $etab
     *
     * @return PandoreContrat
     */
    public function setEtab($etab)
    {
        $this->etab = $etab;
    
        return $this;
    }

    /**
     * Get etab
     *
     * @return string
     */
    public function getEtab()
    {
        return $this->etab;
    }

    /**
     * Set dateSortie
     *
     * @param $dateSortie
     *
     * @return PandoreContrat
     */
    public function setDateSortie($dateSortie)
    {
        $this->dateSortie = $dateSortie;
    
        return $this;
    }

    /**
     * Get dateSortie
     *
     * @return \DateTime
     */
    public function getDateSortie()
    {
        return $this->dateSortie;
    }

    /**
     * Set dateFincont
     *
     * @param $dateFincont
     *
     * @return PandoreContrat
     */
    public function setDateFincont($dateFincont)
    {
        $this->date_fincont = $dateFincont;
    
        return $this;
    }

    /**
     * Get typeContrat
     *
     * @return string
     */
    public function getTypeContrat()
    {
    	return $this->typeContrat;
    }
    
    /**
     * Set typeContrat
     *
     * @param $typeContrat
     *
     * @return PandoreContrat
     */
    public function setTypeContrat($typeContrat)
    {
    	$this->typeContrat = $typeContrat;
    
    	return $this;
    }
    
    
    
    /**
     * Get dateFincont
     *
     * @return \DateTime
     */
    public function getDateFincont()
    {
        return $this->date_fincont;
    }

    /**
     * Set user
     *
     * @param \Administration\SocieteBundle\Entity\PandoreIndividu $user
     *
     * @return PandoreContrat
     */
    public function setUser(\Administration\SocieteBundle\Entity\PandoreIndividu $matricule = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Administration\SocieteBundle\Entity\PandoreIndividu
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set societe
     *
     * @param \Administration\SocieteBundle\Entity\PandoreSociete $societe
     *
     * @return PandoreContrat
     */
    public function setSociete(\Administration\SocieteBundle\Entity\PandoreSociete $societe = null)
    {
        $this->societe = $societe;
    
        return $this;
    }

    /**
     * Get societe
     *
     * @return \Administration\SocieteBundle\Entity\PandoreSociete
     */
    public function getSociete()
    {
        return $this->societe;
    }
}
