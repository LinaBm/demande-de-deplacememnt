<?php

namespace Administration\AclBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AclResourceRepository extends EntityRepository {

    public function findAllowedBundle($bundleName, $userId) {
        $data = $this->createQueryBuilder('resource')
                ->select('right.id', 'resource.name as bundleName', 'right.isAllowed as allowedRight', 'right.userId', 'grp.id grpId', 'userGrp.id userGrpId', 'right.id rightId')
                ->leftJoin('AclBundle:AclUserRight', 'right', 'WITH', 'right.resourceId = resource.id AND resource.isActive=1 AND right.isActive=1')
                ->leftJoin('AclBundle:AclGroup', 'grp', 'WITH', 'grp.id = right.groupId AND grp.isActive=1')
                ->leftJoin('AclBundle:AclUserGroup', 'userGrp', 'WITH', 'userGrp.groupId = right.groupId AND userGrp.userId = :userId AND userGrp.isActive=1')
                ->where('resource.name = :bundleName')
                ->andWhere('right.userId= :userId OR right.userId IS NULL')
                ->setParameter('bundleName', $bundleName)
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getResult();
        $isAllowed = false;
        $levels = array();
        if (empty($data)) {
            $isAllowed = true;
        }
        foreach ($data as $k => $row) {
            # remove user that is not in group
            if (null === $row['userId'] && null !== $row['grpId'] && null === $row['userGrpId']) {
                unset($data[$k]);
                continue;
            }
            if (!empty($row['userId']) and ! empty($row['grpId'])) {
                $levels[1] = $row['allowedRight'];
            }
            if (!empty($row['userId']) and empty($row['grpId'])) {
                $levels[2] = $row['allowedRight'];
            }
            if (empty($row['userId']) and ! empty($row['grpId'])) {
                $levels[3] = $row['allowedRight'];
            }
            if (empty($row['userId']) and empty($row['grpId'])) {
                $levels[4] = $row['allowedRight'];
            }
        }
        krsort($levels);
        foreach ($levels as $level) {
            $isAllowed = $level;
        }
        if ($isAllowed == 1) {
            $isAllowed = true;
        } else {
            $isAllowed = false;
        }
        return $isAllowed;
    }

    public function findUserAllowedOnRoute($routePath, $routeName, $userId) {
        $data = $this->createQueryBuilder('resource')
                ->select('resource.name as resourceName', 'right.isAllowed as allowedRight', 'right.userId', 'resource.routeName', 'resource.route routePath'
                        , 'grp.id grpId', 'userGrp.id userGrpId')
                ->leftJoin('AclBundle:AclUserRight', 'right', 'WITH', 'right.resourceId = resource.id AND resource.isActive=1 AND right.isActive=1')
                ->leftJoin('AclBundle:AclGroup', 'grp', 'WITH', 'grp.id = right.groupId AND grp.isActive=1')
                ->leftJoin('AclBundle:AclUserGroup', 'userGrp', 'WITH', 'userGrp.groupId = right.groupId AND userGrp.userId = :userId AND userGrp.isActive=1')
                ->where('resource.type > 1')
                ->andWhere('grp.isActive = 1 OR grp.isActive IS NULL') // if rule for all the groups
                ->andWhere('userGrp.isActive = 1 OR userGrp.isActive IS NULL') // if rule for all users
                ->andWhere('resource.isActive = 1')
                ->andWhere('right.isActive = 1')
                ->andWhere('resource.route = :routePath OR resource.routeName = :routeName')
                ->andWhere('right.userId = :userId OR right.userId IS NULL')
                ->setParameter('routePath', $routePath)
                ->setParameter('routeName', $routeName)
                ->setParameter('userId', $userId)
                ->orderBy('right.userId', 'DESC') #get route for all users
                ->orderBy('resource.routeName', 'DESC') # get route name first
                ->getQuery()
                ->getResult();
        $isAllowed = false;
        $levels = array();
        if (empty($data)) {
            $isAllowed = true;
        }
        foreach ($data as $k => $row) {
            # remove user that is not in group
            if (null === $row['userId'] && null !== $row['grpId'] && null === $row['userGrpId']) {
                unset($data[$k]);
                continue;
            }
            if (!empty($row['userId']) and ! empty($row['grpId'])) {
                $levels[1] = $row['allowedRight'];
            }
            if (!empty($row['userId']) and empty($row['grpId'])) {
                $levels[2] = $row['allowedRight'];
            }
            if (empty($row['userId']) and ! empty($row['grpId'])) {
                $levels[3] = $row['allowedRight'];
            }
            if (empty($row['userId']) and empty($row['grpId'])) {
                $levels[4] = $row['allowedRight'];
            }
        }
        krsort($levels);
        foreach ($levels as $level) {
            $isAllowed = $level;
        }
        if ($isAllowed == 1) {
            $isAllowed = true;
        } else {
            $isAllowed = false;
        }
        return $isAllowed;
    }

}
