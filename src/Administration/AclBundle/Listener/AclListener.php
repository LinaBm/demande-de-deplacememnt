<?php

/**
 * @author Manea Tiberiu Valentin <tmanea@veo-finances.com>
 */

namespace Administration\AclBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class AclListener {

    protected $container;
    protected $entityManager;
    protected $redirectPage;
    protected $isAJAX = false;

    public function __construct(ContainerInterface $container, $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function onKernelRequest(GetResponseEvent $event) {
        // check application status
        $this->manageAppStatus($event);

        $kernel = $event->getKernel();
        $request = $event->getRequest();
        $container = $this->container;
        $routeName = $request->get('_route');
        $routePath = $request->getPathInfo();
        $token = $container->get('security.context')->getToken();
        $user = (null !== $token ? (null !== $token->getUser() ? $token->getUser() : null ) : null);

        // if user is logged in
        if (!empty($user) && $user != "anon.") {
            $fullController = ($request->attributes->get('_controller') !== null) ? $request->attributes->get('_controller') : '--';
            $parts = $pieces = explode("\\", $fullController);
            $bundleName = '';
            foreach ($parts as $part) {
                if (strpos($part, 'Bundle') !== false) {
                    $bundleName = str_replace('Bundle', '', $part);
                }
            }

            // get last page
            $lastPage = $request->headers->get('referer');

            /* ACL ON BUNDLE */
            $allowedBundle = $container->get('doctrine')->getManager("acl")->getRepository("AclBundle:AclResource")->findAllowedBundle($bundleName, $user->getId());
            /* if not allowed set redirect response for $this->onKernelResponse() */
            if ($allowedBundle === false) {
                if ($request->isXmlHttpRequest()) {// IF REQUEST IS AJAX 
                    return $event->setResponse(new Response('Access denied.'));
                }
//                if (!empty($lastPage)) {
//                    return $event->setResponse(new RedirectResponse($lastPage));
//                } else {
                return $event->setResponse(new RedirectResponse($request->server->get('SCRIPT_NAME') . '/suivi-etudes'));
//                }
            }

            /* ACL ON ROUTE */
            $allowedToRoute = $container->get('doctrine')->getManager("acl")->getRepository("AclBundle:AclResource")->findUserAllowedOnRoute($routePath, $routeName, $user->getId());
            if ($allowedToRoute === false) {
                if ($request->isXmlHttpRequest()) {// IF REQUEST IS AJAX 
                    return $event->setResponse(new Response('Access denied.'));
                }
                if (!empty($lastPage)) {
                    return $event->setResponse(new RedirectResponse($lastPage));
                } else {
                    return $event->setResponse(new RedirectResponse($request->server->get('SCRIPT_NAME') . '/suivi-etudes'));
                }
            }
        }
    }

    public function getRoutesWithDB() {
        $allRoutes = [];
        foreach ($this->container->get('router')->getRouteCollection()->all() as $routeName => $route) {
            $fullController = isset($route->getDefaults()['_controller']) ? $route->getDefaults()['_controller'] : '--';
            $controller = isset($route->getDefaults()['_controller']) ?
                    substr($route->getDefaults()['_controller'], strrpos($route->getDefaults()['_controller'], '\\') + 1, (strrpos($route->getDefaults()['_controller'], '::') - strrpos($route->getDefaults()['_controller'], "\\") - 1)) : '--';

            $methods = isset($route->getRequirements()['_method']) ? $route->getRequirements()['_method'] : '--';
            $parts = $pieces = explode("\\", $fullController);
            $url = $route->getPath();
            foreach ($parts as $part) {
                if (strpos($part, 'Bundle') !== false) {
                    $bundleName = str_replace('Bundle', '', $part);
                }
            }
            $route = $route->compile();
            if (strpos($routeName, "api_") !== 0) {
                $emptyVars = [];
                foreach ($route->getVariables() as $v) {
                    $emptyVars[$v] = $v;
                }
                $url = str_replace(array('{', '}'), "", $url);
                $variables = $route->getVariables();
                $allRoutes[] = [
                    'bundle' => $bundleName,
                    "controller" => $controller,
                    "name" => $routeName,
                    "url" => $url,
                    "variables" => !empty($variables) ? $variables : '--',
                    "methods" => $methods
                ];
            }
        }
        // order by bunle name
        // get all resources defined in db
        $dbResources = $this->container->get('doctrine')->getManager("acl")->getRepository('AclBundle:AclResource')->findAll();
        // sort array
        array_multisort($allRoutes, SORT_ASC);
        $indexArray = array();
        foreach ($dbResources as $route) {
            $index = array_keys($this->array_column($allRoutes, 'name'), $route->getRouteName());
            if (isset($index[0])) {
                $indexArray[] = $index[0];
            }
        }
        // remove route from array
        foreach ($indexArray as $key) {
            unset($allRoutes[$key]);
        }
        return $allRoutes;
    }

    public function getBundleRoutes() {
        $allRoutes = [];
        foreach ($this->container->get('router')->getRouteCollection()->all() as $routeName => $route) {
            $fullController = isset($route->getDefaults()['_controller']) ? $route->getDefaults()['_controller'] : '--';
            $controller = isset($route->getDefaults()['_controller']) ?
                    substr($route->getDefaults()['_controller'], strrpos($route->getDefaults()['_controller'], '\\') + 1, (strrpos($route->getDefaults()['_controller'], '::') - strrpos($route->getDefaults()['_controller'], "\\") - 1)) : '--';

            $methods = isset($route->getRequirements()['_method']) ? $route->getRequirements()['_method'] : '--';
            $parts = $pieces = explode("\\", $fullController);
            $url = $route->getPath();
            foreach ($parts as $part) {
                if (strpos($part, 'Bundle') !== false) {
                    $bundleName = str_replace('Bundle', '', $part);
                }
            }
            $route = $route->compile();
            if (strpos($routeName, "api_") !== 0) {
                $emptyVars = [];
                foreach ($route->getVariables() as $v) {
                    $emptyVars[$v] = $v;
                }
                $url = str_replace(array('{', '}'), "", $url);
                $variables = $route->getVariables();
                $allRoutes[] = [
                    'bundle' => $bundleName,
                    "controller" => $controller,
                    "name" => $routeName,
                    "url" => $url,
                    "variables" => !empty($variables) ? $variables : '--',
                    "methods" => $methods
                ];
            }
        }
        $dbResources = $this->container->get('doctrine')->getManager("acl")->getRepository('AclBundle:AclResource')->findAll();
        // sort array
        array_multisort($allRoutes, SORT_ASC);
        $indexArray = array();
        foreach ($dbResources as $route) {
            $index = array_keys($this->array_column($allRoutes, 'name'), $route->getRouteName());
            if (isset($index[0])) {
                $indexArray[] = $index[0];
            }
        }
        // remove route from array
        foreach ($indexArray as $key) {
            $allRoutes[$key]['active'] = 1;
        }
        return $allRoutes;
    }

    public function getAllRoutes() {
        $allRoutes = [];
        foreach ($this->container->get('router')->getRouteCollection()->all() as $routeName => $route) {
            $fullController = isset($route->getDefaults()['_controller']) ? $route->getDefaults()['_controller'] : '--';
            $controller = isset($route->getDefaults()['_controller']) ?
                    substr($route->getDefaults()['_controller'], strrpos($route->getDefaults()['_controller'], '\\') + 1, (strrpos($route->getDefaults()['_controller'], '::') - strrpos($route->getDefaults()['_controller'], "\\") - 1)) : '--';

            $methods = isset($route->getRequirements()['_method']) ? $route->getRequirements()['_method'] : '--';
            $parts = $pieces = explode("\\", $fullController);
            $url = $route->getPath();
            $bundleName = null;
            foreach ($parts as $part) {
                if (strpos($part, 'Bundle') !== false) {
                    $bundleName = str_replace('Bundle', '', $part);
                }
            }
            $route = $route->compile();
            if (strpos($routeName, "api_") !== 0) {
                $emptyVars = [];
                foreach ($route->getVariables() as $v) {
                    $emptyVars[$v] = $v;
                }
                $url = str_replace(array('{', '}'), "", $url);
                $variables = $route->getVariables();
                $allRoutes[] = [
                    'bundle' => $bundleName,
                    "controller" => $controller,
                    "name" => $routeName,
                    "url" => $url,
                    "variables" => !empty($variables) ? $variables : '--',
                    "methods" => $methods
                ];
            }
        }
        array_multisort($allRoutes, SORT_ASC);
        return $allRoutes;
    }

    public function getAllBundles() {
        $routes = $this->getAllRoutes();
        $bundles = array();
        foreach ($routes as $route) {
            if (!empty($route['bundle'])) {
                $bundles[$route['bundle']] = $route['bundle'];
            }
        }
        return $bundles;
    }

    public function getSetBundles() {
        $bundles = $this->getAllBundles();
        // get all resources defined in db
        $dbResources = $this->container->get('doctrine')->getManager("acl")->getRepository('AclBundle:AclResource')->findBy(array('type' => 1));
        foreach ($dbResources as $resource) {
            if (in_array($resource->getName(), $bundles)) {
                unset($bundles[$resource->getName()]);
            }
        }
        return $bundles;
    }

    // for PHP 5.4=> this is the version pn the server 
    //Signature: array array_column ( array $input , mixed $column_key [, mixed $index_key ] 
    function array_column(array $input, $column_key, $index_key = null) {

        $result = array();
        foreach ($input as $k => $v)
            $result[$index_key ? $v[$index_key] : $k] = $v[$column_key];

        return $result;
    }

    public function manageAppStatus(GetResponseEvent $event) {
        $dbResources = $this->container->get('doctrine')->getManager('acl')->getRepository('AclBundle:ApplicationDown')->find(1);
        if (!empty($dbResources) && $dbResources->getStatus() == 1) {
            $event->setResponse($this->container->get('templating')->renderResponse('AclBundle:extra:appDown.html.twig'));
        }
    }

}
