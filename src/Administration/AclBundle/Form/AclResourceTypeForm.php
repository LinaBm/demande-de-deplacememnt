<?php

namespace Administration\AclBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AclResourceTypeForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'choice', array(
                    'choices' => array(
                        '1' => 'Application', 
                        '2' => 'Route',
                        '3'=> 'Route with parameters'
                        ),
                    'constraints' => array(
                        new NotBlank()
                        ),
                    )
                )
                ->add('submit', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'resourceTypeForm';
    }

}
