<?php

namespace Administration\AclBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GroupeController extends Controller {

    /**
     * @Route("/groupe/visualiser/{page}", name="acl_role_view", 
     * defaults={"page"=1},requirements={"page": "\d+"})
     * @Method({"GET"})
     */
    public function viewAction($page) {
        $em = $this->getDoctrine()->getManager('acl');

        $onPage = 5;
        #sorting params
        $request = $this->container->get('request');
        $sort = null !== $request->query->get('sort') && ($request->query->get('sort') == "resourceName" || $request->query->get('sort') == "resourceType" ) ? $request->query->get('sort') : 'r.name';
        $direction = null !== $request->query->get('direction') && ($request->query->get('direction') == "asc" || $request->query->get('direction') == "desc") ? $request->query->get('direction') : 'ASC';
        $data = $em->createQueryBuilder('query')
                        ->select('r')
                        ->from('AclBundle:AclGroup', 'r')
                        ->where('r.isActive = 1')
                        ->orderBy($sort, $direction)
                        ->getQuery()->getResult();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $data, $request->query->getInt('page', $page)/* page number */
                , $onPage/* limit per page */
        );
        return $this->render('AclBundle:admin:role\visualiser.html.twig', array(
                    'pagination' => $pagination,
                    'page' => $page,
                    'onPage' => $onPage,
        ));
    }

    /**
     * @Route("/groupe/ressources/{id}", name="acl_role_resources", defaults={"id"=0})
     * @Method({"GET"})
     */
    public function viewResourcesAction($id) {
        $em = $this->getDoctrine()->getManager('acl');
        $grp = $em->getRepository('AclBundle:AclGroup')->find($id);
        if (empty($id) || empty($grp)) {
            return $this->redirect($this->generateUrl('acl_role_view'));
        }
        $grpName = $grp->getName();
        $data = $em->createQueryBuilder('query')
                        ->select('ur.id', 'r.id as ressourceId', 'r.name', 'r.isActive', 'ur.isAllowed allowedRight', 'ur.isActive activeRight')
                        ->from('AclBundle:AclResource', 'r')
                        ->innerJoin('AclBundle:AclUserRight', 'ur', 'WITH', 'r.id = ur.resourceId')
                        ->where('ur.userId is null')
                        ->andWhere('ur.groupId = :id')
                        ->setParameter('id', $id)
                        ->orderBy('r.name', 'ASC')
                        ->getQuery()->getResult();
        return $this->render('AclBundle:admin:role\visualiser_resources.html.twig', array(
                    'data' => $data,
                    'id' => $id,
                    'grpName' => $grpName,
        ));
    }

    /**
     * @Route("/groupe/ajouter-ressources/{id}", name="acl_role_add_resources", defaults={"id"=0})
     * @Method({"GET", "POST"})
     */
    public function addResourcesAction($id) {
        $em = $this->getDoctrine()
                ->getManager('acl');
        $group = $em->getRepository('AclBundle:AclGroup')->find($id);
        if (empty($group)) {
            return $this->redirect($this->generateUrl('acl_role_view'));
        }
        $notIn = $em
                ->createQueryBuilder()
                ->select('ur1.resourceId')
                ->from('AclBundle:AclUserRight', 'ur1')
                ->where('ur1.groupId = :id');
        $data = $em->createQueryBuilder('query')
                        ->select('r.id', 'r.name','ur.groupId')
                        ->from('AclBundle:AclResource', 'r')
                        ->leftJoin('AclBundle:AclUserRight', 'ur', 'WITH', 'ur.resourceId = r.id')
                        ->where('ur.groupId != :id OR ur.groupId is null')
                        ->andWhere($notIn->expr()->notIn('ur.resourceId', $notIn->getDQL()).' OR ur.resourceId IS NULL')
                        ->setParameter('id', $id)
                        ->groupBy('r.id')
                        ->orderBy('r.name')
                        ->getQuery()->execute();
        $request = $this->container->get('request');
        if ($request->isMethod('POST')) {
            try {
                $resource = new \Administration\AclBundle\Entity\AclUserRight();
                $resource->setGroupId($id);
                $resource->setResourceId($request->request->get('resourceId'));
                $resource->setIsAllowed($request->request->get('isAllowed'));
                $resource->setCreatedAt(new \DateTime("now"));
                $resource->setCreatedBy($this->getUser()->getId());
                $em->persist($resource);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Erreur sur insert.');
                return $this->redirect($this->generateUrl('acl_role_add_resources', array('id' => $id)));
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource a été ajouté.');
            return $this->redirect($this->generateUrl('acl_role_add_resources', array('id' => $id)));
        }
        return $this->render('AclBundle:admin:role\ajouter_resources.html.twig', array(
                    'id' => $id,
                    'data' => $data,
                    'name' => $group->getName()
        ));
    }

    /**
     * @Route("/groupe/ressources/desactiver/{id}", name="acl_role_resource_deactivate", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function resourceDesactiverAction($id) {
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclUserRight')->findOneBy(array('id' => $id, 'isActive' => 1));
        if (!empty($resource)) {
            try {
                $resource->setIsActive('0');
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource a été désactivé.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer.');
    }

    /**
     * @Route("/groupe/ressources/activer/{id}", name="acl_role_resource_activate", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function resourceActiverAction($id) {
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclUserRight')->findOneBy(array('id' => $id, 'isActive' => 0));
        if (!empty($resource)) {
            try {
                $resource->setIsActive(1);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource a été activé.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer.');
    }

    /**
     * @Route("/groupe/ressources/permettre/{id}", name="acl_role_resource_allow", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function resourceAllowAction($id) {
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclUserRight')->findOneBy(array('id' => $id, 'isActive' => 1, 'isAllowed' => 0));
        if (!empty($resource)) {
            try {
                $resource->setIsAllowed(1);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource a été rejeté.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer.');
    }

    /**
     * @Route("/groupe/ressources/rejeter/{id}", name="acl_role_resource_disallow", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function resourceDisallowAction($id) {
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclUserRight')->findOneBy(array('id' => $id, 'isActive' => 1, 'isAllowed' => 1));
        if (!empty($resource)) {
            try {
                $resource->setIsAllowed(0);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource a été rejeté.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer.');
    }

    /**
     * @Route("/groupe/ajouter", name="acl_role_add")
     * @Method({"GET", "POST"})
     */
    public function addAction() {
        $form = $this->createForm(new \Administration\AclBundle\Form\AclGroupForm());
        $request = $this->container->get('request');
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager("acl");
                    $resourceObj = new \Administration\AclBundle\Entity\AclGroup();
                    $resourceObj->setName(trim($request->request->get('groupForm')['name']));
                    $resourceObj->setCreatedAt(new \DateTime("now"));
                    $resourceObj->setCreatedBy($this->getUser()->getId());
                    $em->persist($resourceObj);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('error', 'Erreur sur insert.');
                    return $this->redirect($this->generateUrl('acl_role_add'));
                }
                $this->get('session')->getFlashBag()->add('success', 'Le rôle ' . $request->request->get('groupForm')['name'] . ' a été créée avec succès.');
                return $this->redirect($this->generateUrl('acl_role_view'));
            }
        }
        return $this->render('AclBundle:admin:role\ajouter.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/groupe/modifier/{roleId}", name="acl_role_modify", defaults={"roleId"=0})
     * @Method({"GET","POST"})
     */
    public function modifyAction($roleId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $role = $em->getRepository('AclBundle:AclGroup')->find($roleId);
        $form = $this->createForm(new \Administration\AclBundle\Form\AclGroupForm(), $role);
        if (null === $role || $role->getIsActive() == 0) {
            return $this->redirect($this->generateUrl('acl_role_add'));
        }
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                if (!empty($role)) {
                    try {
                        $em = $this->getDoctrine()->getManager("acl");
                        $resourceObj = new \Administration\AclBundle\Entity\AclGroup();
                        $resourceObj->setName(trim($request->request->get('groupForm')['name']));
                        $resourceObj->setUpdatedAt(new \DateTime("now"));
                        $resourceObj->setLastUpdatedBy($this->getUser()->getId());
                        $em->flush();
                    } catch (\Doctrine\DBAL\DBALException $e) {
                        $this->get('session')->getFlashBag()->add('error', 'Erreur sur modifier.');
                        return $this->redirect($this->generateUrl('acl_role_modify', array('roleId' => $roleId)));
                    }
                    $this->get('session')->getFlashBag()->add('success', 'La rôle ' . $request->request->get('groupForm')['name'] . ' a été modifier avec succès.');
                    return $this->redirect($this->generateUrl('acl_role_view'));
                } else {
                    $this->get('session')->getFlashBag()->add('error', 'Erreur sur modifier.');
                }
                return $this->redirect($this->generateUrl('acl_role_view'));
            }
        }
        return $this->render('AclBundle:admin:role\modifier.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/groupe/supprimer/{roleId}", name="acl_role_delete", defaults={"roleId"=0})
     * @Method({"POST"})
     */
    public function deleteAction($roleId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");

        $data = $em->createQueryBuilder('query')
                        ->select('grp', 'usrGrp', 'usrRight')
                        ->from('AclBundle:AclGroup', 'grp')
                        ->leftJoin('AclBundle:AclUserGroup', 'usrGrp', 'WITH', 'grp.id = usrGrp.groupId')
                        ->leftJoin('AclBundle:AclUserRight', 'usrRight', 'WITH', 'usrRight.groupId = grp.id')
                        ->where('grp.id = :roleId')
                        ->setParameter('roleId', $roleId)
                        ->getQuery()->getResult();
        if (!empty($data)) {
            $roleName = $data[0]->getName();
            foreach ($data as $row) {
                try {
                    $em->remove($row);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
                    return new Response('Terminer supprimer.');
                }
            }
            $this->get('session')->getFlashBag()->add('success', 'La groupe ' . $roleName . ' a été suprimer avec succès.');
            return new Response('Terminer supprimer.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            return new Response('Terminer supprimer.');
        }
    }

    /**
     * @Route("/groupe/utilisateurs/{roleId}", name="acl_role_users", defaults={"roleId"=0})
     * @Method({"GET"})
     */
    public function usersAction($roleId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $userEm = $this->getDoctrine()->getManager();
        $grp = $em->createQueryBuilder('query')
                        ->select('usrGrp.userId')
                        ->from('AclBundle:AclGroup', 'grp')
                        ->leftJoin('AclBundle:AclUserGroup', 'usrGrp', 'WITH', 'usrGrp.groupId = grp.id')
                        ->where('grp.id = :roleId')
                        ->setParameter('roleId', $roleId)
                        ->getQuery()->getArrayResult();
        $userIds = null;
        foreach ($grp as $row) {
            $userIds[] = $row;
        }
        $users = $userEm
                        ->createQueryBuilder('query')
                        ->select('usr.id as userId', 'usr.username', 'usr.email', 'usr.displayname')
                        ->from('UserUserBundle:User', 'usr')
                        ->where('usr.id in (:userIds)')
                        ->setParameter('userIds', $userIds)
                        ->orderBy('usr.email', 'ASC')
                        ->getQuery()->getResult();

        return $this->render('AclBundle:admin:role\users.html.twig', array(
                    'data' => $users,
                    'roleId' => $roleId,
                    'title' => !empty($data[0]['name']) ? $data[0]['name'] : '',
        ));
    }

    /**
     * @Route("/groupe/ajouter/utilisateurs/{roleId}", name="acl_role_add_user", defaults={"roleId"=0})
     * @Method({"GET", "POST"})
     */
    public function addUserAction($roleId) {
        $em = $this->getDoctrine()->getManager("acl");
        $userEm = $this->getDoctrine()->getManager();
        $grp = $em->createQueryBuilder('query')
                        ->select('usrGrp.userId')
                        ->from('AclBundle:AclGroup', 'grp')
                        ->leftJoin('AclBundle:AclUserGroup', 'usrGrp', 'WITH', 'usrGrp.groupId = grp.id')
                        ->where('grp.id = :roleId')
                        ->setParameter('roleId', $roleId)
                        ->getQuery()->getArrayResult();
        $userIds = null;
        foreach ($grp as $row) {
            $userIds[] = $row;
        }
        $users = $userEm
                        ->createQueryBuilder('query')
                        ->select('usr.id as userId', 'usr.username', 'usr.email', 'usr.displayname')
                        ->from('UserUserBundle:User', 'usr')
                        ->orderBy('usr.email', 'ASC')
                        ->getQuery()->getResult();
        if (!empty($userIds[0]['userId'])) {
            $users = $userEm
                            ->createQueryBuilder('query')
                            ->select('usr.id as userId', 'usr.username', 'usr.email', 'usr.displayname')
                            ->from('UserUserBundle:User', 'usr')
                            ->where('usr.id not in (:userIds)')
                            ->setParameter('userIds', $userIds)
                            ->orderBy('usr.email', 'ASC')
                            ->getQuery()->getResult();
        }
        $request = $this->container->get('request');
        if ($request->isMethod('POST')) {
            $userId = $request->request->get('user');
            if (!empty($userId) && !empty($roleId)) {
                $newRight = new \Administration\AclBundle\Entity\AclUserGroup();
                $newRight->setGroupId($roleId);
                $newRight->setUserId($userId);
                $newRight->setCreatedAt(new \DateTime("now"));
                $newRight->setCreatedBy($this->getUser()->getId());
                try {
                    $em->persist($newRight);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('error', 'Pas ajouté utilisateur.');
                    return $this->redirect($this->generateUrl('acl_role_view'));
                }
                $this->get('session')->getFlashBag()->add('success', 'L\'utilisateur a été ajouté avec succès.');
                return $this->redirect($this->generateUrl('acl_role_view'));
            } else {
                $this->get('session')->getFlashBag()->add('error', 'Pas ajouté utilisateur.');
                return $this->redirect($this->generateUrl('acl_role_view'));
            }
            return $this->redirect($this->generateUrl('acl_role_view'));
        }

        return $this->render('AclBundle:admin:role\users_dropdown.html.twig', array(
                    'users' => $users,
                    'roleId' => $roleId,
        ));
    }

    /**
     * @Route("/groupe/suprimer-utilisateur/{userId}", name="acl_role_remove_user", defaults={"userId"=null})
     * @Method({"POST"})
     */
    public function removeUserAction($userId) {
        $em = $this->getDoctrine()->getManager("acl");
        $user = $em->getRepository('AclBundle:AclUserGroup')->findBy(array('userId' => $userId));
        if (isset($user[0])) {
            try {
                $em->remove($user[0]);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'L\'utilisateur n\'a pas été supprimé.');
                return new Response('Error');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'L\'utilisateur n\'a pas été supprimé.');
            return new Response('Error');
        }
        $this->get('session')->getFlashBag()->add('success', 'L\'utilisateur a  été supprimé.');
        die;
    }

}
