<?php

namespace Administration\HierarchieBundle\Form\Hierarchie;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AjouterHierarchie extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add(
            'hieLibelle', 'text', array(
                'label' => 'Choisir un nom',
                'required' => true,
                'constraints' => array(
                new NotBlank(array(
                    'message' => 'Libellé ne doit pas être vide.'
                        )),
        )));
        $builder->add(
            'hieDescription', 'text', array(
                'label' => 'Description courte',
                'required' => false,
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\HierarchieBundle\Entity\Hierarchie',
        ));
    }

    public function getName() {
        return 'hierarchy';
    }

}
