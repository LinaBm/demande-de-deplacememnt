<?php

namespace Administration\HierarchieBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserList extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('ParSonNom_:', 'textarea');
        $builder->add(
            'grpLibelle', 'text');
    }

    public function getName() {
        return '';
    }

}
