<?php

namespace Administration\HierarchieBundle\Form\Utilisateur;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UtilisateurType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('usrNom', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                    ))
                )
                ->add('usrPrenom', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                    ))
                )
                ->add('usrPoste', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                    ))
                )
                ->add('usrDateEntree', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Range(array('min' => 19000101, 'max' => 20501231)),
                    ))
                )
                ->add('usrEmail', 'email', array(
                    'required' => false,
                    'constraints' => array(
                        new Assert\Email(),
                    ))
                )
                ->add('usrLogin', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrEmailnoz', 'email', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrSociete', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Range(array('min' => 0, 'max' => 999999)),
                    )
                ))
                ->add('usrNumvoie', 'text', array(
                    'label' => 'Numéro de voie: ',
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Range(array('min' => 0, 'max' => 999999)),
                    )
                ))
                ->add('usrAdresse', 'textarea', array(
                    'label' => 'Adresse: ',
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrCodepostal', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Range(array('min' => 0, 'max' => 99999)),
                    )
                ))
                ->add('usrVille', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrDdn', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrNationalite', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrTelephoneFixe', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                ))
                ->add('usrTelephonePort', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                ))
                ->add('usrSituationFamiliale', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrNbEnfants', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Range(array('min' => 0, 'max' => 99)),
                    )
                ))
                ->add('usrTypeContrat', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrHoraire', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Regex(array('pattern' => '~^\d+(,\d+)?$~')),
                    )
                ))
                ->add('usrCategSociopro', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 45)),
                    )
                ))
                ->add('usrRemuneration', 'text', array(
                    'required' => false,
                    'empty_data' => 0,
                    'constraints' => array(
                        new Assert\Regex(array('pattern' => '~^\d+(,\d+)?$~')),
                    )
                ))
                ->add('usrService', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 200)),
                    )
                ))
                ->add('usrDateAnciennete', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                ))
                ->add('usrDateSortie', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                ))
                ->add('usrCodeSap', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString(),
                    'constraints' => array(
                        new Assert\Length(array('min' => 0, 'max' => 6)),
                    )
                ))
                ->add('usr2014', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrMailStatus2015', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrStatus', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrCommentaires', 'textarea', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('usrChecked', 'text', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('createdAt', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('updatedAt', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('dateDemandee', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('dateAccepte', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('dateCollab', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('dateManager', 'date', array(
                    'required' => false,
                    'empty_data' => new EmptyString()
                ))
                ->add('dateValider', 'date', array(
                    'required' => false,
                ))
                ->add('usrComentaires')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\HierarchieBundle\Entity\Utilisateur'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return null;
    }

}
