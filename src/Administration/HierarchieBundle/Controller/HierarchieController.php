<?php

namespace Administration\HierarchieBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityRepository;

class HierarchieController extends BaseController {

    /**
     * @Route("/hierarchie/", name="hierarchie_homepage")
     * @Method({"GET"})
     */
    public function indexAction() {
        return $this->redirect($this->generateUrl('hierarchie-vue'));
    }

    /**
     * @Route("/hierarchie/ajouter/", name="hierarchie-ajouter")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) {
        $hierarchies = new \Administration\HierarchieBundle\Entity\Hierarchie();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Hierarchie\AjouterHierarchie(), $hierarchies);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $label = trim($request->request->get('hierarchy')['hieLibelle']);
                $em = $this->getDoctrine()->getManager("hierarchie_bundle");
                $name = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieLibelle' => $label));
                if (empty($name)) {
                    $hierarchies->setHieLibelle(ucfirst(strtolower($label)));
                    $em->persist($hierarchies);
                    $em->flush();

                    $gpfg = new \Administration\HierarchieBundle\Entity\GroupePereFils();
                    $gpfg->setGpfIdpere(0);
                    $gpfg->setGpfIdfils(100);
                    $hieNew = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieLibelle' => $request->request->get('hierarchy')['hieLibelle']));
                    $gpfg->setHieId($hieNew->getHieId());
                    $em->persist($gpfg);
                    $em->flush();

                    // set message and redirect parameters
                    $this->get('session')->getFlashBag()->add('success', 'La Hiérarchie a bien été ajoutée.');
                    return $this->redirect($this->generateUrl('hierarchie-modifier', array('hierarchie' => $hierarchies->getHieId())));
                } else {
                    // set message and redirect parameters
                    $this->get('session')->getFlashBag()->add('myError', 'Ce hierarhie existe déjà.');
                    return $this->redirect($this->generateUrl('hierarchie-ajouter'));
                }
            } else {
                $validator = new \Administration\HierarchieBundle\Validators\Messages();
                $formErrors = $validator->getErrorMessages($form);
                $this->get('session')->getFlashBag()->add('error', $formErrors);
                return $this->redirect($this->generateUrl('hierarchie-ajouter'));
            }
        }
        // on GET 
        return $this->render('@hierarchie/hierarchie/ajouter_hierarchie.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/hierarchie/vue/", name="hierarchie-vue")
     * @Method({"GET", "POST"})
     */
    public function visualizeAction() {
        $request = $this->getRequest();
        $hieId = $request->query->get('hierarchie');
        $hierarchies = new \Administration\HierarchieBundle\Entity\Hierarchie();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Hierarchie\VueHierarchie(), $hierarchies);
        return $this->render('@hierarchie/hierarchie/visualiser_hierarchie.html.twig', array(
                    'form' => $form->createView(),
                    'selectedHie' => $hieId,
        ));
    }

    /**
     * @Route("/hierarchie/vue/{hieId}", name="hierarchie-vue-par-id")
     * @Method({"GET"})
     */
    public function visualizeHierarchieXHRAction($hieId) {
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {// IF REQUEST IS NOT AJAX 
            return $this->redirect($this->generateUrl('hierarchie-vue'));
        }
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $data = $em->getRepository('HierarchieBundle:Hierarchie')->buildTree((int) $hieId);
        $first = null;
        foreach ($data as $i => $row) {
            if ($row['parent'] == 100) {
                $first = $row['parent'];
            }
        }
        if (!empty($data)) {
            $tree = $this->buildTree($data, $first);
            $list = $this->listTreeUL($tree);
        } else {
            $list = "Cette hiérarchie ne contient pas encore de groupe.";
        }
        return $this->render('@hierarchie/hierarchie/hierarchie.html.twig', array(
                    'list' => $list,
        ));
    }

    /**
     * @Route("/hierarchie/modifier/", name="hierarchie-modifier")
     * @Method({"GET"})
     */
    public function editAction() {
        $request = $this->getRequest();
        $hierarchies = new \Administration\HierarchieBundle\Entity\Hierarchie();
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Hierarchie\VueHierarchie(), $hierarchies);
        $selectedHie = $request->query->get('hierarchie');

        return $this->render('@hierarchie/hierarchie/modifier_hierarchie.html.twig', array(
                    'form' => $form->createView(),
                    'hierarchy' => $selectedHie,
        ));
    }

    /**
     * @Route("/hierarchie/modifier/{hieId}", name="hierarchie-hierarchie-par-id")
     * @Method({"GET"})
     */
    public function editHieAction($hieId) {
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {// IF REQUEST IS NOT AJAX 
            return $this->redirect($this->generateUrl('hierarchie-modifier'));
        }
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $groups = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsForHierarchy($hieId);
        $first = null;
        foreach ($groups as $i => $row) {
            if ($row['gpfIdpere'] == 0) {
                $first = $row['gpfIdpere'];
            }
        }
        if (!empty($groups)) {
            $tree = $this->indentedTree($groups, $first);
        }

        $request = $this->getRequest();
        $action = $request->query->get('loadAction');
        if (!empty($groups)) {
            if ($action == 'addLink') {
                $groupsNotIn = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsNotInHierarchy($hieId);
                return $this->render('@hierarchie/hierarchie/ajouter_groupe.html.twig', array(
                            'list' => $tree,
                            'hieId' => $hieId,
                            'groupsNotIn' => $groupsNotIn
                ));
            }
            if ($action == 'modifyLink') {
                return $this->render('@hierarchie/hierarchie/modifier_groupe.html.twig', array(
                            'list' => $tree,
                            'hieId' => $hieId
                ));
            }
            if ($action == 'deleteLink') {
                if (array_key_exists(100, $tree)) {
                    unset($tree[100]);
                }
                return $this->render('@hierarchie/hierarchie/supprimer_groupe.html.twig', array(
                            'list' => $tree,
                            'hieId' => $hieId
                ));
            }
        }
        return new Response('La hiérarchie n\'existe pas.');
    }

    /**
     * @Route("/hierarchie/ajouter-groupe", name="hierarchie-ajouter-groupe")
     * @Method({"POST"})
     */
    public function addGroupAction() {
        $request = $this->getRequest();
        $parent = $request->request->get('parent');
        $child = $request->request->get('child');
        $hieId = $request->request->get('hieId');

        if (!empty($hieId) && !empty($parent) && !empty($child)) {
            $em = $this->getDoctrine()->getManager("hierarchie_bundle");
            $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findBy(array('grpId' => $child));
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findBy(array('hieId' => $hieId));
            $gpfgExist = $em->getRepository('HierarchieBundle:GroupePereFils')->findBy(array(
                'hieId' => $hieId,
                'gpfIdfils' => $child,
            ));

            if (!empty($childGroup) && !empty($hierarchy)) {
                if (!empty($gpfgExist)) {
                    $this->get('session')->getFlashBag()->add('myError', 'Cette groupe est deja dans cette hiérarchie.');
                    return $this->redirect($this->generateUrl('hierarchie-modifier'));
                }
                $this->get('session')->getFlashBag()->add('success', 'Le groupe ' . $childGroup[0]->getGrpLibelle() . ' a bien été ajoutée à la Hiérarchie ' . $hierarchy[0]->getHieLibelle() . '.');
                $gpfg = new \Administration\HierarchieBundle\Entity\GroupePereFils();
                $em = $this->getDoctrine()->getManager("hierarchie_bundle");
                $gpfg->setGpfIdpere($parent);
                $gpfg->setGpfIdfils($child);
                $gpfg->setHieId($hieId);
                $em->persist($gpfg);
                $em->flush();
            } else {
                $this->get('session')->getFlashBag()->add('myError', 'Le groupe ou la hierarchié n\'existe pas.');
                return $this->redirect($this->generateUrl('hierarchie-modifier'));
            }
            return $this->redirect($this->generateUrl('hierarchie-modifier') . '?hierarchie=' . $hieId);
        } else {
            $this->get('session')->getFlashBag()->add('myError', 'Groupe à ajouter et Groupe père sont obligatoires.');
            return $this->redirect($this->generateUrl('hierarchie-modifier') . '?hierarchie=' . $hieId);
        }
    }

    /**
     * @Route("/hierarchie/ajouter-groupe-parent/{parentId}/{hieId}", name="hierarchie-ajouer-groupe-parent")
     * @Method({"GET"})
     */
    public function addParentAction($parentId, $hieId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $data = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsForHierarchy($hieId);
        $tree = '';

        foreach ($data as $i => $row) {
            if ($row['gpfIdpere'] == 0) {
                $first = $row['gpfIdpere'];
            }
        }
        if (!empty($data)) {
            $tree = $this->indentedTree($data, $first);
        }

        $isChild = $this->childArray($data, $parentId);
        $parent = $this->getParentId($data, $parentId);
        return $this->render('@hierarchie/hierarchie/groupe_pere.html.twig', array(
                    'list' => $tree,
                    'selected' => $parentId,
                    'isChild' => $isChild,
                    'parent' => $parent,
        ));
    }

    /**
     * @Route("/hierarchie/modifier-groupe" , name="hierarchie-modifier-groupe")
     * @Method({"POST"})
     */
    public function modifyGroupAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        $row = $em->getRepository('HierarchieBundle:GroupePereFils')->findOneBy(
                array(
                    'gpfIdfils' => $request->request->get('child'),
                    'hieId' => $request->request->get('hieId')
                )
        );
        if (!empty($row)) {
            $row->setGpfIdpere($request->request->get('parent'));
            $em->flush();
            $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $request->request->get('child')));
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $request->request->get('hieId')));
            $this->get('session')->getFlashBag()->add('success', 'Le groupe ' . $childGroup->getGrpLibelle() . ' a bien été modifié dans la Hiérarchie ' . $hierarchy->getHieLibelle() . '.');
            return $this->redirect($this->generateUrl('hierarchie-modifier') . '?hierarchie=' . $request->request->get('hieId'));
        } else {
            $this->get('session')->getFlashBag()->add('myError', 'La modification du groupe a échoué.');
            return $this->redirect($this->generateUrl('hierarchie-modifier'));
        }
    }

    /**
     * @Route("/hierarchie/obtenir-groupe-parent/{parentId}/{hieId}", name="hierarchie-obtenir-groupe-parent")
     * @Method({"GET"})
     */
    public function getParentAction($parentId, $hieId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $parentRow = $em->getRepository('HierarchieBundle:GroupePereFils')->findOneBy(array('gpfIdfils' => $parentId, 'hieId' => $hieId));
        $name = "------";
        if (!empty($parentRow)) {
            $parent = $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $parentRow->getGpfIdpere()));
        }
        if (!empty($parent)) {
            $name = $parent->getGrpLibelle();
        }
        return new Response('<kbd>' . $name . '</kbd>');
    }

    /**
     * @Route("/hierarchie/supprimer-groupe" , name="hierarchie-supprimer-groupe")
     * @Method({"POST"})
     */
    public function deleteGroupAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        $row = $em->getRepository('HierarchieBundle:GroupePereFils')->findOneBy(
                array(
                    'gpfIdfils' => $request->request->get('child'),
                    'hieId' => $request->request->get('hieId')
                )
        );
        if (!empty($row)) {
            $this->deleteGpfgChildGroups($row->getGpfIdfils(), $request->request->get('hieId'));
            $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $request->request->get('child')));
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $request->request->get('hieId')));
            $em->remove($row);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Le groupe ' . $childGroup->getGrpLibelle() . ' a bien été supprimé dans la Hiérarchie ' . $hierarchy->getHieLibelle() . '.');
            return $this->redirect($this->generateUrl('hierarchie-modifier') . '?hierarchie=' . $request->request->get('hieId'));
        } else {
            if (!empty($grpLibelle) && !empty($hierarchy) && null !== $hierarchy->getHieLibelle()) {
                $this->get('session')->getFlashBag()->add('myError', 'La suppression du groupe ' . $grpLibelle . ' dans la Hiérarchie ' . $hierarchy->getHieLibelle() . ' a échoué.');
            } else {
                $this->get('session')->getFlashBag()->add('myError', 'La suppression a échoué.');
            }
        }
        return $this->redirect($this->generateUrl('hierarchie-modifier'));
    }

    /**
     * @Route("/hierarchie/importation/", name="hierarchie-importation")
     * @Method({"GET", "POST"})
     */
    /* USAGE
     * 1. import excel file
     * 2. read file
     * 3. based on the adtion do the following:
     *      3.1 delete from groupes_pere_fils_gpf based on $hieId
     *      3.2 add to groupes_pere_fils_gpf based on $hieId
     *      3.3 add to usr_role_group if action = importUsr
     * 4. select from groupes_pere_fils_gpf and groupes_grp
     * 
     * for more read the code in the action bellow
     */
    public function importAction() {
        // set execution time 
//        set_time_limit(2000);
        set_time_limit(300);
//        ini_set('memory_limit', '-1');
        // construct form 
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $hierarchies = $em->getRepository('HierarchieBundle:Hierarchie')->findBy(array(), array('hieId' => 'ASC'));
        $request = $this->getRequest();
        $selectedHie = $request->query->get('hierarchie');

        $form = $this->get('form.factory')->createNamedBuilder('', 'form', null, array())
                ->add('importFile', 'file', array('required' => true))
                ->add('hieLibelle', 'entity', array(
                    'class' => 'HierarchieBundle:Hierarchie',
                    'query_builder' => function(EntityRepository $em) {
                        return $em->createQueryBuilder('p')
                                ->orderBy('p.hieLibelle', 'ASC');
                    },
                    'property' => 'hieLibelle',
                    'required' => true,
                    'empty_value' => 'Choisissez une hiérarchie',
                ))
                ->getForm();
        // only on POST
        if ($request->isMethod('POST')) {
            $fileType = null;
            // bind form to request to persist data
            $form->bind($request);
            // upload file 
            $file = $form->get('importFile')->getData();
            $action = $request->request->get('_action');

            if (!empty($file)) {
                $fileType = $file->getClientMimeType();
            }
            // if is not a excel file do not parse the file
            $fileTypes = array('text/vnd.ms-excel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if (!in_array($fileType, $fileTypes)) {
                $this->get('session')->getFlashBag()->add('myError', 'Il ne est pas un fichier excel.');
                return $this->redirect($this->generateUrl('hierarchie-importation'));
            }

            // Get and set infos
            $hieId = (int) $request->request->get('hieLibelle');
            $dir = $this->get('kernel')->getRootDir() . '/../web/bundles/hierarchie/uploads/';
            // generate unique file name 
            $fileName = uniqid($file->getClientOriginalName());
            $file->move($dir, $fileName);

            // DELETE FROM groupes_pere_fils_gpf by hieId and where gpf_idpere != 0
            if (!empty($hieId)) {
                $urgArray = $em->createQueryBuilder('query')
                                ->delete('HierarchieBundle:GroupePereFils', 'gpfg')
                                ->where('gpfg.hieId = ?1')
                                ->andWhere('gpfg.gpfIdpere != 0')
                                ->setParameter(1, $hieId)
                                ->getQuery()->execute();

                // delete from usr_role group
                $urgArray = $em->createQueryBuilder('query')
                                ->delete('HierarchieBundle:UsrRoleGroupe', 'urg')
                                ->where('urg.hieId = ?1')
                                ->setParameter(1, $hieId)
                                ->getQuery()->execute();
            }
            // END DELETE FROM groupes_pere_fils_gpf 


            /* READ EXCEL FILE */
            $phpExcelObject = \PHPExcel_IOFactory::load($dir . $fileName);
            // only for the first worksheet 
            $worksheet = $phpExcelObject->getSheet(0);
            //  Read rows into an array
            $rows = $worksheet->toArray(NULL, NULL, TRUE, FALSE);
            $gpfgDbRows = array();
            $parentGroups = array();
            // get existing groups in DB
            $allDBGroups = $em->createQueryBuilder('exportList')
                            ->select('grp.grpId', 'grp.grpLibelle')
                            ->from('HierarchieBundle:Groupe', 'grp')
                            ->getQuery()->getResult();
            // put all groups in an array
            $allGroups = $allGroupsWithAccent = array();
            foreach ($allDBGroups as $grp) {
                $allGroups[$grp['grpId']] = preg_replace('`&[^;]+;`', '', trim(strtolower(htmlentities($grp['grpLibelle']))));
                $allGroupsWithAccent[$grp['grpId']] = trim($grp['grpLibelle']);
            }
            // start reading the excel file 
            foreach ($rows as $row) {
                for ($i = 0; $i < count($row); $i++) {
                    if (null != $row[$i]) {
                        // check for custom serial (matricule) created trough this application
                        $userCustomMatricule = preg_match('/^u[0-9]{5}/', trim($row[$i]), $matches);
                        // insert new group in DB based on the conditions bellow
                        if (empty($allGroups[array_search(trim($row[$i]), $allGroups)]) && empty($userCustomMatricule) && !is_numeric($row[$i])) {
                            // recheck by searching again in DB for that group
                            $grpDbRow = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpLibelle' => trim($row[$i])));
                            if (empty($grpDbRow)) {
                                $newGrp = new \Administration\HierarchieBundle\Entity\Groupe();
                                $newGrp->setGrpLibelle(trim($row[$i]));
                                $em->persist($newGrp);
                                $em->flush();
                                // add inserted row to groups array
                                $allGroups[$newGrp->getGrpId()] = trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i]))))));
                                $allGroupsWithAccent[$newGrp->getGrpId()] = trim($row[$i]);
                            }
                        }
                        // !!!!!!! add to usr_role_group !!!!!!!!!!!!!!
                        if (isset($row[$i + 1]) && $action == 'importUsr' && is_string($row[$i]) && (is_numeric($row[$i + 1]) || !empty($userCustomMatricule))) {
                            $userId = !empty($userCustomMatricule) ? trim($row[$i]) : trim($row[$i + 1]);
                            $user = $em->getRepository('HierarchieBundle:Utilisateur')->findOneBy(array('usrMatricule' => $userId));
                            $urgIn = $em->getRepository('HierarchieBundle:UsrRoleGroupe')->findOneBy(array(
                                'usrMatricule' => $userId,
                                'hieId' => $hieId
                            ));
                            if (empty($urgIn)) {
                                $roleId = empty($userCustomMatricule) ? trim($row[$i + 2]) : trim($row[$i + 1]);
                                // set group id by searching in existing groups or in new groupe added
                                if (array_search(trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i])))))), $allGroups) == false) {
                                    $grpId = $grpDbRow->getGrpId();
                                } else {
                                    $grpId = array_search(trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i])))))), $allGroups);
                                }
                                if (!empty($userCustomMatricule)) {
                                    $grpId = array_search(trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i - 1])))))), $allGroups);
                                }

                                $urg = new \Administration\HierarchieBundle\Entity\UsrRoleGroupe('UsrRoleGroupe');
                                $urg->setUsrMatricule($userId);
                                $urg->setRolId($roleId);

                                $urg->setGrpId($grpId);
                                $urg->setHieId($hieId);
                                $em->persist($urg);

                                if (!empty($user)) {
                                    try {
                                        $em->flush();
                                        // on exception the entity manager is closed and when that happens reopen entity manager
                                    } catch (\Doctrine\DBAL\DBALException $e) {
                                        if (!$em->isOpen()) {
                                            $em = $this->getDoctrine()->getManager("hierarchie_bundle")->create(
                                                    $em->getConnection(), $em->getConfiguration()
                                            );
                                        }
                                    }
                                }
                            } else {
                                $grpName = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $grpId));
                                $this->get('session')->getFlashBag()->add('myError', $user->getUsrNom() . ' '
                                        . $user->getUsrPrenom() . ' n\'a pas été importé car cette personne est déjà positionnée dans le groupe '
                                        . $grpName->getGrpLibelle() . '.');
                            }
                            // !!!!!!! END add to usr_role_group !!!!!!!!!!!!!!
                        }
                        // check for custom serial (matricule) created trough this application the next row
                        $secondUserCustomMatricule = 0;
                        if (isset($row[$i + 1])) {
                            $secondUserCustomMatricule = preg_match('/^u[0-9]{5}/', trim($row[$i + 1]), $matches);
                        }
                        if (isset($row[$i + 1]) && is_string($row[$i]) && is_string($row[$i + 1]) && empty($userCustomMatricule) && empty($secondUserCustomMatricule)) {
                            $parent = array_search(trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i])))))), $allGroups);
                            $child = array_search(trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[$i + 1])))))), $allGroups);
                            $gpfgParent = $em->getRepository('HierarchieBundle:GroupePereFils')->findBy(array(
                                'gpfIdpere' => $parent, 'gpfIdfils' => $child, 'hieId' => $hieId
                                    )
                            );
                            $gpfgIsChild = $em->getRepository('HierarchieBundle:GroupePereFils')->findBy(array(
                                'gpfIdpere' => $child, 'gpfIdfils' => $parent, 'hieId' => $hieId
                                    )
                            );
                            if (!empty($gpfgIsChild)) {
                                $this->get('session')->getFlashBag()->add('myError', 'Il y avait une erreur. Rechercher dans votre fichier Excel pour le groupe ' . $allGroupsWithAccent[$parent]);
                            }
                            // INSERT into groupes_pere_fils_gpf
                            if (empty($gpfgParent) && empty($gpfgIsChild)) {
//                                // add parents in db table 
                                if (trim(utf8_decode(strtolower(preg_replace('`&[^;]+;`', '', htmlentities(trim($row[0])))))) == $allGroups[$parent]) {
                                    $gpfgParentGroup = $em->getRepository('HierarchieBundle:GroupePereFils')->findBy(array(
                                        'gpfIdpere' => 100, 'gpfIdfils' => $parent, 'hieId' => $hieId
                                            )
                                    );
                                    if (empty($gpfgParentGroup)) {
                                        $newGpfgParent = new \Administration\HierarchieBundle\Entity\GroupePereFils();
                                        $newGpfgParent->setGpfIdpere(100);
                                        $newGpfgParent->setGpfIdfils($parent);
                                        $newGpfgParent->setHieId($hieId);
                                        $em->persist($newGpfgParent);
                                    }
                                }
//                                // add row
                                $newGpfgRow = new \Administration\HierarchieBundle\Entity\GroupePereFils();
                                $newGpfgRow->setGpfIdpere($parent);
                                $newGpfgRow->setGpfIdfils($child);
                                $newGpfgRow->setHieId($hieId);
                                $em->persist($newGpfgRow);
                                $em->flush();
                            }
                        }
                    }
                }
            }
            // END INSERT into groupes_pere_fils_gpf

            /* END RADING EXCEL FILE */

            // delete file from upload directory 
            unlink($dir . $fileName);

            $this->get('session')->getFlashBag()->add('success', 'La hiérarchie a été générée en fonction de votre fichier Excel.');
            return $this->redirect($this->generateUrl('hierarchie-vue') . '?hierarchie=' . $selectedHie);
        }
        return $this->render('@hierarchie/hierarchie/importation.html.twig', array(
                    'form' => $form->createView(),
                    'hierarchies' => $hierarchies,
                    'selectedHie' => $selectedHie,
        ));
    }

    /**
     * @Route("/hierarchie/exportation", name="hierarchie-exportation")
     * @Method({"GET", "POST"})
     */
    public function exportAction() {
        $hierarchies = new \Administration\HierarchieBundle\Entity\Hierarchie();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Hierarchie\VueHierarchie(), $hierarchies);
        $request = $this->getRequest();
        $selectedHie = $request->query->get('hierarchie');
        return $this->render('@hierarchie/hierarchie/exportation.html.twig', array(
                    'form' => $form->createView(),
                    'selectedHie' => $selectedHie,
        ));
    }

    /**
     * @Route("/hierarchie/exportation-groupe/", name="hierarchie-export-groupe")
     * @Method({"GET"})
     */
    public function exportGroupsAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        $hieId = $request->query->get('id');
        $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findBy(array('hieId' => $hieId));
        // set excel variables
        $fileName = str_replace(' ', '_', $hierarchy[0]->getHieLibelle()) . '_groupes_' . date('d-m-Y');
        $createdBy = 'export program';
        $modifiedBy = 'export program';
        $title = substr($hierarchy[0]->getHieLibelle(), 0, 30);

        // create excel object
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator($createdBy)
                ->setLastModifiedBy($modifiedBy)
                ->setTitle($title);
        // database query
        $data = $em->createQueryBuilder('exportList')
                        ->select('grp1.grpLibelle as parent', 'grp.grpLibelle as child', 'urg.usrMatricule', 'urg.rolId')
                        ->from('HierarchieBundle:GroupePereFils', 'gpfg')
                        ->leftJoin('HierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->leftJoin('HierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'urg.grpId = grp.grpId AND (urg.hieId = ' . $hieId . ')')
                        ->innerJoin('HierarchieBundle:Groupe', 'grp1', 'WITH', 'grp1.grpId = gpfg.gpfIdpere')
                        ->where('gpfg.hieId = ' . $hieId)
                        ->orderBy('gpfg.grpLibelle', 'ASC')
                        ->orderBy('grp.grpLibelle', 'ASC')
                        ->getQuery()->getResult();
        $createList = $this->createExcelList($data, 'RACINE');
        $excelList = $this->exportGroupsExcel($createList);
        if (!empty($excelList)) {
            $rowNo = 1;
            // correct $rowNo when group condittion not meet
            $corectRowNo = 0;
            $uniqueGrps = array();
            // add rows to worksheet
            foreach ($excelList as $k => $val) {
                $cleaned = str_replace('childsGrp', '$id', $k);
                $elems = array_values(array_filter(explode('$id', $cleaned)));
                $no = count($elems);
                for ($i = 0; $i < $no; $i++) {
                    if (!in_array($elems[$no - 1], $uniqueGrps)) {
                        // set corect row number
                        $corectRowNo = $rowNo;
                        $uniqueGrps[$elems[$i]] = $elems[$i];
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue(chr(64 + $i + 1) . $rowNo, $elems[$i]);
                    }
                }
                $rowNo = $corectRowNo;
                $rowNo++;
            }

            $phpExcelObject->getActiveSheet()->setTitle($title);
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=' . $fileName . '.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
        $this->get('session')->getFlashBag()->add('myError', 'La hiérarchie n\'a pas de groupes.');
        return $this->redirect($this->generateUrl('hierarchie-exportation'));
    }

    /**
     * @Route("/hierarchie/exportation-utilisateur/", name="hierarchie-export-utilisateur")
     * @Method({"GET"})
     */
    public function exportUsersAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        $hieId = $request->query->get('id');
        $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findBy(array('hieId' => $hieId));
        // set excel variables
        $fileName = str_replace(' ', '_', $hierarchy[0]->getHieLibelle()) . '_groupesAndUser_' . date('d-m-Y');
        $createdBy = 'export program';
        $modifiedBy = 'export program';
        $title = substr($hierarchy[0]->getHieLibelle(), 0, 30);

        // create excel object
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator($createdBy)
                ->setLastModifiedBy($modifiedBy)
                ->setSubject($fileName)
                ->setTitle($title);
        // database query
        $data = $em->createQueryBuilder('exportList')
                        ->select('grp1.grpLibelle as parent', 'grp.grpLibelle as child', 'urg.usrMatricule', 'urg.rolId')
                        ->from('HierarchieBundle:GroupePereFils', 'gpfg')
                        ->leftJoin('HierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->leftJoin('HierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'urg.grpId = grp.grpId AND (urg.hieId = ' . $hieId . ')')
                        ->innerJoin('HierarchieBundle:Groupe', 'grp1', 'WITH', 'grp1.grpId = gpfg.gpfIdpere')
                        ->where('gpfg.hieId = ' . $hieId)
                        ->orderBy('gpfg.grpLibelle', 'ASC')
                        ->orderBy('grp.grpLibelle', 'ASC')
                        ->getQuery()->getResult();
        $createList = $this->createExcelList($data, 'RACINE');
        $excelList = $this->exportGroupsAndUsersExcel($createList);
        // remove from list if father is same as child
        foreach ($excelList as $index => $value) {
            $search = str_replace('$id', '', strrchr($index, '$id'));
            if ($search == substr($value, strpos($value, '$idchildsGrp$id'))) {
                unset($excelList[$index]);
            }
            if ($search == substr($value, strpos($value, '$idchildsGrp$id') + 15)) {
                unset($excelList[$index]);
            }
        }
        // end remove
        if (!empty($excelList)) {
            $j = 1;
            // add rows to worksheet
            foreach ($excelList as $k => $val) {
                $cleaned = str_replace('childsGrp', '$id', $k);
                $elems = array_values(array_filter(explode('$id', $cleaned)));
                $no = count($elems);
                for ($i = 0; $i < $no; $i++) {
                    if ($no > 1) {
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue(chr(64 + $i + 1) . $j, $elems[$i])
                                ->setCellValue(chr(64 + $no + 1) . $j, $val);
                    }
                }
                $j++;
            }
            $phpExcelObject->getActiveSheet()->setTitle($title);
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=' . $fileName . '.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
        $this->get('session')->getFlashBag()->add('myError', 'La hiérarchie n\'a pas de groupes.');
        return $this->redirect($this->generateUrl('hierarchie-exportation'));
    }

//        ini_set('xdebug.var_display_max_children', '200');
//        ini_set('xdebug.var_display_max_data', '200000');
//        ini_set('xdebug.var_display_max_depth', '200');
}
