<?php

namespace Administration\HierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UtilisateurController extends BaseController {

    /**
     * @Route("/utilisateur/", name="utilisateur")
     * @Method({"GET"})
     */
    public function userAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $nameList = $em->getRepository('HierarchieBundle:Utilisateur')->findAllUsers();
        $request = $this->getRequest();
        $userId = $request->query->get('user');
        return $this->render('@hierarchie/utilisateur/index.html.twig', array(
                    'userList' => $nameList,
                    'selected' => !empty($userId) ? $userId : null,
        ));
    }

    /**
     * @Route("/utilisateur/gerer")
     * @Method({"POST"})
     */
    public function manageAction() {
        $request = $this->getRequest();
        $serial = $request->request->get('serial');
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $user = $em->getRepository('HierarchieBundle:Utilisateur')->findOneBy(array('usrMatricule' => $serial));
        $exitDate = trim($user->getUsrDateSortie());
        return $this->render('@hierarchie/utilisateur/gerer.html.twig', array(
                    'serial' => $serial,
                    'isEmployee' => empty($exitDate) ? true : ( ($exitDate >= date('Ymd')) ? true : false )
        ));
    }

    /**
     * @Route("/utilisateur/utilisateur-attache-employe")
     * @Method({"POST"})
     */
    public function groupsToAdd() {
        $request = $this->getRequest();
        $serial = $request->request->get('serial');
        if (!empty($serial)) {
            $em = $this->getDoctrine()->getManager("hierarchie_bundle");
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findHierarchies($serial);
            return $this->render('@hierarchie/utilisateur/ajouter.html.twig', array(
                        'hierarchy' => $hierarchy,
                        'serial' => $serial
            ));
        }
        return new Response('');
    }

    /**
     * @Route("/utilisateur/groupe-a-ajouter")
     * @Method({"GET"})
     */
    public function getGroupToAdd() {
        $request = $this->getRequest();
        $hieId = (int) $request->query->get('hieId');
        $serial = $request->query->get('serial');
        if (!empty($hieId)) {
            $em = $this->getDoctrine()->getManager("hierarchie_bundle");
            $data = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsForHierarchy($hieId);

            $first = null;
            foreach ($data as $i => $row) {
                if ($row['gpfIdpere'] == 0) {
                    $first = $row['gpfIdpere'];
                }
            }
            if (!empty($data)) {
                $tree = $this->indentedTree($data, $first);
            }
            if (isset($tree[100])) {
                unset($tree[100]);
            }

            $usrGrp = $em->getRepository('HierarchieBundle:Hierarchie')->getUsrRoleGroup($serial, $hieId);

            return $this->render('@hierarchie/utilisateur/groupe.html.twig', array(
                        'list' => $tree,
                        'grpIn' => $usrGrp,
            ));
        }
        return new Response('Error');
    }

    /**
     * @Route("/utilisateur/ajouter-groupe", name="utilisateur-ajouter-groupe")
     * @Method({"POST"})
     */
    public function addGroup() {
        $request = $this->getRequest();
        $hieId = (int) $request->request->get('hieId');
        $groupId = (int) $request->request->get('groupId');
        $roleId = (int) $request->request->get('roleId');
        $serial = $request->request->get('serial');
        $urg = new \Administration\HierarchieBundle\Entity\UsrRoleGroupe();
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $user = $em->getRepository('HierarchieBundle:Utilisateur')->findOneBy(array('usrMatricule' => $serial));
        $group = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $groupId));
        $urgIn = $em->getRepository('HierarchieBundle:UsrRoleGroupe')->findOneBy(array(
            'usrMatricule' => $serial,
            'hieId' => $hieId
        ));
        $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $hieId));
        if (empty($urgIn) && !empty($hierarchy) && !empty($groupId) && !empty($roleId) && !empty($serial)) {
            $urg->setUsrMatricule($serial);
            $urg->setRolId($roleId);
            $urg->setGrpId($groupId);
            $urg->setHieId($hieId);
            $em->persist($urg);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' a bien été affecté au groupe ' . $group->getGrpLibelle() . '.');
        } else {
            $this->get('session')->getFlashBag()->add('myError', 'L\'affectation de ' . $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' au groupe ' . $group->getGrpLibelle() . ' a échoué. Vérifiez si cette utilisateur n\'est pas dans une autre groupe dans cette hiérarchie.');
        }
        return new Response('ok');
    }

    /**
     * @Route("/utilisateur/modifier")
     * @Method({"POST"})
     */
    public function modify() {
        $request = $this->getRequest();
        $serial = $request->request->get('serial');
        if (!empty($serial)) {
            $em = $this->getDoctrine()->getManager("hierarchie_bundle");
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->getHierachiesWhereUserIn($serial);
            $countHie = count($hierarchy);
            $selected = '';
            if ($countHie == 1) {
                $selected = 'selected';
            }
            return $this->render('@hierarchie/utilisateur/modifier.html.twig', array(
                        'hierarchy' => $hierarchy,
                        'serial' => $serial,
                        'selected' => $selected
            ));
        }
        return new Response('');
    }

    /**
     * @Route("/utilisateur/modifier-groupe")
     * @Method({"POST"})
     */
    public function modifyGroup() {
        $request = $this->getRequest();
        $hieId = (int) $request->request->get('hieId');
        $groupId = (int) $request->request->get('groupId');
        $roleId = (int) $request->request->get('roleId');
        $serial = $request->request->get('serial');
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $user = $em->getRepository('HierarchieBundle:Utilisateur')->findOneBy(array('usrMatricule' => $serial));
        $group = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $groupId));
        $urgIn = $em->getRepository('HierarchieBundle:UsrRoleGroupe')->findOneBy(array(
            'usrMatricule' => $serial,
            'hieId' => $hieId
        ));
        if (!empty($urgIn)) {
            $qb = $em->createQueryBuilder();
            $q = $qb->update('HierarchieBundle:UsrRoleGroupe', 'urg')
                    ->set('urg.rolId', '?1')
                    ->set('urg.grpId', '?2')
                    ->where('urg.usrMatricule = ?3')
                    ->andWhere('urg.hieId = ?4')
                    ->setParameter(1, $roleId)
                    ->setParameter(2, $groupId)
                    ->setParameter(3, $serial)
                    ->setParameter(4, $hieId)
                    ->getQuery();
            $p = $q->execute();
            $this->get('session')->getFlashBag()->add('success', 'La modification de l\'affectation de ' . $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' au groupe ' . $group->getGrpLibelle() . ' a bien été effectuée.');
        } else {
            $this->get('session')->getFlashBag()->add('myError', 'L\'affectation de ' . $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' au groupe ' . $group->getGrpLibelle() . ' a échoué.');
        }
        return new Response('ok');
    }

    /**
     * @Route("/utilisateur/supprimer")
     * @Method({"POST"})
     */
    public function delete() {
        $request = $this->getRequest();
        $serial = $request->request->get('serial');
        if (!empty($serial)) {
            $em = $this->getDoctrine()->getManager("hierarchie_bundle");
            $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->getHierachiesWhereUserIn($serial);
            $countHie = count($hierarchy);
            $selected = '';
            if ($countHie == 1) {
                $selected = 'selected';
            }
            return $this->render('@hierarchie/utilisateur/supprimer.html.twig', array(
                        'hierarchy' => $hierarchy,
                        'serial' => $serial,
                        'selected' => $selected
            ));
        }
        return new Response('');
    }

    /**
     * @Route("/utilisateur/groupe-a-supprimer")
     * @Method({"GET"})
     */
    public function getGroupToDelete() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $userDetails = $user = $em->getRepository('HierarchieBundle:Hierarchie')->getUsrRoleGroup($request->query->get('serial'), $request->query->get('hieId'));
        return new Response(json_encode($userDetails));
    }

    /**
     * @Route("/utilisateur/supprimer-groupe")
     * @Method({"POST"})
     */
    public function deleteGroup() {
        $request = $this->getRequest();
        $hieId = (int) $request->request->get('hieId');
        $groupId = (int) $request->request->get('grpId');
        $serial = $request->request->get('serial');
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $user = $em->getRepository('HierarchieBundle:Utilisateur')->findOneBy(array('usrMatricule' => $serial));
        $hie = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $hieId));

        $urgIn = $em->getRepository('HierarchieBundle:UsrRoleGroupe')->findOneBy(array(
            'usrMatricule' => $serial,
            'hieId' => $hieId
        ));

        if (!empty($urgIn)) {
            $deleteUrg = $em->createQuery("DELETE HierarchieBundle:UsrRoleGroupe urg WHERE urg.usrMatricule = '" . $serial . "' AND urg.hieId = " . $hieId);
            $deleteUrg->execute();
            $this->get('session')->getFlashBag()->add('success', 'L\'utilisateur ' . $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' a été enlevé du hierarchie ' . $hie->getHieLibelle() . '.');
        } else {
            $this->get('session')->getFlashBag()->add('myError', 'La suppression de l\'affectation de ' . $user->getUsrPrenom() . ' ' . $user->getUsrNom() . ' au groupe ' . $group->getGrpLibelle() . ' a échoué.');
        }
        return new Response('ok');
    }

    /**
     * @Route("/utilisateur/ajouter", name="utilisateur-ajouter")
     * @Method({"GET","POST"})
     */
    public function addUserAction(Request $request) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $user = new \Administration\HierarchieBundle\Entity\Utilisateur();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Utilisateur\UtilisateurType(), $user);
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $matricule = $em->getRepository('HierarchieBundle:Utilisateur')->countCustomMatricule();
                $user->setUsrMatricule($matricule);
                $user->setCreatedAt(new \Datetime());
                $user->setUpdatedAt(new \Datetime('0000-00-00 00:00:00'));
                $user->setDateDemandee(new \Datetime('0000-00-00 00:00:00'));
                $user->setDateAccepte(new \Datetime('0000-00-00 00:00:00'));
                $user->setDateCollab(new \Datetime('0000-00-00 00:00:00'));
                $user->setDateManager(new \Datetime('0000-00-00 00:00:00'));
                $user->setDateValider(new \Datetime('0000-00-00 00:00:00'));

                try {
                    $em->persist($user);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('myError', 'Error sur insert.');
                    return $this->render('@hierarchie/utilisateur/ajouter_utilisateur.html.twig', array(
                                'form' => $form->createView(),
                                'title' => 'Affecter un collaborateur'
                    ));
                }
                $this->get('session')->getFlashBag()->add('success', 'L\'utilisateur a été ajouté avec succès');
                return $this->redirect($this->generateUrl('utilisateur')."?user=".$user->getUsrMatricule());
            }
            $this->get('session')->getFlashBag()->add('myError', 'Formulaire contient des erreurs');
        }
        return $this->render('@hierarchie/utilisateur/ajouter_utilisateur.html.twig', array(
                    'form' => $form->createView(),
                    'title' => 'Ajouter un collaborateur'
        ));
    }

    /**
     * @Route("/utilisateur/editer/{id}/{allUsers}", name="utilisateur-editer", defaults={"id"=0, "allUsers"=0})
     * @Method({"GET","POST"})
     */
    public function editAction($id, $allUsers, Request $request) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        if ($allUsers == 0) {
            $users = $em->getRepository('HierarchieBundle:Utilisateur')->findAllActiveUsers();
        } else {
            $users = $em->getRepository('HierarchieBundle:Utilisateur')->findUsers();
        }
        $user = $em->getRepository('HierarchieBundle:Utilisateur')->find($id);
        // if empty user based on id inserted by the user then redirect to add function
        if (empty($user) && !empty($id)) {
            return $this->redirect($this->generateUrl('utilisateur-ajouter'));
        }
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Utilisateur\UtilisateurType(), $user);
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                try {
                    $em->persist($user);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('myError', 'Error sur insert.');
                    return $this->render('@hierarchie/utilisateur/utilisateur_liste.html.twig', array(
                                'users' => $users,
                                'form' => $form->createView(),
                                'id' => $id,
                                'isPandore' => is_numeric($id) ? 1 : 0,
                    ));
                }
                $this->get('session')->getFlashBag()->add('success', 'L\'utilisateur ' . $user->getUsrNom() . ' ' . $user->getUsrPrenom() . ' a été modifie avec succès');
                return $this->redirect($this->generateUrl('utilisateur-editer', array('id' => $id)));
            }
            $this->get('session')->getFlashBag()->add('myError', 'Formulaire contient des erreurs');
        }
        return $this->render('@hierarchie/utilisateur/utilisateur_liste.html.twig', array(
                    'users' => $users,
                    'form' => $form->createView(),
                    'id' => $id,
                    'isPandore' => is_numeric($id) ? 1 : 0,
        ));
    }

}
