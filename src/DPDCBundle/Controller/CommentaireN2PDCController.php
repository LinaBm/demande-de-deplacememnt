<?php

namespace DPDCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DPDCBundle\Entity\CommentaireN2Mois;
use \DateTime;
use DPDCBundle\Form\CommentaireN2PDCType;
use DPDCBundle\Form\CommentaireN2MoisType;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

class CommentaireN2PDCController extends Controller {

	/**
	 * @Route("commentaires-n2-mois-n1/{mois}/{N1}", name="dpdc_commentaires_n2_mois_n1", defaults={"mois"=NULL, "N1"=NULL})
	 * @Method({"GET", "POST"})
	 */
	public function commentaireN2PDCAction(Request $request, $mois, $N1) {
		$user = $this->getUser();
		$userId = $user->getId();
		$leMois = DateTime::createFromFormat('d-m-y H:i:s', '01-' . $mois . ' 00:00:00');
		if(!$leMois){
			$message = 'Pas de mois!';
			$this->get('session')->getFlashBag()->add('dpdcManagerSuccess', $message);
			return $this->redirect($this->generateUrl('dpdc_vision_n2', array('N2' => $userId)));
		}
		$today = new DateTime('now');
		if($leMois->format('m-y') !== $today->format('m-y')){
			$message = 'Pas le mois actuel!';
			$this->get('session')->getFlashBag()->add('dpdcManagerSuccess', $message);
			return $this->redirect($this->generateUrl('dpdc_vision_n2', array('N2' => $userId)));
		}
		$em = $this->getDoctrine()->getManager('default');
		$niveau1 = $em->getRepository('UserUserBundle:User')->find($N1);
		if(!$niveau1){
			$message = 'Pas de niveau1 valide!';
			$this->get('session')->getFlashBag()->add('dpdcManagerSuccess', $message);
			return $this->redirect($this->generateUrl('dpdc_vision_n2', array('N2' => $userId)));
		}
		$testProchaineMois = clone $leMois;
		$testProchaineMois->modify('+1 month');
		// GET PLANNED POINTS DE CONTROLE FOR GIVEN CONDITIONS
		$planifies = $this->getDoctrine()
			->getRepository('DPDCBundle:PointDeControle')
			->getPlanifiesMoisN1N2($mois, (int)$N1, $userId);
		// GET RESPONSES FOR PLANNED POINTS DE CONTROLE FOR GIVEN CONDITIONS
		$qb0 = $em->createQueryBuilder();
		$qb0->select('r')
			->from('\DPDCBundle\Entity\Resultat', 'r')
			->where('YEAR(r.date) = YEAR(:leMois)')
			->andWhere('MONTH(r.date) = MONTH(:leMois)')
			->andWhere('r.niveau1 = ' . (int)$N1)
			->andWhere('r.pointDeControle IN (:myVals)');
		$qb0->setParameter('leMois', $leMois, \Doctrine\DBAL\Types\Type::DATETIME);
		$qb0->setParameter('myVals', $planifies);
		$reponses = $qb0->getQuery()
							->getResult();
		// GET COMMENTAIRES FOR PLANNED POINTS DE CONTROLE FOR GIVEN CONDITIONS
		$qb = $em->createQueryBuilder();
		$qb->select('c')
			->from('\DPDCBundle\Entity\CommentaireN2Mois', 'c')
			->where('c.mois = :leMois')
			->andWhere('c.niveau1 = ' . (int)$N1)
			->andWhere('c.niveau2 = ' . $userId)
			->andWhere('c.pointDeControle IN (:myVals)');
		$qb->setParameter('leMois', $leMois, \Doctrine\DBAL\Types\Type::DATETIME);
		$qb->setParameter('myVals', $planifies);
		$commentaires = $qb->getQuery()
						->getResult();
		// REMOVE ALREADY RESPONDED PLANNED POINTS DE CONTROLE FOR GIVEN CONDITIONS
		foreach($reponses as $reponse){
			unset($planifies[$reponse->getPointDeControle()]);
		}
		$pourAjouter = $planifies;
		// REMOVE EXISTING COMMENTAIRES TO INSTANTIATE ONLY THE NEW ONES CONDITIONS
		foreach($commentaires as $comment){
			unset($pourAjouter[$comment->getPointDeControle()->getId()]);
		}
		foreach($pourAjouter as $idPDC){
			$nouveauCommentaire = new CommentaireN2Mois();
			$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($idPDC);
			$nouveauCommentaire->setPointDeControle($pointDeControle);
			$nouveauCommentaire->setMois($leMois);
			$nouveauCommentaire->setNiveau1($pointDeControle->getNiveau1());
			$nouveauCommentaire->setNiveau2($pointDeControle->getNiveau2());
			// ADD TO COMMENTAIRES NEWLY INSTANTIATED ONES
			$commentaires[$idPDC] = $nouveauCommentaire;
		}
		$forms=$this->createForm(new CommentaireN2MoisType(), $commentaires);
		$forms->handleRequest($request);
		if ($forms->isValid()) {
			for ($i=0;$i<count($commentaires);$i++){
				$commentaire= $forms["commentaire_".$i]->getData();
				if($forms["commentaire_".$i]['commentaire']->getData() !== null){
					$em = $this->getDoctrine()->getManager();
					$em->persist($commentaire);
					$em->flush();
				}else{
					$em->remove($commentaire);
					$em->flush();
				}
			}
			$message = 'Les commentaires sont bien enregistrés!';
			$this->get('session')->getFlashBag()->add('dpdcManagerSuccess', $message);
			return $this->redirect($this->generateUrl('dpdc_vision_n2', array('niveau2' => $userId)));
		}
		return $this->render('DPDCBundle:commentaireN2PDC:ajouterCommentaireN2Mois.html.twig', array(
				'leMois' => $leMois,
				'niveau1' => $niveau1,
				'niveau2' => $user,
				'forms' => $forms->createView(),
				'noCommentaires' => count($commentaires),
				'title' => 'Commentaires ' . $leMois->format('Y-m') . ', niveau2 ' . $user->getDisplayName() . ', niveau1 ' . $niveau1->getDisplayName()
		));
	}

}