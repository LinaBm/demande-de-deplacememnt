<?php

namespace DPDCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DPDCBundle\Entity\CommentaireN2Mois;
use DPDCBundle\Entity\PointDeControle;
use Symfony\Component\Validator\Constraints\GreaterThan;


class CommentaireN2MoisType extends AbstractType
{
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$i=0;
		foreach ($options['data'] as $commmentaire){			
			if(is_object($commmentaire)){
				$builder->add('commentaire_'.$i, new CommentaireN2PDCType(), array('data' => $commmentaire, 'label' => ' '));
				$i++;
			}
		}		
		$builder->add('save', 'submit', array(
//							'label' => 'Enregistrer',
							'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button')
						)
					)	
				->getForm();
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => null,
			'csrf_protection' => false
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'commentaire_n2_mois_type';
	}
}