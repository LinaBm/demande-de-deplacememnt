<?php

namespace DPDCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resultat
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DPDCBundle\Entity\CommentaireN2MoisRepository")
 * @ORM\Table(name="dpdc_comentaire_n2_mois"))
 */
class CommentaireN2Mois
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DPDCBundle\Entity\PointDeControle")
     * @ORM\JoinColumn(name="pointDeControle", referencedColumnName="id")
     */
    private $pointDeControle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mois", type="date", nullable=false)
     */
    private $mois;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="niveau1", referencedColumnName="id")
     **/
    private $niveau1;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="niveau2", referencedColumnName="id")
     **/
    private $niveau2;
	
    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdded", type="date", nullable=true)
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="date", nullable=true)
     */
    private $dateModified;

    /**
     * @var string
     *
     * @ORM\Column(name="addedBy", type="string", length=255, nullable=true)
     */
    private $addedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=255, nullable=true)
     */
    private $modifiedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pointDeControle = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveau1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveau2 = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pointDeControle
     *
     * @param integer $pointDeControle
     *
     * @return CommentaireN2Mois
     */
    public function setPointDeControle($pointDeControle)
    {
        $this->pointDeControle = $pointDeControle;

        return $this;
    }

    /**
     * Get pointDeControle
     *
     * @return integer
     */
    public function getPointDeControle()
    {
        return $this->pointDeControle;
    }

    /**
     * Set mois
     *
     * @param \DateTime $mois
     *
     * @return CommentaireN2Mois
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return \DateTime
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set niveau1
     *
     * @param User\UserBundle\Entity\User
     * @return CommentaireN2Mois
     */
    public function setNiveau1($niveau1)
    {
        $this->niveau1 = $niveau1;
        return $this;
    }

    /**
     * Get niveau1
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getNiveau1()
    {
        return $this->niveau1;
    }

    /**
     * Set niveau2
     *
     * @param User\UserBundle\Entity\User
     * @return CommentaireN2Mois
     */
    public function setNiveau2($niveau2)
    {
        $this->niveau2 = $niveau2;
        return $this;
    }

    /**
     * Get niveau2
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getNiveau2()
    {
        return $this->niveau2;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return CommentaireN2Mois
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return CommentaireN2Mois
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return CommentaireN2Mois
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set addedBy
     *
     * @param string $addedBy
     *
     * @return CommentaireN2Mois
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;
        return $this;
    }

    /**
     * Get addedBy
     *
     * @return string
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return CommentaireN2Mois
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }
}

