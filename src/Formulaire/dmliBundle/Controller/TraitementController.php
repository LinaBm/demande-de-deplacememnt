<?php

namespace Formulaire\dmliBundle\Controller;


use Formulaire\dmliBundle\Utils\BD;
use IOFactory;
use PHPExcel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class TraitementController extends Controller
{
    var $data;

    var $cheminDossier;

    var $nameFile;


    public function indexAction()
    {

        $request = $this->getRequest();
        if ($request->isMethod('POST')) {
            $this->TraitementDmli($_POST);

            $this->variablePrepare();

            $this->fileArchival();

            $this->createExcelFile();

            $this->sendMail();

            $this->sauvegarde();

            $message = 'Votre demande a bien été prise en compte. Vous allez recevoir une copie du mail envoyé au service informatique.';
        } else {
            $message = 'Veuillez remplir le formulaire';
        }
        return $this->container->get('templating')->renderResponse('dmliBundle:Default:Traitement.html.twig', array('msg' => $message)
        );
    }

    function TraitementDmli($donneesPost)
    {
        //On recupere les donn�es
        $this->data = $donneesPost;
    }

    function variablePrepare()
    {
        $em = $this->getDoctrine()->getManager();
        $oDB = new BD();
      //var_dump($this->data);die;
        //date de creation
        $this->dateDemande = date('d-m-Y');

//recuperation des libelles
        if ($this->data['formulaire_dmlibundle_admdirection']['societe'] >= 1) {
            $this->societeLib = $oDB->getSociete($this->data['formulaire_dmlibundle_admdirection']['societe'], $em);
            $this->societe = ($this->societeLib->getReference() . " - " . $this->societeLib->getLibelle()); //connection ligne societe ( id - libelle)
        } else {
            $this->societe = $this->data['userNewCompany'];
        }
        $this->direction = $oDB->getDirection($this->data['formulaire_dmlibundle_admdirection']['direction'], $em);
        $this->service = $oDB->getDepartment($this->data['formulaire_dmlibundle_admdirection']['service'], $em);

//nom prenom
        $this->data['userName'] = strtoupper($this->data['formulaire_dmlibundle_admdirection']['nom']);
        $this->data['userFirstname'] = strtolower($this->data['formulaire_dmlibundle_admdirection']['prenom']);
        $this->data['userFirstname'] = ucfirst($this->data['formulaire_dmlibundle_admdirection']['prenom']);
        $this->data['senderName'] = strtoupper($this->get('security.context')->getToken()->getUser()->getSurname());
        $this->data['senderFirstname'] = strtolower($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderFirstname'] = ucfirst($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $this->data['timeSlotStart'] = $this->data['debut'];
        $this->data['timeSlotEnd'] = $this->data['fin'];

        if (isset($this->data['formulaire_dmlibundle_admdirection']['posteNonAffect'])) {
            $this->pole = "NA";
            unset($this->data['mailing']);
            unset($this->data['mailingNew']);
            $this->poste = "";
        } else {
            // $this->pole = $oDB->getDivision($this->data['formulaire_dmlibundle_admdirection']['pole'],$em);
            //cas spec DAM
            if (isset($this->data['formulaire_dmlibundle_admdirection']['pole'])) {
                if ($this->data['formulaire_dmlibundle_admdirection']['pole'] >= 1) {

                    $this->pole = $oDB->getDivision($this->data['formulaire_dmlibundle_admdirection']['pole'], $em);
                } else {
                    $this->pole = $this->data['inputNewDivision'];
                }
            } else {
                $this->pole = "";
            }

            if (isset($this->data['formulaire_dmlibundle_admdirection']['poste'])) {

                if ($this->data['formulaire_dmlibundle_admdirection']['poste'] >= 1) {
                        $this->poste = $oDB->getPost($this->data['formulaire_dmlibundle_admdirection']['poste'], $em);
                    } else  if (($this->data['formulaire_dmlibundle_admdirection']['poste'] == 'STAGIAIRE') || ($this->data['formulaire_dmlibundle_admdirection']['poste'] == "APPRENTI(E)")) {
                    $this->poste = $this->data['formulaire_dmlibundle_admdirection']['poste'];


                } else {
                    $this->poste = $this->data['userNewPost'];
                  }

            }
            else {
                    $this->poste = $this->data['userNewPost'];
                }

            }

//fin cas DAM
            $em1 = $this->getDoctrine()->getEntityManager()->getConnection();
            if ((isset($this->data['formulaire_dmlibundle_admdirection']['crmProf'])) && ($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'] != '')) {
                $this->crmProfil = $oDB->getCrmProfil($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'], $em1);
                $this->crmFunction = $oDB->getCrmFunction($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'], $em1);
                $this->crmCategorie = $oDB->getCrmCategorie($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'], $em1);
                $this->crmTeam = $oDB->getCrmTeam($this->data['formulaire_dmlibundle_admdirection']['selectCrmTeam'], $em1);
            }

            if (isset($this->data['formulaire_dmlibundle_admdirection']['inputCodeUserKeyPerso'])) {
                if (($this->data['formulaire_dmlibundle_admdirection']['inputCodeUserKeyPerso'] != "")) {
                    $this->codeUserKeyPerso = $this->data['formulaire_dmlibundle_admdirection']['inputCodeUserKeyPerso'];
                    if ($this->codeUserKeyPerso == "AC") {
                        if ($this->data['formulaire_dmlibundle_admdirection']['selectCodeUserKeyPays'] != "") {
                            $this->codeUserClePays = $oDB->getDamClePays($this->data['formulaire_dmlibundle_admdirection']['selectCodeUserKeyPays'], $em1);
                        } else {
                            $this->codeUserClePays = "";
                        }
                        $this->codeUserCommentaire = $oDB->getDamComment($this->data['formulaire_dmlibundle_admdirection']['selectCodeUserComment'], $em1);
                    }
                }
                if (isset($this->data['formulaire_dmlibundle_admdirection']['selectCodeUserSSPole'])) {

                    $this->codeUserKeyPerso = $this->data['formulaire_dmlibundle_admdirection']['inputCodeUserKeyPerso'];

                    if ($this->data['formulaire_dmlibundle_admdirection']['inputCodeUserKeyPerso'] == "CP") {
                        $this->codeUserSSPole = $oDB->getDamSSPole($this->data['formulaire_dmlibundle_admdirection']['selectCodeUserSSPole'], $em1);
                    }
                }
            }


//Boucle pour recuperer les hardware
            /*$nbHardware = count($this->data['hardware']);

            for ($y = 0; $y <= $nbHardware; $y++) {
                $this->aHardware[$y] = $oDB->getHardware($this->data['hardware'][$y],$em1);
            }*/


            if (isset($this->data['mailing'])) {
                $y = 0;
                while (isset($this->data['mailing'][$y])) {
                    $this->aMailing[$y] = $oDB->getMail($this->data['mailing'][$y], $em1);
                    $this->aMailingId[$y] = $this->data['mailing'][$y];
                    $y++;
                }
                $z = 0;
                while (isset($this->data['mailingNew'][$z])) {
                    $this->aMailing[$y] = $this->data['mailingNew'][$z];
                    $this->aMailingId[$y] = "NULL";
                    $y++;
                    $z++;
                }
            }


            if (isset($this->data["nouvel"])) {
                $this->demande="Nouvel Arrivant";
                if (isset($this->data['hardware'])) {
                    $y = 0;
                    while (isset($this->data['hardware'][$y])) {

                        $this->aHardware[$y] = $oDB->getHardware($this->data['hardware'][$y], $em1);
                        $this->aHardware[$y]['qte'] = $this->data['qte'][$y];
                        $y++;
                    }
                }


//Boucle pour recuperer les software
                if (isset($this->data['software'])) {
                    $y = 0;
                    while (isset($this->data['software'][$y])) {
                        $this->aSoftware[$y] = $oDB->getSoftware($this->data['software'][$y], $em1);
                        $y++;
                    }
                }
                $this->couleur = array("0" => "366092", "1" => "FE0000", "2" => "92CDDC", "3" => "F24545", "4" => "EF9B9B");
            } else if (isset($this->data["mercato"])) {
                $this->demande="Mercato";
                if (isset($this->data['hardware'])) {
                    $y = 0;
                    while (isset($this->data['hardware'][$y])) {

                        $this->aHardware[$y] = $oDB->getHardware($this->data['hardware'][$y], $em1);
                        $this->aHardware[$y]['qte'] = $this->data['qte'][$y];
                        $y++;
                    }
                }
//Boucle pour recuperer les software
                if (isset($this->data['software'])) {
                    $y = 0;
                    while (isset($this->data['software'][$y])) {
                        $this->aSoftware[$y] = $oDB->getSoftware($this->data['software'][$y], $em1);
                        $y++;
                    }
                }
                $this->couleur = array("0" => "08B708", "1" => "FE0000", "2" => "B4F7B4", "3" => "F24545", "4" => "EF9B9B");
            } else if (isset($this->data["userGen"])) {
                $this->demande="Utilisateur Générique";
                $this->data['userName'] = '';
                $this->data['userFirstname']='';
                $this->couleur = array("0" => "F24545", "1" => "FE0000", "2" => "EF9B9B", "3" => "F24545", "4" => "EF9B9B");
                unset($this->data['software']);
                unset($this->data['hardware']);
            } else if (isset($this->data["renouvellement"])) {
                $this->demande="Renouvellement de matériel";

                if (isset($this->data['hardware'])) {
                    $y = 0;
                    while (isset($this->data['hardware'][$y])) {

                        $this->aHardware[$y] = $oDB->getHardware($this->data['hardware'][$y], $em1);
                        $this->aHardware[$y]['qte'] = $this->data['qte'][$y];
                        $y++;
                    }
                }


                $this->data['matricule'] = $this->data['formulaire_dmlibundle_admdirection']['matricule'];
//Boucle pour recuperer les software
                if (isset($this->data['software'])) {
                    $y = 0;
                    while (isset($this->data['software'][$y])) {
                        $this->aSoftware[$y] = $oDB->getSoftware($this->data['software'][$y], $em1);
                        $y++;
                    }
                }
                $this->couleur = array("0" => "F89900", "1" => "FE0000", "2" => "FBC670", "3" => "F24545", "4" => "EF9B9B");
            }else {
                $this->demande="Nouvel Arrivant";
                if (isset($this->data['hardware'])) {
                    $y = 0;
                    while (isset($this->data['hardware'][$y])) {

                        $this->aHardware[$y] = $oDB->getHardware($this->data['hardware'][$y], $em1);
                        $this->aHardware[$y]['qte'] = $this->data['qte'][$y];
                        $y++;
                    }
                }


//Boucle pour recuperer les software
                if (isset($this->data['software'])) {
                    $y = 0;
                    while (isset($this->data['software'][$y])) {
                        $this->aSoftware[$y] = $oDB->getSoftware($this->data['software'][$y], $em1);
                        $y++;
                    }
                }
                $this->couleur = array("0" => "366092", "1" => "FE0000", "2" => "92CDDC", "3" => "F24545", "4" => "EF9B9B");

            }

            $this->nameOfFile = 'DMLI ' . $this->direction . ' ' . $this->data['userName'] . ' ' . $this->data['userFirstname'] . ' ' . date('d-m-Y-His') . '.xls';
            $this->pathOfFile = 'uploads/DMLI/' . $this->direction . '/' . $this->service . '/' . $this->nameOfFile;

            $this->sendMailToAddData = 0;


        }

        function fileArchival()
        {

            //creation du repertoire de base
            if (@mkdir("uploads/DMLI", 0777, true)) ;

            //creation du repertoire de la dir
            if (@mkdir('uploads/DMLI/' . $this->direction, 0777, true)) ;

            //creation du repertoire du service
            if (@mkdir('uploads/DMLI/' . $this->direction . '/' . $this->service, 0777, true)) ;

        }

        function createExcelFile()
        {


            /* Cr�ation des objets de base et initialisation des informations d'ent�te */
            $oPHPExcel = new PHPExcel();
            $writer = \PHPExcel_IOFactory::createWriter($oPHPExcel, 'Excel5');

            /* File Propertie */
            $oPHPExcel->getProperties()->setCreator($this->data['senderFirstname'] . " " . $this->data['senderName']);
            $oPHPExcel->setActiveSheetIndex(0);

            $sheet = $oPHPExcel->getActiveSheet();

            //Re-size column & row (100x100)
            $rowNum = 1;
            $colNum = 'A';
            do {
                //Column
                $sheet->getRowDimension($rowNum)->setRowHeight(12.5);
                ++$rowNum;
                //Row
                $sheet->getColumnDimension($colNum)->setWidth(2.5);
                ++$colNum;

            } while ($rowNum <= 100);


// ## Generate Static Block (Title - Date - User - Sender) ## //



            $sheet = $this->generateBlockSender($sheet, $this);


            if (isset($this->data["nouvel"])) {
                $sheet = $this->generateBlockTitle($sheet);
                $sheet = $this->generateBlockUser($sheet, $this);
                $sheet = $this->generateBlockDate($sheet, $this->data);
            } else if (isset($this->data["mercato"])) {
                $sheet = $this->generateBlockTitle($sheet);
                $sheet = $this->generateBlockUser($sheet, $this);
                $sheet = $this->generateBlockDate($sheet, $this->data);
            } else if (isset($this->data["userGen"])) {
                $sheet = $this->generateBlockTitle($sheet);
                $sheet = $this->generateBlockUserGen($sheet, $this);
                $sheet = $this->generateBlockDate($sheet, $this->data);
            } else if (isset($this->data["renouvellement"])) {
                $sheet = $this->generateBlockTitlemAT($sheet);
                $sheet = $this->generateBlockMat($sheet, $this);
                $sheet = $this->generateBlockUser($sheet, $this);
                $sheet = $this->generateBlockDate($sheet, $this->data);
            }else{
                $sheet = $this->generateBlockTitle($sheet);
                $sheet = $this->generateBlockUser($sheet, $this);
                $sheet = $this->generateBlockDate($sheet, $this->data);
                $this->demande='Nouvel Arrivant';
            }

            $lineStart = 16;


// ## Generate  Block hardware et software ## //

            $saveLineStart = $lineStart;//variable de sauvegarde de la ligne de debut du block

            // compte le nombre de Hardware
            $nbLineMaxHard = 0;
            if (isset($this->data['hardware'])) {
                $nbLineMaxHard = count($this->data['hardware']) - 1;
            }

            // compte le nombre de Software
            $nbLineMaxSoft = 0;
            if (isset($this->data['software'])) {
                $nbLineMaxSoft = count($this->data['software']) - 1;
            }

            $nbLineMax = -1;

            // compare les deux resultats pour garder le plus �lev�
            if ($nbLineMaxSoft >= $nbLineMaxHard) {
                $nbLineMax = $nbLineMaxSoft;
            } else {
                $nbLineMax = $nbLineMaxHard;
            }


            if ($nbLineMax != -1) { //Ici le fichier est a la ligne 17

                //hardware
                if (isset($this->data['hardware'])) {
                    $this->generateBlockHardwareTitle($sheet);
                    $this->generateBlockHardwareHead($sheet);

                    for ($i = 0; $i <= $nbLineMaxHard; $i++) { //Boucle tant qu il y a des Hardware
                        $lineStart = 21 + $i;
                        $this->generateBlockHardwareCol($sheet, $lineStart, $i, $this);
                    }
                }

                //software
                if (isset($this->data['software'])) {
                    $this->generateBlockSoftwareTitle($sheet);
                    $this->generateBlockSoftwareHead($sheet);

                    for ($i = 0; $i <= $nbLineMaxSoft; $i++) {// boucle tant qu il y a des Software
                        $lineStart = 21 + $i;
                        $this->generateBlockSoftwareCol($sheet, $lineStart, $i, $this);
                    }
                }
            }


            $lineStart = $saveLineStart + $nbLineMax + 7; //$lineStart+1 pour sauter une ligne (1 pour la ligne vide + 1 pour ecrire dans la ligne de dessous


// ## Generate  Block listMail - Repository - Comment ## //

            $saveLineStart = $lineStart; //variable de sauvegarde de la ligne de debut du block

            // ** LISTMAIL ** //

            $lineStart = $saveLineStart; //reinitialisation du numero de ligne

            $nbLineMaxListMail = 0;
            // compte le nombre de lignes de listes de dif
            if (isset($this->data['mailing'])) {
                if (isset($this->data['mailingNew'])) {
                    $nbLineMaxListMail = count($this->data['mailingNew']) + count($this->data['mailing']);
                } else {
                    $nbLineMaxListMail = count($this->data['mailing']);
                }
            }
            $nbLineMaxListMail = $nbLineMaxListMail - 1;

            if ($nbLineMaxListMail >= 0) {
                $this->generateBlockMailTitle($sheet, $lineStart);
                $lineStart = $lineStart + 2;

                $this->generateBlockMailSubTitle($sheet, $lineStart, $this);
                $lineStart = $lineStart + 1;


                for ($i = 0; $i <= $nbLineMaxListMail; $i++) {

                    $this->generateBlockMailCol($sheet, $lineStart, $this, $i);
                    $lineStart = $lineStart + 1;
                }

            }


            // ** REPOSITORY ** //
            $lineStart = $saveLineStart; //reinitialisation du numero de ligne

            $nbLineMaxRepository = 0;

            // compte le nombre de lignes de listes de dif
            while (isset($this->data['mailing'][$nbLineMaxRepository])) {
                $nbLineMaxRepository = $nbLineMaxRepository + 1;
            }

            if ($nbLineMaxRepository > 0) {

            }


            // ** COMMENT ** //
            $lineStart = $saveLineStart; //reinitialisation du numero de ligne

            $nbLineMaxLineComment = 0;

            if (strlen($this->data['formulaire_dmlibundle_admdirection']['remarque']) != 0) {
                $this->generateBlockCommentTitle($sheet, $lineStart);

                $lineStart = $lineStart + 2;

                $a = 0;
                $b = 40;


                while ($a <= strlen($this->data['formulaire_dmlibundle_admdirection']['remarque'])) {
                    $a = $a + 40;
                    $nbLineMaxLineComment++;
                }

                //ajout le commentaire
                $this->generateBlockCommentLine($sheet, $lineStart, $this->data['formulaire_dmlibundle_admdirection']['remarque'], $nbLineMaxLineComment);

                $lineStart = $lineStart + $nbLineMaxLineComment + 1;

                $this->cellBorderLarge('AV' . ($lineStart) . ':BQ' . ($lineStart), 'top', $sheet); //bordure bas
            }


            //determine le debut du prochain block
            $lineStart = $saveLineStart + 2 + $this->calculNumEOT($nbLineMaxLineComment, $nbLineMaxRepository, $nbLineMaxListMail); //$saveLineStart prend la valeur du d�but du block precedent / 2 correspond a la hauteurs des titres du block precedent / calculNumEOT retourne la hauteur la plus grande des corps de tableau

            $saveLineStart = $lineStart + 3; //Saut de ligne

            $lineStart = $saveLineStart;


            // ## Generate  Block SAP - CRM - GLPI ## //

            // ** SAP ** //
            if (isset($this->data['formulaire_dmlibundle_admdirection']['sapProf'])) {

                $this->generateBlockSapTitle($sheet, $lineStart, $this);

                $lineStart = $lineStart + 2;

                $this->genrateBlockSapSubtitleProfil($sheet, $lineStart, $this);

                $lineStart = $lineStart + 1;

                $this->genrateBlockSapLineProfil($sheet, $lineStart, $this);

                $lineStart = $lineStart + 2;

                $this->generateBlockSapSubtitleComment($sheet, $lineStart, $this);

                $lineStart++;

                $a = 0;
                $firstline = $lineStart;

                while ($a <= strlen($this->data['formulaire_dmlibundle_admdirection']['textareaComment'])) {
                    $a = $a + 40;
                    $lineStart++;
                }

                $this->generateBlockSapComment($sheet, $firstline, $this->data['formulaire_dmlibundle_admdirection']['textareaComment'], $lineStart, $this);

                $this->cellBorderLarge('B' . ($lineStart + 1) . ':W' . ($lineStart + 1), 'top', $sheet); //bordure bas
            }


            $lineEndSap = $lineStart;
            $lineStart = $saveLineStart;


            // ** CRM ** //

            if (isset($this->data['formulaire_dmlibundle_admdirection']['crmProf'])) {

                $this->generateBlockCrmTitle($sheet, $lineStart, $this);
                if ($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'] != '') {
                    $lineStart = $lineStart + 2;

                    $this->generateBlockCrmProfilFonctionCat($sheet, $lineStart, $this);

                    $lineStart = $lineStart + 6;

                    $this->generateBlockCrmTeam($sheet, $lineStart, $this);
                    $lineStart = $lineStart + 2;
                }

            }


            $lineStart = $saveLineStart;


            // ** CODE SAP ** //

            if (isset($this->codeUserKeyPerso) && ($this->codeUserKeyPerso != "")) {
                $this->generateBlockCodeUserTitle($sheet, $lineStart);
                $lineStart = $lineStart + 2;
                $this->generateBlockCodeUserLine($sheet, $lineStart, $this);
            }
            $lineStart = $lineEndSap + 2;
            if (isset($this->data['formulaire_dmlibundle_admdirection']['cognosProf'])) {

                $this->generateBlockCognosTitle($sheet, $lineStart, $this);

                $lineStart = $lineStart + 2;

                $this->genrateBlockCognosSubtitleProfil($sheet, $lineStart, $this);

                $lineStart = $lineStart + 1;

                $this->genrateBlockCognosLineProfil($sheet, $lineStart, $this);

                $lineStart = $lineStart + 2;

                $this->generateBlockCognosSubtitleComment($sheet, $lineStart, $this);

                $lineStart++;

                $a = 0;
                $firstline = $lineStart;

                while ($a <= strlen($this->data['formulaire_dmlibundle_admdirection']['cognosComment'])) {
                    $a = $a + 40;
                    $lineStart++;
                }

                $this->generateBlockCognosComment($sheet, $firstline, $this->data['formulaire_dmlibundle_admdirection']['cognosComment'], $lineStart, $this);

                $this->cellBorderLarge('B' . ($lineStart + 1) . ':W' . ($lineStart + 1), 'top', $sheet); //bordure bas
            }


// ## SAVE ## //
            $writer->save(str_replace(__FILE__, 'uploads/DMLI/' . $this->direction . '/' . $this->service . '/' . $this->nameOfFile, __FILE__)); // ajouter chemin avec les dir et serv


            /********************************************************************************
             *                                 FUNCTIONS                                        *
             ********************************************************************************/
        }

// ** FILE ** //
        function generateBlockTitle($sheet)
        {
            //content
            $sheet->setCellValue('A1', 'DEMANDE DE MATERIEL ET/OU LOGICIELS INFORMATIQUES (DMLI)');

            //merge
            $sheet->mergeCells('A1:BR4');

            //front
            $this->cellStyleAlignmentCenter('A1', $sheet);
            $sheet->getStyle('A1')->getFont()->setBold(true)->setSize(11);


            //Image
            $objDrawing = new \PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('headerDmli');
            $objDrawing->setDescription('headerDmli');
            $objDrawing->setPath('images/headerDmliExcel.png');
            $objDrawing->setHeight(71);
            $objDrawing->setCoordinates('A1');
            $objDrawing->setOffsetX(250);
            $objDrawing->setWorksheet($sheet);


            return $sheet;
        }

        function generateBlockTitleMat($sheet)
        {
            //content
            $sheet->setCellValue('A1', 'DEMANDE DE MATERIEL ET/OU LOGICIELS INFORMATIQUES (DMLI)');

            //merge
            $sheet->mergeCells('A1:CH4');

            //front
            $this->cellStyleAlignmentCenter('A1', $sheet);
            $sheet->getStyle('A1')->getFont()->setBold(true)->setSize(11);


            //Image
            $objDrawing = new \PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('headerDmli');
            $objDrawing->setDescription('headerDmli');
            $objDrawing->setPath('images/headerDmliExcel1.png');
            $objDrawing->setHeight(71);
            $objDrawing->setCoordinates('A1');
            $objDrawing->setOffsetX(334);
            $objDrawing->setWorksheet($sheet);


            return $sheet;
        }

        function generateBlockDate($sheet, $data)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B6', 'Date de demande');
            $sheet->setCellValue('B7', date('d-m-Y'));

            $sheet->setCellValue('M6', 'Date de liv. souhaitee');
            $sheet->setCellValue('M7', $data['deliveryDate']);

            $sheet->setCellValue('X6', 'Type de demande');
            if (isset($this->demande)) {
                $sheet->setCellValue('X7', $this->demande);
            }
        else{$this->demande="Nouvel Arrivant";
                $sheet->setCellValue('X7', $this->demande);}


            // ## MERGE ## //
            $sheet->mergeCells('B6:J6');
            $sheet->mergeCells('B7:J8');

            $sheet->mergeCells('M6:U6');
            $sheet->mergeCells('M7:U8');

            $sheet->mergeCells('X6:AH6');
            $sheet->mergeCells('X7:AH8');


            // ## BORDER ## //
            $this->cellBorderLarge('B6:J6', 'top', $sheet);
            $this->cellBorderLarge('B7:J7', 'top', $sheet);
            $this->cellBorderLarge('B9:J9', 'top', $sheet);
            $this->cellBorderLarge('A6:A8', 'right', $sheet);
            $this->cellBorderLarge('J6:J8', 'right', $sheet);

            $this->cellBorderLarge('M6:U6', 'top', $sheet);
            $this->cellBorderLarge('M7:U7', 'top', $sheet);
            $this->cellBorderLarge('M9:U9', 'top', $sheet);
            $this->cellBorderLarge('U6:U8', 'right', $sheet);
            $this->cellBorderLarge('L6:L8', 'right', $sheet);

            $this->cellBorderLarge('X6:AH6', 'top', $sheet);
            $this->cellBorderLarge('X7:AH7', 'top', $sheet);
            $this->cellBorderLarge('X9:AH9', 'top', $sheet);
            $this->cellBorderLarge('AH6:AH8', 'right', $sheet);
            $this->cellBorderLarge('W6:W8', 'right', $sheet);

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B6', $sheet);
            $this->cellStyleAlignmentCenter('B7', $sheet);
            $this->cellStyleAlignmentCenter('M6', $sheet);
            $this->cellStyleAlignmentCenter('M7', $sheet);
            $this->cellStyleAlignmentCenter('X6', $sheet);
            $this->cellStyleAlignmentCenter('X7', $sheet);

            $this->cellColor('B6', $this->couleur["0"], $sheet);
            $this->cellColor('M6', $this->couleur["0"], $sheet);
            $this->cellColor('X6', $this->couleur["0"], $sheet);

            $sheet->getStyle('B6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            $sheet->getStyle('M6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            $sheet->getStyle('X6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockMat($sheet, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('BT6', 'Matricule');
            $sheet->setCellValue('BT7', $object->data['matricule']);


            // ## MERGE ## //
            $sheet->mergeCells('BT6:CH6');
            $sheet->mergeCells('BT7:CH8');


            // ## BORDER ## //
            $this->cellBorderLarge('BT6:CH6', 'top', $sheet);
            $this->cellBorderLarge('BT7:CH7', 'top', $sheet);
            $this->cellBorderLarge('BT9:CH9', 'top', $sheet);
            $this->cellBorderLarge('BS6:BS8', 'right', $sheet);
            $this->cellBorderLarge('CH6:CH8', 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('BT6', $sheet);
            $this->cellStyleAlignmentCenter('BT7', $sheet);


            $this->cellColor('BT6', $this->couleur["0"], $sheet);

            $sheet->getStyle('BT6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            return $sheet;
        }

        function generateBlockSender($sheet, $object)
        {
            //content
            $sheet->setCellValue('B10', 'Demandeur');
            $sheet->setCellValue('C12', $object->data['senderName']); //Name
            $sheet->setCellValue('S12', $object->data['senderFirstname']); //Firstname

            $sheet->setCellValue('B14', 'Mail');
            $sheet->setCellValue('B15', $object->data['senderMail']); //mail

            //merge
            $sheet->mergeCells('B10:AH11');
            $sheet->mergeCells('C12:Q13'); //Name
            $sheet->mergeCells('S12:AG13'); //Firstname

            $sheet->mergeCells('B14:AH14');
            $sheet->mergeCells('B15:AH16'); //Mail


            // ## BORDER ## //
            $this->cellBorderLarge('B10:AH10', 'top', $sheet);
            $this->cellBorderLarge('B12:AH12', 'top', $sheet);
            $this->cellBorderLarge('B14:AH14', 'top', $sheet);
            $this->cellBorderLarge('B15:AH15', 'top', $sheet);
            $this->cellBorderLarge('B17:AH17', 'top', $sheet);

            $this->cellBorderLarge('A10:A16', 'right', $sheet);
            $this->cellBorderLarge('AH10:AH16', 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B10', $sheet);
            $this->cellStyleAlignmentCenter('C12', $sheet);
            $this->cellStyleAlignmentCenter('S12', $sheet);
            $this->cellStyleAlignmentCenter('B14', $sheet);
            $this->cellStyleAlignmentCenter('B15', $sheet);

            $this->cellColor('B10', $this->couleur["0"], $sheet);
            $this->cellColor('B14', $this->couleur["2"], $sheet);

            $sheet->getStyle('B10')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            $sheet->getStyle('B14')->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function generateBlockUser($sheet, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('AK6', 'Utilisateur');
            $sheet->setCellValue('AK8', 'Nom - Prenom'); //subtitle

            $sheet->setCellValue('AL9', $object->data['userName']); //Name
            $sheet->setCellValue('BB9', $object->data['userFirstname']); //Firstname

            $sheet->setCellValue('AK11', 'Societe - Poste');

            if (($object->data['userNewCompany']) == '') {
                $sheet->setCellValue('AL12', $object->societe); //Soci�t�

            } else {
                $object->sendMailToAddData = 1;

                $sheet->setCellValue('AL12', $object->data['userNewCompany']);
                $sheet->getStyle('AL12')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
                $sheet = $this->adviseNewEntity($sheet, $object->sendMailToAddData);
            }

            $sheet->setCellValue('BA12', '-');

            if (($object->data['userNewPost']) == '') {
                $sheet->setCellValue('BB12', $object->poste); //Poste
            } else {
                $object->sendMailToAddData = 1;

                $sheet->setCellValue('BB12', $object->data['userNewPost']);
                $sheet->getStyle('BB12')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
                $sheet = $this->adviseNewEntity($sheet);
            }


            $sheet->setCellValue('AK14', 'Direction - Service - Pole');

            $sheet->setCellValue('AK15', $object->direction); //Direction
            $sheet->setCellValue('AU15', '-');
            $sheet->setCellValue('AV15', $object->service); //Service
            $sheet->setCellValue('BF15', '-');
            $sheet->setCellValue('BG15', $object->pole); //Pole

            // ## MERGE ## //
            $sheet->mergeCells('AK6:BQ7');
            $sheet->mergeCells('AK8:BQ8'); //subtitle
            $sheet->mergeCells('AL9:AZ10'); //Name
            $sheet->mergeCells('BB9:BP10'); //Firstname

            $sheet->mergeCells('AK11:BQ11');

            $sheet->mergeCells('AL12:AZ13'); //Soci�t�
            $sheet->mergeCells('BA12:BA13');
            $sheet->mergeCells('BB12:BP13'); //Poste

            $sheet->mergeCells('AK14:BQ14');

            $sheet->mergeCells('AK15:AT16'); //Direction
            $sheet->mergeCells('AU15:AU16');
            $sheet->mergeCells('AV15:BE16'); //Service
            $sheet->mergeCells('BF15:BF16');
            $sheet->mergeCells('BG15:BQ16'); //Pole


            // ## BORDER ## //
            $this->cellBorderLarge('AK6:BQ6', 'top', $sheet);
            $this->cellBorderLarge('AK8:BQ8', 'top', $sheet);
            $this->cellBorderLarge('AK9:BQ9', 'top', $sheet);
            $this->cellBorderLarge('AK11:BQ11', 'top', $sheet);
            $this->cellBorderLarge('AK12:BQ12', 'top', $sheet);
            $this->cellBorderLarge('AK14:BQ14', 'top', $sheet);
            $this->cellBorderLarge('AK15:BQ15', 'top', $sheet);
            $this->cellBorderLarge('AK17:BQ17', 'top', $sheet);
            $this->cellBorderLarge('AJ6:AJ16', 'right', $sheet);
            $this->cellBorderLarge('BQ6:BQ16', 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AK6', $sheet);//"Utilisateur"
            $this->cellStyleAlignmentCenter('Ak8', $sheet);//subtitle
            $this->cellStyleAlignmentCenter('AL9', $sheet);//Name
            $this->cellStyleAlignmentCenter('BB9', $sheet);//Firstname

            $this->cellStyleAlignmentCenter('AK11', $sheet);//"Soci�t� - Poste"
            $this->cellStyleAlignmentCenter('AL12', $sheet);//Society
            $this->cellStyleAlignmentCenter('BA12', $sheet);
            $this->cellStyleAlignmentCenter('BB12', $sheet);//Post

            $this->cellStyleAlignmentCenter('AK14', $sheet);//"Direction - Service - Pole"
            $this->cellStyleAlignmentCenter('AK15', $sheet);//Direction
            $this->cellStyleAlignmentCenter('AU15', $sheet);
            $this->cellStyleAlignmentCenter('AV15', $sheet);//Department
            $this->cellStyleAlignmentCenter('BF15', $sheet);
            $this->cellStyleAlignmentCenter('BG15', $sheet);//Division

            $this->cellColor('AK6', $this->couleur["0"], $sheet);
            $this->cellColor('AK8', $this->couleur["2"], $sheet);
            $this->cellColor('AK11', $this->couleur["2"], $sheet);
            $this->cellColor('AK14', $this->couleur["2"], $sheet);

            $sheet->getStyle('AK6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            $sheet->getStyle('AK8')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('AK11')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('AK14')->getFont()->setBold(true)->setItalic(true)->setSize(9);

            if (($object->data['inputNewDivision']) != '') {
                $sheet->getStyle('BG15')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
            }

            return $sheet;
        }

        function generateBlockUserGen($sheet, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('AK6', 'Utilisateur');

            $sheet->setCellValue('AK8', 'Societe - Poste');

            if (($object->data['userNewCompany']) == '') {
                $sheet->setCellValue('AL9', $object->societe); //Soci�t�

            } else {
                $object->sendMailToAddData = 1;

                $sheet->setCellValue('AL9', $object->data['userNewCompany']);
                $sheet->getStyle('AL9')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
                $sheet = $this->adviseNewEntity($sheet, $object->sendMailToAddData);
            }

            $sheet->setCellValue('BA8', '-');

            if (($object->data['userNewPost']) == '') {
                $sheet->setCellValue('BB9', $object->poste); //Poste
            } else {
                $object->sendMailToAddData = 1;

                $sheet->setCellValue('BB9', $object->data['userNewPost']);
                $sheet->getStyle('BB9')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
                $sheet = $this->adviseNewEntity($sheet);
            }


            $sheet->setCellValue('AK11', 'Direction - Service - Pole');

            $sheet->setCellValue('AK12', $object->direction); //Direction
            $sheet->setCellValue('AU12', '-');
            $sheet->setCellValue('AV12', $object->service); //Service
            $sheet->setCellValue('BF12', '-');
            $sheet->setCellValue('BG12', $object->pole); //Pole

            // ## MERGE ## //
            $sheet->mergeCells('AK6:BQ7');
            $sheet->mergeCells('AK8:BQ8'); //subtitle
            $sheet->mergeCells('AL9:AZ10'); //Name
            $sheet->mergeCells('BB9:BP10'); //Firstname

            $sheet->mergeCells('AK11:BQ11');

            $sheet->mergeCells('AK12:AT13'); //Direction
            $sheet->mergeCells('AU12:AU13');
            $sheet->mergeCells('AV12:BE13'); //Service
            $sheet->mergeCells('BF12:BF13');
            $sheet->mergeCells('BG12:BQ13'); //Poste


            //Pole


            // ## BORDER ## //
            $this->cellBorderLarge('AK6:BQ6', 'top', $sheet);
            $this->cellBorderLarge('AK8:BQ8', 'top', $sheet);
            $this->cellBorderLarge('AK9:BQ9', 'top', $sheet);
            $this->cellBorderLarge('AK11:BQ11', 'top', $sheet);
            $this->cellBorderLarge('AK12:BQ12', 'top', $sheet);
            $this->cellBorderLarge('AK14:BQ14', 'top', $sheet);
            $this->cellBorderLarge('AJ6:AJ13', 'right', $sheet);
            $this->cellBorderLarge('BQ6:BQ13', 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AK6', $sheet);//"Utilisateur"

            $this->cellStyleAlignmentCenter('AK8', $sheet);//"Soci�t� - Poste"
            $this->cellStyleAlignmentCenter('AL9', $sheet);//Society
            $this->cellStyleAlignmentCenter('BA9', $sheet);
            $this->cellStyleAlignmentCenter('BB9', $sheet);//Post

            $this->cellStyleAlignmentCenter('AK11', $sheet);//"Direction - Service - Pole"
            $this->cellStyleAlignmentCenter('AK12', $sheet);//Direction
            $this->cellStyleAlignmentCenter('AU12', $sheet);
            $this->cellStyleAlignmentCenter('AV12', $sheet);//Department
            $this->cellStyleAlignmentCenter('BF12', $sheet);
            $this->cellStyleAlignmentCenter('BG12', $sheet);//Division

            $this->cellColor('AK6', $this->couleur["0"], $sheet);
            $this->cellColor('AK8', $this->couleur["2"], $sheet);
            $this->cellColor('AK11', $this->couleur["2"], $sheet);


            $sheet->getStyle('AK6')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');
            $sheet->getStyle('AK8')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('AK11')->getFont()->setBold(true)->setItalic(true)->setSize(9);


            if (($object->data['inputNewDivision']) != '') {
                $sheet->getStyle('BG12')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
            }

            return $sheet;
        }

        function generateBlockHardwareTitle($sheet)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B18', 'Materiels demande');

            // ## MERGE ## //
            $sheet->mergeCells('B18:AH19'); //Merge pour la legend

            // ## BORDER ## //
            $this->cellBorderLarge('B18:AH18', 'top', $sheet);
            $this->cellBorderLarge('B20:AH20', 'top', $sheet);
            $this->cellBorderLarge('A18:A19', 'right', $sheet);
            $this->cellBorderLarge('AH18:AH19', 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B18', $sheet);

            $this->cellColor('B18', $this->couleur["0"], $sheet);

            $sheet->getStyle('B18')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;

        }

        function generateBlockHardwareHead($sheet)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B20', 'Libelle');
            $sheet->setCellValue('Z20', 'Qte');
            $sheet->setCellValue('AC20', 'Prix/U');

            // ## MERGE ## //
            $sheet->mergeCells('B20:Y20');
            $sheet->mergeCells('Z20:AB20');
            $sheet->mergeCells('AC20:AH20');

            // ## BORDER ## //
            $this->cellBorderLarge('B20:AH20', 'bottom', $sheet);

            $this->cellBorderLarge('A20', 'right', $sheet);
            $this->cellBorderLarge('Y20', 'right', $sheet);
            $this->cellBorderLarge('AB20', 'right', $sheet);
            $this->cellBorderLarge('AH20', 'right', $sheet);

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B20', $sheet);
            $this->cellStyleAlignmentCenter('Z20', $sheet);
            $this->cellStyleAlignmentCenter('AC20', $sheet);

            $this->cellColor('B20', $this->couleur["2"], $sheet);
            $this->cellColor('Z20', $this->couleur["2"], $sheet);
            $this->cellColor('AC20', $this->couleur["2"], $sheet);

            $sheet->getStyle('B20')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('Z20')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('AC20')->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function generateBlockHardwareCol($sheet, $lineStart, $i, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, $object->aHardware[$i]['har_libelle']); //name
            $sheet->setCellValue('Z' . $lineStart, $object->aHardware[$i]['qte']); //qty
            $sheet->setCellValue('AC' . $lineStart, $object->aHardware[$i]['har_prix']); //price/u

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':Y' . ($lineStart)); //name
            $sheet->mergeCells('Z' . ($lineStart) . ':AB' . ($lineStart)); //qty
            $sheet->mergeCells('AC' . ($lineStart) . ':AH' . ($lineStart)); //price/u


            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':AH' . ($lineStart), 'bottom', $sheet); //bordure du bas

            $this->cellBorderLarge('A' . ($lineStart), 'right', $sheet);
            $this->cellBorderLarge('Y' . ($lineStart), 'right', $sheet);
            $this->cellBorderLarge('AB' . ($lineStart), 'right', $sheet);
            $this->cellBorderLarge('AH' . ($lineStart), 'right', $sheet);


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('Z' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('AC' . $lineStart, $sheet);

            $sheet->getStyle('B' . $lineStart)->getFont()->setSize(10);
            $sheet->getStyle('Z' . $lineStart)->getFont()->setSize(10);
            $sheet->getStyle('AC' . $lineStart)->getFont()->setSize(10);

            return $sheet;
        }

        function generateBlockSoftwareTitle($sheet)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('AK18', 'Logiciels a installer');

            // ## MERGE ## //
            $sheet->mergeCells('AK18:BQ19'); //Merge pour la legend list

            // ## BORDER ## //
            $this->cellBorderLarge('AK18:BQ18', 'top', $sheet);
            $this->cellBorderLarge('AK20:BQ20', 'top', $sheet);
            $this->cellBorderLarge('AJ18:AJ19', 'right', $sheet);
            $this->cellBorderLarge('BQ18:BQ19', 'right', $sheet);

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AK18', $sheet);

            $this->cellColor('AK18', $this->couleur["0"], $sheet);

            $sheet->getStyle('AK18')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockSoftwareHead($sheet)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('AK20', 'Libelle');
            //$sheet->setCellValue('BI20', 'Qte');
            $sheet->setCellValue('BI20', 'Prix/U');

            // ## MERGE ## //
            $sheet->mergeCells('AK20:BH20');
            $sheet->mergeCells('BI20:BQ20');
            // $sheet->mergeCells('BL20:BQ20');

            // ## BORDER ## //
            $this->cellBorderLarge('AK20:BQ20', 'bottom', $sheet);

            $this->cellBorderLarge('AJ20', 'right', $sheet);
            $this->cellBorderLarge('BH20', 'right', $sheet);
            // $this->cellBorderLarge('BK20', 'right', $sheet);
            $this->cellBorderLarge('BQ20', 'right', $sheet);

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AK20', $sheet);
            $this->cellStyleAlignmentCenter('BI20', $sheet);
            //  $this->cellStyleAlignmentCenter('BL20', $sheet);

            $this->cellColor('AK20', $this->couleur["2"], $sheet);
            $this->cellColor('BI20', $this->couleur["2"], $sheet);
            //  $this->cellColor('BL20', $this->couleur["2"], $sheet);

            $sheet->getStyle('AK20')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('BI20')->getFont()->setBold(true)->setItalic(true)->setSize(9);
            // $sheet->getStyle('BL20')->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function generateBlockSoftwareCol($sheet, $lineStart, $i, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('AK' . $lineStart, $object->aSoftware[$i]['sft_libelle']); //name
            //$sheet->setCellValue('BI' . $lineStart, ''); //qty
            $sheet->setCellValue('BI' . $lineStart, $object->aSoftware[$i]['sft_prix']); //price/u


            // ## MERGE ## //
            $sheet->mergeCells('AK' . ($lineStart) . ':BH' . ($lineStart)); //name
            $sheet->mergeCells('BI' . ($lineStart) . ':BQ' . ($lineStart)); //qty
            // $sheet->mergeCells('BL' . ($lineStart) . ':BQ' . ($lineStart)); //price/u


            // ## BORDER ## //
            $this->cellBorderLarge('AK' . ($lineStart) . ':BQ' . ($lineStart), 'bottom', $sheet); //bordure du bas

            $this->cellBorderLarge('AJ' . ($lineStart), 'right', $sheet);
            $this->cellBorderLarge('BH' . ($lineStart), 'right', $sheet);
            //$this->cellBorderLarge('BK' . ($lineStart), 'right', $sheet);
            $this->cellBorderLarge('BQ' . ($lineStart), 'right', $sheet);

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AK' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('BI' . $lineStart, $sheet);
            // $this->cellStyleAlignmentCenter('BL' . $lineStart, $sheet);

            $sheet->getStyle('AK' . $lineStart)->getFont()->setSize(10);
            $sheet->getStyle('BI' . $lineStart)->getFont()->setSize(10);
            //  $sheet->getStyle('BL' . $lineStart)->getFont()->setSize(10);

            return $sheet;
        }

        function generateBlockMailTitle($sheet, $lineStart)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . ($lineStart), 'Liste de diffusion');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart + 1) . ':W' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $this->cellColor('B' . $lineStart, $this->couleur["0"], $sheet);
            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockMailSubTitle($sheet, $lineStart, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . ($lineStart), 'Plage :');
            $sheet->setCellValue('H' . ($lineStart), $object->data['timeSlotStart']);
            $sheet->setCellValue('O' . ($lineStart), '-');
            $sheet->setCellValue('Q' . ($lineStart), $object->data['timeSlotEnd']);

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':G' . ($lineStart));
            $sheet->mergeCells('H' . ($lineStart) . ':N' . ($lineStart));
            $sheet->mergeCells('O' . ($lineStart) . ':P' . ($lineStart));
            $sheet->mergeCells('Q' . ($lineStart) . ':W' . ($lineStart));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('H' . ($lineStart) . ':H' . ($lineStart + 1), 'left', $sheet); //bordure gauche
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('H' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('O' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('Q' . $lineStart, $sheet);

            $this->cellColor('B' . $lineStart, $this->couleur["2"], $sheet);
            $this->cellColor('H' . $lineStart, $this->couleur["2"], $sheet);
            $this->cellColor('O' . $lineStart, $this->couleur["2"], $sheet);
            $this->cellColor('Q' . $lineStart, $this->couleur["2"], $sheet);

            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);
            $sheet->getStyle('O' . $lineStart)->getFont()->setBold(true)->setSize(10);

            return $sheet;
        }

        function generateBlockMailCol($sheet, $lineStart, $object, $i)
        {

            // ## CONTENT ## //

            $sheet->setCellValue('B' . $lineStart, $object->aMailing[$i]);


            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart));


            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart), 'left', $sheet); //bordure gauche


            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);


            return $sheet;
        }

        function generateBlockRepositoryTitle($sheet, $lineStart)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('Y' . ($lineStart), 'Repertoire et Groupe de partage');

            // ## MERGE ## //
            $sheet->mergeCells('Y' . ($lineStart) . ':AT' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart) . ':Y' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('AU' . ($lineStart) . ':AU' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('Y' . $lineStart, $sheet);
            $this->cellColor('Y' . $lineStart, $this->couleur["0"], $sheet);
            $sheet->getStyle('Y' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockRepositoryCol($sheet, $lineStart, $data)
        {
            //content
            $sheet->setCellValue('AX' . $lineStart, 'listes des repertoires : en dev');
            //merge
            $sheet->mergeCells('AX' . ($lineStart) . ':BW' . ($lineStart + 1)); //Merge pour la legend list
            //border
            $styleA11 = $sheet->getStyle('AX14:BW14');
            $styleA11->applyFromArray(array(
                'borders' => array(
                    'top' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
            $styleA11 = $sheet->getStyle('BW14:BW' . ($lineStart + 1));
            $styleA11->applyFromArray(array(
                'borders' => array(
                    'right' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));

            //front
            $sheet->getStyle('AX' . ($lineStart))->applyFromArray(array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

            return $sheet;
        }

        function generateBlockCommentTitle($sheet, $lineStart)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('AV' . ($lineStart), 'Remarque');

            // ## MERGE ## //
            $sheet->mergeCells('AV' . ($lineStart) . ':BQ' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('AV' . ($lineStart) . ':BQ' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('AV' . ($lineStart + 1) . ':BQ' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('AV' . ($lineStart) . ':AV' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('BR' . ($lineStart) . ':BR' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AV' . $lineStart, $sheet);
            $this->cellColor('AV' . $lineStart, $this->couleur["0"], $sheet);
            $sheet->getStyle('AV' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockCommentLine($sheet, $lineStart, $ligne, $nbLineMaxLineComment)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('AV' . ($lineStart), $ligne);

            // ## MERGE ## //
            $sheet->mergeCells('AV' . ($lineStart) . ':BQ' . ($lineStart + $nbLineMaxLineComment));

            // ## BORDER ## //
            $this->cellBorderLarge('AV' . ($lineStart) . ':AV' . ($lineStart + $nbLineMaxLineComment), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('BR' . ($lineStart) . ':BR' . ($lineStart + $nbLineMaxLineComment), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('AV' . $lineStart, $sheet);

            $sheet->getStyle('AV' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
            $sheet->getStyle('AV' . $lineStart)->getAlignment()->setWrapText(true);


        }

        function generateBlockSapTitle($sheet, $lineStart, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'SAP');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart + 1) . ':W' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);

            if (isset($object->data['formulaire_dmlibundle_admdirection']['sapUserGen'])) {
                $sheet->setCellValue('B' . $lineStart, 'SAP - Utilisateur Generique');
                $this->cellColor('B' . $lineStart, 'F24545', $sheet);
            } else {
                $this->cellColor('B' . $lineStart, $this->couleur["0"], $sheet);
            }

            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;

        }

        function genrateBlockSapSubtitleProfil($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'Profil a dupliquer');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);

            if (isset($object->data['formulaire_dmlibundle_admdirection']['sapUserGen'])) {
                $this->cellColor('B' . $lineStart, 'EF9B9B', $sheet);

            } else {
                $this->cellColor('B' . $lineStart, $this->couleur["2"], $sheet);
            }

            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function genrateBlockSapLineProfil($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, $object->data['formulaire_dmlibundle_admdirection']['inputProfilSap']);

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $sheet->getStyle('B' . $lineStart)->getFont()->setSize(10);

            return $sheet;
        }

        function generateBlockSapSubtitleComment($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'Remarque');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);

            if (isset($object->data['formulaire_dmlibundle_admdirection']['sapUserGen'])) {
                $this->cellColor('B' . $lineStart, 'EF9B9B', $sheet);

            } else {
                $this->cellColor('B' . $lineStart, $this->couleur["2"], $sheet);
            }

            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function generateBlockSapComment($sheet, $firstLine, $ligne, $maxLine, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . ($firstLine), $ligne);

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($firstLine) . ':W' . ($maxLine));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($firstLine) . ':B' . ($maxLine), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($firstLine) . ':X' . ($maxLine), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $firstLine, $sheet);

            //couleur rouge
            $sheet->getStyle('B' . $firstLine)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);


            $sheet->getStyle('B' . $firstLine)->getAlignment()->setWrapText(true);

        }

        function generateBlockCognosTitle($sheet, $lineStart, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'Cognos');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart + 1) . ':W' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);

            $this->cellColor('B' . $lineStart, $this->couleur["0"], $sheet);


            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;

        }

        function genrateBlockCognosSubtitleProfil($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'Profil a dupliquer');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);

            $this->cellColor('B' . $lineStart, $this->couleur["2"], $sheet);

            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function genrateBlockCognosLineProfil($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, $object->data['formulaire_dmlibundle_admdirection']['cognosProfil']);

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $sheet->getStyle('B' . $lineStart)->getFont()->setSize(10);

            return $sheet;
        }

        function generateBlockCognosSubtitleComment($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('B' . $lineStart, 'Remarque');

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($lineStart) . ':W' . ($lineStart));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('B' . ($lineStart) . ':W' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('B' . ($lineStart) . ':B' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($lineStart) . ':X' . ($lineStart), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $lineStart, $sheet);
            $this->cellColor('B' . $lineStart, $this->couleur["2"], $sheet);
            $sheet->getStyle('B' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

            return $sheet;
        }

        function generateBlockCognosComment($sheet, $firstLine, $ligne, $maxLine, $object)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('B' . ($firstLine), $ligne);

            // ## MERGE ## //
            $sheet->mergeCells('B' . ($firstLine) . ':W' . ($maxLine));

            // ## BORDER ## //
            $this->cellBorderLarge('B' . ($firstLine) . ':B' . ($maxLine), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('X' . ($firstLine) . ':X' . ($maxLine), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('B' . $firstLine, $sheet);

            //couleur rouge
            $sheet->getStyle('B' . $firstLine)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);


            $sheet->getStyle('B' . $firstLine)->getAlignment()->setWrapText(true);

        }

        function generateBlockCrmTitle($sheet, $lineStart, $object)
        {
            ## CONTENT ## //
            $sheet->setCellValue('Y' . $lineStart, 'CRM');

            ## MERGE ## //
            $sheet->mergeCells('Y' . ($lineStart) . ':AT' . ($lineStart + 1));

            ## BORDER ## //
            $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart) . ':Y' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('AU' . ($lineStart) . ':AU' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            ## FRONT ## //
            $this->cellStyleAlignmentCenter('Y' . $lineStart, $sheet);

            if (isset($object->data['formulaire_dmlibundle_admdirection']['crmUserGen'])) {
                $sheet->setCellValue('Y' . $lineStart, 'CRM - Utilisateur Generique');
                $this->cellColor('Y' . $lineStart, 'F24545', $sheet);
            } else {
                $this->cellColor('Y' . $lineStart, $this->couleur["0"], $sheet);
            }

            $sheet->getStyle('Y' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;

        }

        function generateBlockCrmProfilFonctionCat($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('Y' . $lineStart, 'Profil');
            $sheet->setCellValue('Y' . ($lineStart + 2), 'Fonction');
            $sheet->setCellValue('Y' . ($lineStart + 4), 'Catégorie');

            $sheet->setCellValue('AE' . $lineStart, $object->crmProfil);
            $sheet->setCellValue('AE' . ($lineStart + 2), $object->crmFunction);
            $sheet->setCellValue('AE' . ($lineStart + 4), $object->crmCategorie);

            // ## MERGE ## //
            $sheet->mergeCells('Y' . ($lineStart) . ':AD' . ($lineStart + 1));
            $sheet->mergeCells('Y' . ($lineStart + 2) . ':AD' . ($lineStart + 3));
            $sheet->mergeCells('Y' . ($lineStart + 4) . ':AD' . ($lineStart + 5));

            $sheet->mergeCells('AE' . ($lineStart) . ':AT' . ($lineStart + 1));
            $sheet->mergeCells('AE' . ($lineStart + 2) . ':AT' . ($lineStart + 3));
            $sheet->mergeCells('AE' . ($lineStart + 4) . ':AT' . ($lineStart + 5));

            // ## BORDER ## //
            $this->cellBorderLarge('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart + 3) . ':AT' . ($lineStart + 3), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart + 5) . ':AT' . ($lineStart + 5), 'bottom', $sheet); //bordure bas

            $this->cellBorderLarge('Y' . ($lineStart) . ':Y' . ($lineStart + 5), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('AE' . ($lineStart) . ':AE' . ($lineStart + 5), 'left', $sheet); //bordure centre
            $this->cellBorderLarge('AU' . ($lineStart) . ':AU' . ($lineStart + 5), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            //legend
            $this->cellStyleAlignmentCenter('Y' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('Y' . ($lineStart + 2), $sheet);
            $this->cellStyleAlignmentCenter('Y' . ($lineStart + 4), $sheet);


            if (isset($object->data['formulaire_dmlibundle_admdirection']['crmUserGen'])) {
                $this->cellColor('Y' . $lineStart, 'EF9B9B', $sheet);
                $this->cellColor('Y' . ($lineStart + 2), 'EF9B9B', $sheet);
                $this->cellColor('Y' . ($lineStart + 4), 'EF9B9B', $sheet);
            } else {
                $this->cellColor('Y' . $lineStart, $this->couleur["2"], $sheet);
                $this->cellColor('Y' . ($lineStart + 2), $this->couleur["2"], $sheet);
                $this->cellColor('Y' . ($lineStart + 4), $this->couleur["2"], $sheet);
            }

            $sheet->getStyle('Y' . $lineStart)->getFont()->setBold(true)->setSize(10);
            $sheet->getStyle('Y' . ($lineStart + 2))->getFont()->setBold(true)->setSize(10);
            $sheet->getStyle('Y' . ($lineStart + 4))->getFont()->setBold(true)->setSize(10);

            //info
            $this->cellStyleAlignmentCenter('AE' . $lineStart, $sheet);
            $this->cellStyleAlignmentCenter('AE' . ($lineStart + 2), $sheet);
            $this->cellStyleAlignmentCenter('AE' . ($lineStart + 4), $sheet);

            $sheet->getStyle('AE' . $lineStart)->getFont()->setBold(true)->setSize(10);
            $sheet->getStyle('AE' . ($lineStart + 2))->getFont()->setBold(true)->setSize(10);
            $sheet->getStyle('AE' . ($lineStart + 4))->getFont()->setBold(true)->setSize(10);


            return $sheet;

        }

        function generateBlockCrmTeam($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('Y' . $lineStart, 'Nom de l equipe');
            $sheet->setCellValue('Y' . ($lineStart + 1), $object->crmTeam);

            // ## MERGE ## //
            $sheet->mergeCells('Y' . ($lineStart) . ':AT' . ($lineStart));
            $sheet->mergeCells('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 3));

            // ## BORDER ## //
            $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart) . ':Y' . ($lineStart), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('AU' . ($lineStart) . ':AU' . ($lineStart), 'left', $sheet); //bordure gauche

            $this->cellBorderLarge('Y' . ($lineStart + 3) . ':AT' . ($lineStart + 3), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('Y' . ($lineStart + 1) . ':Y' . ($lineStart + 3), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('AU' . ($lineStart + 1) . ':AU' . ($lineStart + 3), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            $this->cellStyleAlignmentCenter('Y' . $lineStart, $sheet);

            if (isset($object->data['formulaire_dmlibundle_admdirection']['crmUserGen'])) {
                $this->cellColor('Y' . $lineStart, 'EF9B9B', $sheet);
            } else {
                $this->cellColor('Y' . $lineStart, $this->couleur["2"], $sheet);
            }

            $sheet->getStyle('Y' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

            $this->cellStyleAlignmentCenter('Y' . ($lineStart + 1), $sheet);
            $sheet->getStyle('Y' . ($lineStart + 1))->getFont()->setSize(10);

            return $sheet;

        }

        /* function generateBlockCrmSector($sheet, $lineStart, $object)
         {

             // ## CONTENT ## //
             $sheet->setCellValue('Y' . $lineStart, utf8_encode('Secteur / Famille'));
             $sheet->setCellValue('Y' . ($lineStart + 1), utf8_encode(utf8_encode($object->data['formulaire_dmlibundle_admdirection']['crmSecteur'])));

             // ## MERGE ## //
             $sheet->mergeCells('Y' . ($lineStart) . ':AT' . ($lineStart));
             $sheet->mergeCells('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 2));

             // ## BORDER ## //
             $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'top', $sheet); //bordure haut
             $this->cellBorderLarge('Y' . ($lineStart) . ':AT' . ($lineStart), 'bottom', $sheet); //bordure bas
             $this->cellBorderLarge('Y' . ($lineStart) . ':Y' . ($lineStart), 'left', $sheet); //bordure droite
             $this->cellBorderLarge('AU' . ($lineStart) . ':AU' . ($lineStart), 'left', $sheet); //bordure gauche

             $this->cellBorderLarge('Y' . ($lineStart + 1) . ':AT' . ($lineStart + 2), 'bottom', $sheet); //bordure bas
             $this->cellBorderLarge('Y' . ($lineStart + 1) . ':Y' . ($lineStart + 2), 'left', $sheet); //bordure droite
             $this->cellBorderLarge('AU' . ($lineStart + 1) . ':AU' . ($lineStart + 2), 'left', $sheet); //bordure gauche

             // ## FRONT ## //
             $this->cellStyleAlignmentCenter('Y' . $lineStart, $sheet);

             if ($object->data['formulaire_dmlibundle_admdirection']['crmUserGen'] == 1) {
                 $this->cellColor('Y' . $lineStart, 'EF9B9B', $sheet);
             } else {
                 $this->cellColor('Y' . $lineStart, $this->couleur["2"], $sheet);
             }

             $sheet->getStyle('Y' . $lineStart)->getFont()->setBold(true)->setItalic(true)->setSize(9);

             $this->cellStyleAlignmentCenter('Y' . ($lineStart + 1), $sheet);
             $sheet->getStyle('Y' . ($lineStart + 1))->getFont()->setSize(10);

             return $sheet;

         }*/
        function generateBlockCodeUserTitle($sheet, $lineStart)
        {
            ## CONTENT ## //
            $sheet->setCellValue('AV' . $lineStart, 'CODE SAP');

            ## MERGE ## //
            $sheet->mergeCells('AV' . ($lineStart) . ':BQ' . ($lineStart + 1));

            ## BORDER ## //
            $this->cellBorderLarge('AV' . ($lineStart) . ':BQ' . ($lineStart), 'top', $sheet); //bordure haut
            $this->cellBorderLarge('AV' . ($lineStart + 1) . ':BQ' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('AV' . ($lineStart) . ':AV' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('BR' . ($lineStart) . ':BR' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            ## FRONT ## //
            $this->cellStyleAlignmentCenter('AV' . $lineStart, $sheet);
            $this->cellColor('AV' . $lineStart, $this->couleur["0"], $sheet);
            $sheet->getStyle('AV' . $lineStart)->getFont()->setBold(true)->setSize(10)->getColor()->setRGB('FEFEFE');

            return $sheet;
        }

        function generateBlockCodeUserLine($sheet, $lineStart, $object)
        {

            // ## CONTENT ## //
            $sheet->setCellValue('AV' . $lineStart, 'Type Personnel');
            $sheet->setCellValue('BB' . $lineStart, $object->codeUserKeyPerso);

            // ## MERGE ## //
            $sheet->mergeCells('AV' . ($lineStart) . ':BA' . ($lineStart + 1));
            $sheet->mergeCells('BB' . ($lineStart) . ':BQ' . ($lineStart + 1));

            // ## BORDER ## //
            $this->cellBorderLarge('AV' . ($lineStart + 1) . ':BQ' . ($lineStart + 1), 'bottom', $sheet); //bordure bas
            $this->cellBorderLarge('AV' . ($lineStart) . ':AV' . ($lineStart + 1), 'left', $sheet); //bordure droite
            $this->cellBorderLarge('BB' . ($lineStart) . ':BB' . ($lineStart + 1), 'left', $sheet); //bordure centre
            $this->cellBorderLarge('BR' . ($lineStart) . ':BR' . ($lineStart + 1), 'left', $sheet); //bordure gauche

            // ## FRONT ## //
            //legend
            $this->cellStyleAlignmentCenter('AV' . $lineStart, $sheet);
            $this->cellColor('AV' . ($lineStart), $this->couleur["2"], $sheet);
            $sheet->getStyle('AV' . ($lineStart))->getFont()->setBold(true)->setSize(10);

            //info
            $this->cellStyleAlignmentCenter('BB' . ($lineStart), $sheet);
            $sheet->getStyle('BB' . ($lineStart))->getFont()->setSize(10);

            // Acheteur
            if ($object->codeUserKeyPerso == "AC") {

                $sheet->setCellValue('AV' . ($lineStart + 2), 'Clé Pays');
                $sheet->setCellValue('AV' . ($lineStart + 4), 'Commentaire');


                $sheet->setCellValue('BB' . ($lineStart + 2), $object->codeUserClePays);
                $sheet->setCellValue('BB' . ($lineStart + 4), $object->codeUserCommentaire);

                // ## MERGE ## //
                $sheet->mergeCells('AV' . ($lineStart + 2) . ':BA' . ($lineStart + 3));
                $sheet->mergeCells('AV' . ($lineStart + 4) . ':BA' . ($lineStart + 5));


                $sheet->mergeCells('BB' . ($lineStart + 2) . ':BQ' . ($lineStart + 3));
                $sheet->mergeCells('BB' . ($lineStart + 4) . ':BQ' . ($lineStart + 5));

                // ## BORDER ## //
                $this->cellBorderLarge('AV' . ($lineStart + 3) . ':BQ' . ($lineStart + 3), 'bottom', $sheet); //bordure bas
                $this->cellBorderLarge('AV' . ($lineStart + 5) . ':BQ' . ($lineStart + 5), 'bottom', $sheet); //bordure bas


                $this->cellBorderLarge('AV' . ($lineStart + 2) . ':AV' . ($lineStart + 5), 'left', $sheet); //bordure droite
                $this->cellBorderLarge('BB' . ($lineStart + 2) . ':BB' . ($lineStart + 5), 'left', $sheet); //bordure centre
                $this->cellBorderLarge('BR' . ($lineStart + 2) . ':BR' . ($lineStart + 5), 'left', $sheet); //bordure gauche


                // ## FRONT ## //
                //legend
                $this->cellStyleAlignmentCenter('AV' . ($lineStart + 2), $sheet);
                $this->cellStyleAlignmentCenter('AV' . ($lineStart + 4), $sheet);


                $this->cellColor('AV' . ($lineStart + 2), $this->couleur["2"], $sheet);
                $this->cellColor('AV' . ($lineStart + 4), $this->couleur["2"], $sheet);


                $sheet->getStyle('AV' . ($lineStart + 2))->getFont()->setBold(true)->setSize(10);
                $sheet->getStyle('AV' . ($lineStart + 4))->getFont()->setBold(true)->setSize(10);


                //info
                $this->cellStyleAlignmentCenter('BB' . ($lineStart + 2), $sheet);
                $this->cellStyleAlignmentCenter('BB' . ($lineStart + 4), $sheet);


                $sheet->getStyle('BB' . ($lineStart + 2))->getFont()->setSize(10);
                $sheet->getStyle('BB' . ($lineStart + 4))->getFont()->setSize(10);

            } // Chef produit
            elseif ($object->codeUserKeyPerso == 'CP') {
                $sheet->setCellValue('AV' . ($lineStart + 2), 'Pole');
                $sheet->setCellValue('AV' . ($lineStart + 4), 'Sous Pole');

                $sheet->setCellValue('BB' . ($lineStart + 2), $object->pole);
                $sheet->setCellValue('BB' . ($lineStart + 4), $object->codeUserSSPole);

                // ## MERGE ## //
                $sheet->mergeCells('AV' . ($lineStart + 2) . ':BA' . ($lineStart + 3));
                $sheet->mergeCells('AV' . ($lineStart + 4) . ':BA' . ($lineStart + 5));

                $sheet->mergeCells('BB' . ($lineStart + 2) . ':BQ' . ($lineStart + 3));
                $sheet->mergeCells('BB' . ($lineStart + 4) . ':BQ' . ($lineStart + 5));

                // ## BORDER ## //
                $this->cellBorderLarge('AV' . ($lineStart + 3) . ':BQ' . ($lineStart + 3), 'bottom', $sheet); //bordure bas
                $this->cellBorderLarge('AV' . ($lineStart + 5) . ':BQ' . ($lineStart + 5), 'bottom', $sheet); //bordure bas

                $this->cellBorderLarge('AV' . ($lineStart + 2) . ':AV' . ($lineStart + 5), 'left', $sheet); //bordure droite
                $this->cellBorderLarge('BB' . ($lineStart + 2) . ':BB' . ($lineStart + 5), 'left', $sheet); //bordure centre
                $this->cellBorderLarge('BR' . ($lineStart + 2) . ':BR' . ($lineStart + 5), 'left', $sheet); //bordure gauche


                // ## FRONT ## //
                //legend
                $this->cellStyleAlignmentCenter('AV' . ($lineStart + 2), $sheet);
                $this->cellStyleAlignmentCenter('AV' . ($lineStart + 4), $sheet);

                $this->cellColor('AV' . ($lineStart + 2), $this->couleur["2"], $sheet);
                $this->cellColor('AV' . ($lineStart + 4), $this->couleur["2"], $sheet);

                $sheet->getStyle('AV' . ($lineStart + 2))->getFont()->setBold(true)->setSize(10);
                $sheet->getStyle('AV' . ($lineStart + 4))->getFont()->setBold(true)->setSize(10);

                //info
                $this->cellStyleAlignmentCenter('BB' . ($lineStart + 2), $sheet);
                $this->cellStyleAlignmentCenter('BB' . ($lineStart + 4), $sheet);

                $sheet->getStyle('BB' . ($lineStart + 2))->getFont()->setSize(10);
                $sheet->getStyle('BB' . ($lineStart + 4))->getFont()->setSize(10);
            }

            return $sheet;

        }

        function adviseNewEntity($sheet)
        {
            // ## CONTENT ## //
            $sheet->setCellValue('BS11', 'Attention : L element present sur cette DMLI vient d etre saisie par l utilisateur.');
            $sheet->setCellValue('BS12', 'Il est possible qu il ne soit pas renseigne dans la base de donnee.');
            $sheet->setCellValue('BS13', 'Contactez le pole web afin de l inserer dans la base.');

            // ## FRONT ## //
            $sheet->getStyle('BS11')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
            $sheet->getStyle('BS12')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);
            $sheet->getStyle('BS13')->getFont()->setBold(true)->setSize(10)->getColor()->setRGB($this->couleur["1"]);

            return $sheet;
        }

        function calculNumEOT($numLine1, $numLine2, $numLine3)
        {

            //trouve le plus grand des trois et le renvoit
            if ($numLine1 < $numLine2) {

                if ($numLine2 < $numLine3) {
                    $lineStart = $numLine3;
                } else {
                    $lineStart = $numLine2;
                }
            } elseif ($numLine1 < $numLine3) {

                $lineStart = $numLine3;

            } else {
                $lineStart = $numLine1;
            }
            return $lineStart;
        }

        function cellBorderLarge($cells, $location, $sheet)
        {
            $styleA11 = $sheet->getStyle($cells);
            $styleA11->applyFromArray(array(
                'borders' => array(
                    $location => array(
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM))));
        }

        function cellStyleAlignmentCenter($cells, $sheet)
        {
            $sheet->getStyle($cells)->applyFromArray(array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
        }

        function cellColor($cells, $color, $sheet)
        {
            global $objPHPExcel;
            $sheet->getStyle($cells)->getFill()
                ->applyFromArray(array('type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => $color)
                ));
        }

        function sendMail()
        {
            $message = \Swift_Message::newInstance()
                ->setSubject("DMLI " .$this->direction ." (".$this->demande.") : " . $this->data['userFirstname'] . " " . $this->data['userName'] . " - du " . $this->dateDemande . " | pour le " . $this->data['deliveryDate'])
                ->setFrom('tester.dmli@gmail.com')
                ->setTo($this->data['senderMail'])
                ->setCc('lina.benmohamed@ozeol.com')
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'dmliBundle:email:dmliMail.html.twig', array('nameuser' => $this->data['userName'], 'firstnameuser' => $this->data['userFirstname'], 'date' => $this->dateDemande)
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($this->pathOfFile));
            $this->get('mailer')->send($message);

            /*
            if( svp()){
                    $mailMessage = \Swift_Message::newInstance()
                        ->setContentType('text/html')
                        ->setSubject("DMLI ".$this->direction." (".$this->demande.") : ".$this->data['userFirstname']." ".$this->data['userName']." - du ".$this->dateDemande." | pour le ".$this->data['deliveryDate'])
                        ->setFrom($this->data['senderMail'])
                        ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
                        ->setCC($this->data['senderMail'].", svp.informatique@noz.fr")
                        ->setBCC('poleweb@noz.fr')
                        ->setBody(
                            $this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                                'dmliBundle:email:dmliMail.html.twig',array('nameuser'=> $this->data['userName'], 'firstnameuser'=>$this->data['userFirstname'], 'date'=> $this->dateDemande)
                            ),
                            'text/html'
                        )
                        ->attach(\Swift_Attachment::fromPath($this->pathOfFile));}
            else{
            $mailMessage = \Swift_Message::newInstance()
                        ->setContentType('text/html')
                        ->setSubject("DMLI ".$this->direction." (".$this->demande.") : ".$this->data['userFirstname']." ".$this->data['userName']." - du ".$this->dateDemande." | pour le ".$this->data['deliveryDate'])
                        ->setFrom($this->data['senderMail'])
                        ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
                        ->setCC($this->data['senderMail'])
                        ->setBCC('poleweb@noz.fr')
                        ->setBody(
                            $this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                                'dmliBundle:email:dmliMail.html.twig',array('nameuser'=> $this->data['userName'], 'firstnameuser'=>$this->data['userFirstname'], 'date'=> $this->dateDemande)
                            ),
                            'text/html'
                        )
                        ->attach(\Swift_Attachment::fromPath($this->pathOfFile));
            }
                    $reponse=$this->get('mailer')->send($mailMessage);
            */
        }


        function sauvegarde()
        {
            $em1 = $this->getDoctrine()->getEntityManager()->getConnection();
            $oDB = new BD();

            $dateDemande = date('Y-m-d');


            $dateLivraison = $this->data['deliveryDate'];

            //pole
            if (isset($this->data['formulaire_dmlibundle_admdirection']['pole'])) {
                $pole = $this->data['formulaire_dmlibundle_admdirection']['pole'];
            } else(
            $pole = "NULL"
            );

            //poste
            if ($this->data['userNewPost'] == '') {
                $postestag = $this->data['formulaire_dmlibundle_admdirection']['poste'];
                if ($postestag == "STAGIAIRE" || $postestag == "APPRENTI(E)") {
                    $posteId = "NULL";
                    $posteLibelle = $this->data['formulaire_dmlibundle_admdirection']['poste'];
                } else {
                    $posteId = $this->data['formulaire_dmlibundle_admdirection']['poste'];
                    $posteLibelle = "NULL";
                }
            } else {
                $posteId = "NULL";
                $posteLibelle = $this->data['userNewPost'];
            }

            //societe
            if ($this->data['userNewCompany'] == '') {
                $societeId = $this->data['formulaire_dmlibundle_admdirection']['societe'];
                $societeLibelle = "NULL";
            } else {
                $societeId = "NULL";
                $societeLibelle = $this->data['userNewCompany'];
            }

            //SAP
            if (isset($this->data['formulaire_dmlibundle_admdirection']['sapProf'])) {
                //sap profil
                if ($this->data['formulaire_dmlibundle_admdirection']['inputProfilSap'] != '') {
                    $sapPorfil = $this->data['formulaire_dmlibundle_admdirection']['inputProfilSap'];
                } else {
                    $sapPorfil = "NULL";
                }

                //sap remarque
                if ($this->data['formulaire_dmlibundle_admdirection']['textareaComment'] != '') {
                    $sapRemarque = $this->data['formulaire_dmlibundle_admdirection']['textareaComment'];
                } else {
                    $sapRemarque = "NULL";
                }
            } else {
                $sapPorfil = "NULL";
                $sapRemarque = "NULL";
            }

            //Cognos
            if (isset($this->data['formulaire_dmlibundle_admdirection']['cognosProf'])) {
                //cognos profil
                if ($this->data['formulaire_dmlibundle_admdirection']['cognosProfil'] != '') {
                    $cognosPorfil = $this->data['formulaire_dmlibundle_admdirection']['cognosProfil'];
                } else {
                    $cognosPorfil = "NULL";
                }

                //sap remarque
                if ($this->data['formulaire_dmlibundle_admdirection']['cognosComment'] != '') {
                    $cognosRemarque = $this->data['formulaire_dmlibundle_admdirection']['cognosComment'];
                } else {
                    $cognosRemarque = "NULL";
                }
            } else {
                $cognosPorfil = "NULL";
                $cognosRemarque = "NULL";
            }

            //CRM
            if (isset($this->data['formulaire_dmlibundle_admdirection']['crmProf'])) {
                //crm equipe
                if ($this->data['formulaire_dmlibundle_admdirection']['selectCrmTeam'] != '') {
                    $crmEqu = $this->data['formulaire_dmlibundle_admdirection']['selectCrmTeam'];
                } else {
                    $crmEqu = "NULL";
                }

                //crm profil
                if ($this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'] != '') {
                    $crmProfil = $this->data['formulaire_dmlibundle_admdirection']['selectCrmProfil'];
                } else {
                    $crmProfil = "NULL";
                }
            } else {
                $crmEqu = "NULL";
                $crmProfil = "NULL";
            }

            //remarque
            if ($this->data['formulaire_dmlibundle_admdirection']['remarque'] != '') {
                $remarque = $this->data['formulaire_dmlibundle_admdirection']['remarque'];
            } else {
                $remarque = "NULL";
            }


            $aDemande = array($dateDemande,
                $dateLivraison,
                $this->data['senderName'],
                $this->data['senderFirstname'],
                $this->data['senderMail'],
                $this->data['userName'],
                $this->data['userFirstname'],
                $this->data['formulaire_dmlibundle_admdirection']['direction'],
                $this->data['formulaire_dmlibundle_admdirection']['service'],
                $pole,
                $posteId,
                $posteLibelle,
                $societeId,
                $societeLibelle,
                $sapPorfil,
                $sapRemarque,
                $crmEqu,
                $crmProfil,
                $remarque,
                $cognosPorfil,
                $cognosRemarque,
            );

            $oDB->insertDemande($aDemande, $em1);

            $demId = $oDB->getLastId('dem_id', 'dmli_save_demande_dem', $em1);


            // ## MAIL ## //
            if (isset($this->aMailing)) {
                $nbLineMaxListMail = count($this->aMailing);

                $nbLineMaxListMail = $nbLineMaxListMail - 1;

                if ($nbLineMaxListMail > -1) {
                    for ($i = 0; $i <= $nbLineMaxListMail; $i++) {
                        $oDB->insertDemandeMail($this->aMailing[$i], $this->aMailingId[$i], $demId, $em1);
                    }
                }
            }

            // ## SOFTWARE ## //
            if (isset($this->data['software'])) {
                $nbLineMaxSoft = count($this->data['software']);

                $nbLineMaxSoft = $nbLineMaxSoft - 1;

                if ($nbLineMaxSoft > -1) {
                    for ($i = 0; $i <= $nbLineMaxSoft; $i++) {
                        $oDB->insertDemandeSoftware($this->data['software'][$i], $demId, $em1);
                    }
                }
            }

            // ## HARDWARE ## //
            if (isset($this->data['hardware'])) {
                $nbLineMaxHard = count($this->data['hardware']);

                $nbLineMaxHard = $nbLineMaxHard - 1;

                if ($nbLineMaxHard > -1) {
                    for ($i = 0; $i <= $nbLineMaxHard; $i++) {
                        $oDB->insertDemandeHardware($this->data['hardware'][$i], $demId, $em1);
                    }
                }
            }


        }

        function svp()
        {
            if (isset($this->data['hardware'])) {
                $em1 = $this->getDoctrine()->getEntityManager()->getConnection();
                $nbLineMaxHard = count($this->data['hardware']);

                $nbLineMaxHard = $nbLineMaxHard - 1;

                if ($nbLineMaxHard > -1) {
                    for ($i = 0; $i <= $nbLineMaxHard; $i++) {
                        $sql = "  SELECT * from dmli_hardware_har where har_svp = 1 AND har_id= " . $this->data['hardware'][$i] . "
        ;";
                        // execute request
                        $query = $em1->prepare($sql);
                        $query->execute();
                        $result = $query->fetchAll();
                        if (count($result) > 0) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }

