<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliCrmprofilCrmPro
 *
 * @ORM\Table(name="dmli_crmprofil_crm_pro", indexes={@ORM\Index(name="fk_dmli_crmprofil_crm_pro_dmli_crmfonction_crm_fon1_idx", columns={"crm_fon_id"}), @ORM\Index(name="fk_dmli_crmprofil_crm_pro_dmli_crmcategorie_crm_cat1_idx", columns={"crm_cat_id"})})
 * @ORM\Entity
 */
class DmliCrmprofilCrmPro
{
    /**
     * @var string
     *
     * @ORM\Column(name="crm_pro_libelle", type="string", length=70, nullable=true)
     */
    private $crmProLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_pro_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $crmProId;

    /**
     * @var \Formulaire\dmliBundle\Entity\DmliCrmfonctionCrmFon
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\DmliCrmfonctionCrmFon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crm_fon_id", referencedColumnName="crm_fon_id")
     * })
     */
    private $crmFon;

    /**
     * @var \Formulaire\dmliBundle\Entity\DmliCrmcategorieCrmCat
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\DmliCrmcategorieCrmCat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crm_cat_id", referencedColumnName="crm_cat_id")
     * })
     */
    private $crmCat;


    /**
     * Set crmProLibelle
     *
     * @param string $crmProLibelle
     *
     * @return DmliCrmprofilCrmPro
     */
    public function setCrmProLibelle($crmProLibelle)
    {
        $this->crmProLibelle = $crmProLibelle;
    
        return $this;
    }

    /**
     * Get crmProLibelle
     *
     * @return string
     */
    public function getCrmProLibelle()
    {
        return $this->crmProLibelle;
    }

    /**
     * Get crmProId
     *
     * @return integer
     */
    public function getCrmProId()
    {
        return $this->crmProId;
    }

    /**
     * Set crmFon
     *
     * @param \Formulaire\dmliBundle\Entity\DmliCrmfonctionCrmFon $crmFon
     *
     * @return DmliCrmprofilCrmPro
     */
    public function setCrmFon(\Formulaire\dmliBundle\Entity\DmliCrmfonctionCrmFon $crmFon = null)
    {
        $this->crmFon = $crmFon;
    
        return $this;
    }

    /**
     * Get crmFon
     *
     * @return \Formulaire\dmliBundle\Entity\DmliCrmfonctionCrmFon
     */
    public function getCrmFon()
    {
        return $this->crmFon;
    }

    /**
     * Set crmCat
     *
     * @param \Formulaire\dmliBundle\Entity\DmliCrmcategorieCrmCat $crmCat
     *
     * @return DmliCrmprofilCrmPro
     */
    public function setCrmCat(\Formulaire\dmliBundle\Entity\DmliCrmcategorieCrmCat $crmCat = null)
    {
        $this->crmCat = $crmCat;
    
        return $this;
    }

    /**
     * Get crmCat
     *
     * @return \Formulaire\dmliBundle\Entity\DmliCrmcategorieCrmCat
     */
    public function getCrmCat()
    {
        return $this->crmCat;
    }
}

