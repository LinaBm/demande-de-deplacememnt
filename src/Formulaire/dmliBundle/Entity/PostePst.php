<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostePst
 *
 * @ORM\Table(name="poste_pst")
 * @ORM\Entity
 */
class PostePst
{
    /**
     * @var string
     *
     * @ORM\Column(name="pst_libelle", type="string", length=70, nullable=false)
     */
    private $pstLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="pst_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pstId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliDampersonnelDamPrs", mappedBy="pst")
     */
    private $damPrs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->damPrs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pstLibelle
     *
     * @param string $pstLibelle
     *
     * @return PostePst
     */
    public function setPstLibelle($pstLibelle)
    {
        $this->pstLibelle = $pstLibelle;
    
        return $this;
    }

    /**
     * Get pstLibelle
     *
     * @return string
     */
    public function getPstLibelle()
    {
        return $this->pstLibelle;
    }

    /**
     * Get pstId
     *
     * @return integer
     */
    public function getPstId()
    {
        return $this->pstId;
    }

    /**
     * Add damPr
     *
     * @param \Formulaire\dmliBundle\Entity\DmliDampersonnelDamPrs $damPr
     *
     * @return PostePst
     */
    public function addDamPr(\Formulaire\dmliBundle\Entity\DmliDampersonnelDamPrs $damPr)
    {
        $this->damPrs[] = $damPr;
    
        return $this;
    }

    /**
     * Remove damPr
     *
     * @param \Formulaire\dmliBundle\Entity\DmliDampersonnelDamPrs $damPr
     */
    public function removeDamPr(\Formulaire\dmliBundle\Entity\DmliDampersonnelDamPrs $damPr)
    {
        $this->damPrs->removeElement($damPr);
    }

    /**
     * Get damPrs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDamPrs()
    {
        return $this->damPrs;
    }
}

