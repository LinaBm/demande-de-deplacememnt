<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliDamsspolDamSsp
 *
 * @ORM\Table(name="dmli_damsspol_dam_ssp", indexes={@ORM\Index(name="fk_dmli_damsspol_dam_ssp_pole_pol1_idx", columns={"pol_id"})})
 * @ORM\Entity
 */
class DmliDamsspolDamSsp
{
    /**
     * @var string
     *
     * @ORM\Column(name="dam_ssp_libelle", type="string", length=45, nullable=false)
     */
    private $damSspLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dam_ssp_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $damSspId;

    /**
     * @var \Formulaire\dmliBundle\Entity\PolePol
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\PolePol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pol_id", referencedColumnName="pol_id")
     * })
     */
    private $pol;


    /**
     * Set damSspLibelle
     *
     * @param string $damSspLibelle
     *
     * @return DmliDamsspolDamSsp
     */
    public function setDamSspLibelle($damSspLibelle)
    {
        $this->damSspLibelle = $damSspLibelle;
    
        return $this;
    }

    /**
     * Get damSspLibelle
     *
     * @return string
     */
    public function getDamSspLibelle()
    {
        return $this->damSspLibelle;
    }

    /**
     * Get damSspId
     *
     * @return integer
     */
    public function getDamSspId()
    {
        return $this->damSspId;
    }

    /**
     * Set pol
     *
     * @param \Formulaire\dmliBundle\Entity\PolePol $pol
     *
     * @return DmliDamsspolDamSsp
     */
    public function setPol(\Formulaire\dmliBundle\Entity\PolePol $pol = null)
    {
        $this->pol = $pol;
    
        return $this;
    }

    /**
     * Get pol
     *
     * @return \Formulaire\dmliBundle\Entity\PolePol
     */
    public function getPol()
    {
        return $this->pol;
    }
}

