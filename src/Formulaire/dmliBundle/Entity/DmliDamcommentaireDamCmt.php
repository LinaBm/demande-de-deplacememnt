<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliDamcommentaireDamCmt
 *
 * @ORM\Table(name="dmli_damcommentaire_dam_cmt")
 * @ORM\Entity
 */
class DmliDamcommentaireDamCmt
{
    /**
     * @var string
     *
     * @ORM\Column(name="dam_cmt_libelle", type="string", length=45, nullable=false)
     */
    private $damCmtLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dam_cmt_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $damCmtId;


    /**
     * Set damCmtLibelle
     *
     * @param string $damCmtLibelle
     *
     * @return DmliDamcommentaireDamCmt
     */
    public function setDamCmtLibelle($damCmtLibelle)
    {
        $this->damCmtLibelle = $damCmtLibelle;
    
        return $this;
    }

    /**
     * Get damCmtLibelle
     *
     * @return string
     */
    public function getDamCmtLibelle()
    {
        return $this->damCmtLibelle;
    }

    /**
     * Get damCmtId
     *
     * @return integer
     */
    public function getDamCmtId()
    {
        return $this->damCmtId;
    }
}

