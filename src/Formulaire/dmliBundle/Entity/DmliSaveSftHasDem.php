<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliSaveSftHasDem
 *
 * @ORM\Table(name="dmli_save_sft_has_dem", indexes={@ORM\Index(name="fk_dmli_software_sft_has_dmli_save_demande_dem_dmli_save_de_idx", columns={"dem_id"}), @ORM\Index(name="fk_dmli_software_sft_has_dmli_save_demande_dem_dmli_softwar_idx", columns={"sft_id"})})
 * @ORM\Entity
 */
class DmliSaveSftHasDem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="dem_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $demId;

    /**
     * @var \Formulaire\dmliBundle\Entity\DmliSoftwareSft
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Formulaire\dmliBundle\Entity\DmliSoftwareSft")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sft_id", referencedColumnName="sft_id")
     * })
     */
    private $sft;


    /**
     * Set demId
     *
     * @param integer $demId
     *
     * @return DmliSaveSftHasDem
     */
    public function setDemId($demId)
    {
        $this->demId = $demId;
    
        return $this;
    }

    /**
     * Get demId
     *
     * @return integer
     */
    public function getDemId()
    {
        return $this->demId;
    }

    /**
     * Set sft
     *
     * @param \Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft
     *
     * @return DmliSaveSftHasDem
     */
    public function setSft(\Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft)
    {
        $this->sft = $sft;
    
        return $this;
    }

    /**
     * Get sft
     *
     * @return \Formulaire\dmliBundle\Entity\DmliSoftwareSft
     */
    public function getSft()
    {
        return $this->sft;
    }
}

