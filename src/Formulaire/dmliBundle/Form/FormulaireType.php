<?php

namespace Formulaire\dmliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormulaireType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            #livraison
            #->add('livraison', 'date', array('mapped' => false))
            #utilisateur

            ->add('nom', "text", array('mapped' => false, 'attr'=>array('class'=>'form-control  input-sm')))
            ->add('prenom', "text", array('mapped' => false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('matricule', "text", array('mapped' => false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('societe', 'choice',  array('label'=>'Société', 'mapped'=>false, 'attr'=>array('class'=>'chosen-select', 'style'=>'width: 200px;') ))
            ->add('direction', 'choice', array('label'=>'Direction',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('service','choice', array('label'=>'Service',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('pole','choice', array('label'=>'Pole',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('posteNonAffect','checkbox', array('label'=>'NA', 'value'=>'1', 'mapped'=>false ,'required'=>false))
            ->add('poste','choice', array('label'=>'Poste', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            #code utilisateur

            ->add('inputCodeUserKeyPerso','text', array('label'=>'Type de personnel',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('selectCodeUserTypeAchat','choice', array('label'=>'Type d\'acheteur',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('selectCodeUserKeyPays','choice', array('label'=>'Clé pays', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('selectCodeUserComment','choice', array('label'=>'Commentaire', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('selectCodeUserSSPole','choice', array('label'=>'Sous Pole', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            #Demande
            #option
                #outlook
            ->add('selectListMail','choice', array('label'=>'','multiple'=>true, 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('buttonAddListMail', 'button', array('label'=>'Ajouter','attr' => array('value'=>'Ajouter','class'=>"btn btn-sm btn-default")))

            ->add('sapProf', 'checkbox', array('mapped' => true, 'label' => 'Profil SAP','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('crmProf', 'checkbox', array('mapped' => true, 'label' => 'Profil CRM','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('cognosProf', 'checkbox', array('mapped' => true, 'label' => 'Accès Cognos','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))

            #profil SAP
            ->add('sapUserGen', 'checkbox', array('mapped' => true, 'label' => 'Utilisateur Générique','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('inputProfilSap', 'text', array('required'=>false,'mapped' => false, 'label' => 'Profil à dupliquer', 'attr'=>array('class'=>'form-control input-sm')))
            ->add('textareaComment', 'textarea', array('required'=>false,'mapped' => false, 'label' => 'Commentaires', 'attr'=>array('class'=>'form-control','style'=>'
    height: 80px;
    width: auto;')))
            #profil CRM
            ->add('crmUserGen', 'checkbox', array('required'=>false,'mapped' => false, 'label' => 'Utilisateur Générique'))
            ->add('selectCrmProfil','choice', array('label'=>'Profile CRM',  'mapped'=>false , 'attr'=>array('class'=>'form-control input-sm'),'required'=>false))
            ->add('selectCrmTeam','choice', array('label'=>'Nom de l\'equipe', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm'),'required'=>false ))

            #Accès Cognos
            ->add('cognosProfil', 'text', array('required'=>false,'mapped' => false, 'label' => 'Profil à dupliquer', 'attr'=>array('class'=>'form-control input-sm')))
            ->add('cognosComment', 'textarea', array('required'=>false, 'mapped' => false, 'label' => 'Commentaires', 'attr'=>array('class'=>'form-control','style'=>'
    height: 80px;
    width: auto;')))
            #remarque
            ->add('remarque', 'textarea', array('required'=>false,'mapped' => false, 'label' => false, 'attr'=>array('class'=>'form-control input-sm')))

            #categorie
            ->add('nouvel', 'checkbox', array('mapped' => true, 'label' => 'Nouvel Arrivant','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('mercato', 'checkbox', array('mapped' => true, 'label' => 'Mercato','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('userGen', 'checkbox', array('mapped' => true, 'label' => 'Utilisateur Générique','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('renouvellement', 'checkbox', array('mapped' => true, 'label' => 'Renouvellement de Matériel','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))


            #validation de formulaire
        ->add('Valider', 'submit', array('attr'=>array('class'=>'btn btn-sm btn-default','onsubmit'=>'return doNotSubmit()')))
        #liste logiciel
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'formulaire_dmlibundle_admdirection';
    }
}
