<?php

namespace Formulaire\DDRCBundle\Validator;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class FilesTotalSize extends Constraint
{
    public $message;
    
    public function validatedBy()
    {
        return get_class($this).'Validator'; 
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
