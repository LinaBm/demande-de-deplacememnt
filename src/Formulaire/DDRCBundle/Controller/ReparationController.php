<?php

namespace Formulaire\DDRCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\DDRCBundle\Entity\Reparation;
use Formulaire\DDRCBundle\Form\ReparationType;
use Formulaire\DDRCBundle\Email\Email;
use Symfony\Component\HttpFoundation\Response;
use Formulaire\DDRCBundle\Word\WordGenerator;

/**
 * Reparation controller.
 *
 * @Route("/reparation")
 */
class ReparationController extends Controller
{
    /**
     * Creates a new Reparation entity.
     *
     * @Route("/", name="reparation_create")
     * @Method("POST")
     * @Template("DDRCBundle:Reparation:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Reparation();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            //Retrieve the service (object) that handles moving files to uploadDir and returns the file name to be saved in Database
            $attachement_service = $this->get('attachment_handling_service');           
            
            if(!is_null($entity->getAttachement1())){
                $fileName = $attachement_service->addAttachments($entity->getAttachement1(),$entity, 'Facture de reparation');
                $entity->setAttachement1($fileName);
            }
            
            if(!is_null($entity->getAttachement3())){
                $fileName = $attachement_service->addAttachments($entity->getAttachement3(),$entity, 'Devis payant');
                $entity->setAttachement3($fileName);
            }
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement2(),$entity, 'Ticket achat');
            $entity->setAttachement2($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement4(),$entity, 'La  fiche de remboursement ');
            $entity->setAttachement4($fileName);
            
            // ... persist the $entity variable or any other work
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
  
            //------- Generate word --------------
            $societe= $em->getRepository('Administration\SocieteBundle\Entity\PandoreSociete')->find($entity->getIdMagasin()->getSociete());
            $wordGenerator = $this->get('word_gen');
            $pathWordFile = $wordGenerator->generate($entity, $societe);
            
             //--- Send email------------------
            $files=array('la facture de la reparation et/ou du devis payant','le ticket achat', 'la  fiche de remboursement');
            $Email = $this->get('email_service');
            $Email->send3($entity, $files, $pathWordFile);
            
            return $this->render('DDRCBundle:Default:success.html.twig');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Reparation entity.
     *
     * @param Reparation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Reparation $entity)
    {
        $form = $this->createForm(new ReparationType(), $entity, array(
            'action' => $this->generateUrl('reparation_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer la demande'));

        return $form;
    }

    /**
     * Displays a form to create a new Reparation entity.
     *
     * @Route("/new", name="reparation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Reparation();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
}
