<?php

namespace Formulaire\DDRCBundle\Utils;

/**
 * Description of NameGenerator
 *
 * @author pana_cr
 */
class NameGenerator {
    
    public static  function createNameForUploadedFile($entity, $file_type, $extension)
    {
        //var_dump($entity->getId()); die();
        //$file_type means 'Ticket d achat, Devis etc
        $name = $entity->getIdMagasin()->getMagLibelle() ." RBT ".
                substr(uniqid(),8,4) . " " .
                $entity->getLastName() ." ". $entity->getDate()->format('dmo') ." ".
                $file_type .'.'.
                $extension;
        
        return $name;
    }
    
    public static function createNameForGeneratedWord($entity)
    {
         $name = $entity->getIdMagasin()->getMagLibelle() ." RBT ".
                substr(uniqid(),8,4) . " " .
                $entity->getLastName() ." ". $entity->getDate()->format('dmo') ." ".
                'courrier.docx';
        
        return $name;
    }
}
