<?php

namespace Formulaire\AdminBundle\Controller\Profil;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class AddProfilController extends Controller
{
    /**
 * @Route("/profil/add", name="add_profil")
 * @Template()
 */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Profil:AjouterProfil.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/profil/adddepartment")
     * @Template()
     */
    public function departmentAction()
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $direction = $request->get('direction');
            $result = array();
            $services = $em->getRepository('dmliBundle:AdmService')->findBy(array('direction' => $direction));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }
            if (count($result) >= 1) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/adddirection")
     * @Template()
     */
    public function directionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $result = array();
            $services = $em->getRepository('dmliBundle:AdmDirection')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }

            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/adddivision")
     * @Template()
     */
    public function divisionAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $serv = $request->get('service');
            $result = array();
            $services = $em->getRepository('dmliBundle:PolePol')->findBy(array('srv' => $serv));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getPolId();
                $result[$i]['libelle'] = $service->getPolLibelle();
                $i++;
            }
            if (count($result) > 0) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }



    /**
     * @Route("/profil/addpost")
     * @Template()
     */
    public function postAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $idBridge = $request->get('idBridge');
            if ($idBridge[1] != '') {
                if ($idBridge[2] == 'NULL') {
                    $lineSql = " AND pol_id IS NULL ";
                } else {
                    $lineSql = " AND pol_id = " . $idBridge[2];
                }


                $sql = "	SELECT pst_id, pst_libelle
				FROM poste_pst
				ORDER BY pst_libelle;
			";

                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $result = $query->fetchAll();

                if (count($result) > 0) {

                    $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';

                    foreach ($result as $row) {
                        $output .= '<option value="' . $row['pst_id'] . '">' . $row['pst_libelle'] . '</option>';
                    }

                    $output .= '<option value="-1"> -- AUTRE -- </option>';


                    $return = $output;
                    $response = new Response();
                    $data = json_encode($return);
                    $response->headers->set('Content-Type', 'application/json');
                    $response->setContent($data);
                    return $response;
                }
            } else {
                return new Response('Erreur');
            }
        }
    }





}
