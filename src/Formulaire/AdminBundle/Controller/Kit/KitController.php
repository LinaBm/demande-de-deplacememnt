<?php

namespace Formulaire\AdminBundle\Controller\Kit;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class KitController extends Controller
{
    /**
     * @Route("/kit", name="index_kit")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliPackhardwarePkh')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getPkhId();
            $result[$i]['libelle'] = $service->getPkhLibelle();
            $i++;
        }
        return $this->render('AdminBundle:Kit:ListeKit.html.twig', array('result'=> $result));

}
    /**
     * @Route("/kit/remove/{id}", name="remove_kit")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:DmliPackhardwarePkh')->find($id);

        $em->remove($product);
        $em->flush();
        $request->getSession()->getFlashBag()->add(
            'success',
            'Kit Matériel supprimé!'
        );
        return $this->redirect($this->generateUrl('index_kit'));
    }
    /**
     * @Route("/kit/hardware")
     *
     */
    public function hardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $sql = "SELECT har_id, har_libelle, har_prix
				FROM dmli_hardware_har d inner join typemateriel t on d.typ_id=t.id where t.eta_id=1
			";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    /**
     * @Route("/kit/listHardwareInPack")
     *
     */
    public function listHardwareInPackAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idPack = $request->get('pack');

            $sql = "	SELECT har_id, har_libelle, har_prix
			FROM dmli_hardware_har
			WHERE har_id IN (
				SELECT har_id
				FROM dmli_pkh_has_har
				WHERE pkh_id = " . $idPack . "
			)
		";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

}
