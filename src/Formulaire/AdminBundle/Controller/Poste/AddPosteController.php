<?php

namespace Formulaire\AdminBundle\Controller\Poste;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class AddPosteController extends Controller
{
    /**
     * @Route("/poste/add", name="add_poste")
     * @Template()
     */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new AdminType());
        return $this->render('AdminBundle:Poste:AjouterPoste.html.twig', array(
            'form' => $form->createView()
        ));

    }
}