<?php

namespace Suivi\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table("se_contact")
 * @ORM\Entity(repositoryClass="Suivi\ContactBundle\Entity\ContactRepository")
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demande;
    
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="text")
     */
    private $objet;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

/**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     * @return Contact
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande 
     */
    public function getDemande()
    {
        return $this->demande;
    }
    /**
     * Set objet
     *
     * @param string $objet
     * @return Contact
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet
     *
     * @return string 
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Contact
     */
    public function setMessage($message)
    {
    	$this->message = $message;
    
    	return $this;
    }
    
    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
    	return $this->message;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Contact
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
