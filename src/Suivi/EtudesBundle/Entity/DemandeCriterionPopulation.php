<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeDemandeCriterion
 *
 * @ORM\Table(name="se_demande_criterion_population")
 * @ORM\Entity
 */
class DemandeCriterionPopulation {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=false)
     */
    private $points;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SeDemandeCriterion
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return SeDemandeCriterion
     */
    public function setPoints($points) {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints() {
        return $this->points;
    }

    public function __toString() {
        return strval($this->id);
    }

}
