<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemandeApplication
 *
 * @ORM\Table(name="se_demande_application")
 * @ORM\Entity()
 */
class DemandeApplication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="demande_id", type="integer")
     */
    private $demandeId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="application_id", type="integer")
     */
    private $applicationId;
    
    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande", inversedBy="demandeApplication")
     * @ORM\JoinColumn(nullable=false)
     */
    private $demande;
    
    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Application")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", nullable=true)
     */
    private $application;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set demandeId
     *
     * @param string $demandeId
     * @return DemandeApplication
     */
    public function setDemandeId($demandeId)
    {
        $this->demandeId = $demandeId;

        return $this;
    }

    /**
     * Get demandeId
     *
     * @return string 
     */
    public function getDemandeId()
    {
        return $this->demandeId;
    }
    
    /**
     * Set applicationId
     *
     * @param string $applicationId
     * @return DemandeApplication
     */
    public function setApplicationId($applicationId)
    {
        $this->applicationId = $applicationId;

        return $this;
    }

    /**
     * Get applicationId
     *
     * @return string 
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }
    
    /**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     * @return DemandeUser
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande 
     */
    public function getDemande()
    {
        return $this->demande;
    }
    
    /**
     * Get application
     *
     * @return \Suivi\EtudesBundle\Entity\Application 
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set application
     *
     * @param \Suivi\EtudesBundle\Entity\Application $application
     * @return DemandeUser
     */
    public function setApplication(\Suivi\EtudesBundle\Entity\Application $application)
    {
        $this->application = $application;

        return $this;
    }
}
