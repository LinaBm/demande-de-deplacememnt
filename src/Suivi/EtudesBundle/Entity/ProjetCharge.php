<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjetTauxHoraire
 *
 * @ORM\Table(name="se_projet_charge")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ProjetChargeRepository")
 */
class ProjetCharge {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="demande_id", type="integer", nullable=false)
     */
    private $demandeId;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demande;

    /**
     * @var integer
     *
     * @ORM\Column(name="demandeuser_id", type="integer", nullable=false)
     */
    private $demandeUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period", type="string", nullable=false)
     */
    private $period;

    /**
     * @var integer
     *
     * @ORM\Column(name="hours", type="integer", nullable=false)
     */
    private $hours;

    /**
     * @var integer
     *
     * @ORM\Column(name="hours_worked", type="integer", nullable=true)
     */
    private $hoursWorked;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ProjetCharge
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set demandeId
     *
     * @param integer $demandeId
     *
     * @return ProjetCharge
     */
    public function setDemandeId($demandeId) {
        $this->demandeId = $demandeId;

        return $this;
    }

    /**
     * Get demandeId
     *
     * @return integer
     */
    public function getDemandeId() {
        return $this->demandeId;
    }

    /**
     * Set demandeUserId
     *
     * @param integer $demandeUserId
     *
     * @return ProjetCharge
     */
    public function setDemandeUserId($demandeUserId) {
        $this->demandeUserId = $demandeUserId;

        return $this;
    }

    /**
     * Get demandeUserId
     *
     * @return integer
     */
    public function getDemandeUserId() {
        return $this->demandeUserId;
    }

    /**
     * Set period
     *
     * @param \DateTime $period
     *
     * @return ProjetTauxHoraire
     */
    public function setPeriod($period) {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \DateTime
     */
    public function getPeriod() {
        return $this->period;
    }

    /**
     * Set hours
     *
     * @param integer $hours
     *
     * @return ProjetCharge
     */
    public function setHours($hours) {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return integer
     */
    public function getHours() {
        return $this->hours;
    }

    /**
     * Set hoursWorked
     *
     * @param integer $hoursWorked
     *
     * @return ProjetCharge
     */
    public function setHoursWorked($hoursWorked) {
        $this->hoursWorked = $hoursWorked;

        return $this;
    }

    /**
     * Get hoursWorked
     *
     * @return integer
     */
    public function getHoursWorked() {
        return $this->hoursWorked;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\DemandeUser", inversedBy="charge")
     * @ORM\JoinColumn(name="demandeuser_id", referencedColumnName="id")
     */
    private $demandeUser;

    /**
     * Get demandeUser
     *
     * @return \Suivi\EtudesBundle\Entity\DemandeUser 
     */
    public function getDemandeUser() {
        return $this->demandeUser;
    }

    /**
     * Set demandeUser
     *
     * @param integer $demandeUser
     *
     * @return ProjetCharge
     */
    public function setDemandeUser($demandeUser) {
        $this->demandeUser = $demandeUser;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\ProjetTauxHoraire", inversedBy="charge")
     * @ORM\JoinColumns(
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="user_id"),
     *      @ORM\JoinColumn(name="period", referencedColumnName="period")
     * ) 
     */
    private $tax;

    /**
     * Get tax
     *
     * @return \Suivi\EtudesBundle\Entity\ProjetTauxHoraire 
     */
    public function getTax() {
        return $this->tax;
    }

    /**
     * Set tax
     *
     * @param integer $tax
     *
     * @return ProjetTauxHoraire
     */
    public function setTax($tax) {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ProjetCharge
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set demande
     *
     * @param \stdClass $demande
     * @return ProjetCharge
     */
    public function setDemande($demande) {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \stdClass 
     */
    public function getDemande() {
        return $this->demande;
    }

}
