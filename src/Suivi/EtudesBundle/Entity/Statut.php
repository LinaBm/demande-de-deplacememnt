<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statut
 *
 * @ORM\Table(name="se_statut")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\StatutRepository")
 */
class Statut
{
    const STAND_BY      = 0;
    const NOUVEAU       = 1;
    const PRIS_EN_COMPTE= 2;
    const ETUDES        = 3;
    const EN_ATTENTE    = 4;
    const VALIDE        = 5;
    const EN_COURS_CP   = 6;
    const EN_COURS_DEV  = 7;
    const RECETTE_INFO  = 8;
    const RECETTE_USER  = 9;
    const MISE_EN_PROD  = 10;
    const CLOS          = 11;
    const ANNULE        = 12;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Statut
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
