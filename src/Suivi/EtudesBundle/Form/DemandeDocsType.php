<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeDocsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', 'choice', array(
                    'choices' => array(
                        'upload' => 'Uploadez un fichier',
                        'reseau' => 'Entrez le chemin du fichier'
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false,
                ))
                ->add('name', 'file', array(
                    'mapped' => false,
                    'required' => false,
                ))
                ->add('path', 'text', array(
                    'mapped' => false,
                    'required' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                array(
                    'data_class' => 'Suivi\EtudesBundle\Entity\DemandeDocs',
                )
            );
        }

    /**
     * @return string
     */
    public function getName() {
        return 'demandedocs';
    }

}
        