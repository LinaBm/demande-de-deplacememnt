<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class StandByForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('raison', 'choice', array(
                    'choices' => array(
                        'Besoin non défini' => 'Besoin non défini',
                        'Sujet dépriorisé' => 'Sujet dépriorisé',
                        'Autre' => 'Autre',
                    ),
                    'empty_value' => false,
                    'required' => true,
                    'constraints' => array(new Assert\NotNull(array('message' => 'Libellé ne doit pas être vide.'))
                    ))
                )
                ->add('description', 'textarea', array(
                    'required' => true,
                    'constraints' => array(new Assert\NotNull(array('message' => 'Libellé ne doit pas être vide.'))
                    ))
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
            'validation_groups' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'standBy';
    }

}
