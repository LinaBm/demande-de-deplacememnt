<?php
//  Julien Bureau <jbureau@noz.fr> 						
//  25-11-14 												
//  formulaire d affectation de ressources à une demande 	
namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Suivi\EtudesBundle\Entity\DemandeUser;

class DemandeAffectationType extends AbstractType
{
	private $demande;
	private  $listeObservateurs;
	private  $listeChefDeProjets;
	private  $listeTechs;
	private $em;
		
	public function __construct($demande, $em){
		$this->demande = $demande;
		$this->em = $em;
		$this->listeObservateurs=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getObservateursbyDemande($demande);
		$this->listeChefDeProjets=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getChefDeProjetbyDemande($demande);
		$this->listeTechs=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getTechniciensbyDemande($demande);
	}
	
    /**
     * Julien Bureau <jbureau@noz.fr>
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	
    	$builder
	    	->add('chefdeProjet', 'entity', array(
	    			'class' => 'UserUserBundle:User',
	    			'property' => 'displayname',
	    			'query_builder'=> $this->em->getRepository('UserUserBundle:User')
	    			->getByRole('ROLE_TECH'),
	    			'required' => false,
	    			'multiple' => true,
	    			'expanded' => false,
	    			'mapped'=>false,
	    			'data' => $this->listeChefDeProjets,
	    	))
	    	
	    	->add('techniciens', 'entity', array(
	    			'class' => 'UserUserBundle:User',
	    			'property' => 'displayname',
	    			'query_builder'=> $this->em->getRepository('UserUserBundle:User')
	    			->getByRole('ROLE_TECH'),
	    			'required' => false,
	    			'multiple' => true,
	    			'expanded' => false,
	    			'mapped'=>false,
	    			'data' => $this->listeTechs,
	    	))
    	;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_affectation';
    }
}