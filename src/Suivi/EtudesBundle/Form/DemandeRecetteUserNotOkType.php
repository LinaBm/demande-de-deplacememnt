<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeRecetteUserNotOkType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('description','textarea', array(
        	'mapped'=>false,
        ))
        
        ->add('cahier', 'file', array(
        	'mapped'=>false,
       		'required' => false,
        ))
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_recetteusernotok';
    }
}
