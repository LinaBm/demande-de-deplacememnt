<?php

namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Statut;

class LoadStatut extends AbstractFixture implements OrderedFixtureInterface
{

  /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 1;
  }


  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste des noms de statut à ajouter
    $names = array(
      'Nouveau',
      'Etudes',
      'En attente validation',
      'Validation (devis)',
      'En cours - Chef de projet',
      'En cours - Développeur',
      'Recettes (Info)',
      'Recettes (Utilisateurs)',
      'Mise en production',
      'Clos',
      'Annulé',
    );

    foreach ($names as $name) {
      // On crée le statut
      $statut = new Statut();
      $statut->setName($name);

      // On la persiste
      $manager->persist($statut);
    }

    // On déclenche l'enregistrement de toutes les catégories
  //  $manager->flush();
  }
}