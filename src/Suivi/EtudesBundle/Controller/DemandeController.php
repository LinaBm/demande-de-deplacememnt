<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Suivi\EtudesBundle\Entity\Demande;
use Suivi\EtudesBundle\Entity\Statut;
use Suivi\EtudesBundle\Entity\DemandeSearch;
use Suivi\EtudesBundle\Entity\DemandeUser;
use Suivi\EtudesBundle\Form\DemandeType;
use Suivi\EtudesBundle\Form\DemandeSearchType;
use Suivi\EtudesBundle\Form\DemandeEditType;
use Suivi\EtudesBundle\Form\DemandeEstimationType;
use Suivi\EtudesBundle\Form\EstimationEtudesType;
use Suivi\EtudesBundle\Form\AvancementType;
use Suivi\EtudesBundle\Form\DemandeSpecType;
use Suivi\EtudesBundle\Form\DemandeRecetteUserOkType;
use Suivi\EtudesBundle\Form\DemandeRecetteUserNotOkType;
use Suivi\EtudesBundle\Form\DemandeChangerProdType;
use Suivi\EtudesBundle\Form\DemandeModifierDateType;
use Suivi\UserBundle\Entity\User;
//use Suivi\UserBundle\Form\UserType;
//use Direction;
//use Direction\DirectionBundle\Entity\ServiceSrv;
//use Direction\DirectionBundle\Entity\DirectionDir;
//use Symfony\Component\Validator\Constraints\Url;
//use Suivi\EtudesBundle\Form\PiecejointeType;
use Suivi\EtudesBundle\Form\DemandeAffectationType;
use Symfony\Component\HttpFoundation\Response;

class DemandeController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $statusDemandes = $countStatuses = null;
        $user = $this->getUser();
        $viewVariables = array();
        if ($user->isGranted('ROLE_MANAGER_TECH')) {
            $statusDemandes = $em->getRepository('SuiviEtudesBundle:Demande')->getStatusDemandes();
            $countStatuses = $em->getRepository('SuiviEtudesBundle:Demande')->setStatusColors();
            $viewVariables['statusDemandes'] = $statusDemandes;
            $viewVariables['countStatuses'] = $countStatuses;
            $viewVariables['customSum'] = $em->getRepository('SuiviEtudesBundle:Demande')->getCustomStatusesSum($viewVariables['countStatuses']);
        }
        return $this->render('SuiviEtudesBundle:Demande:index.html.twig', $viewVariables);
    }

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
    public function listeAction(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();
        //Init des variables
        $data = array();
        //Instance du formulaire
        $recherche = new DemandeSearch();

        //récupération du formulaire dans l'url
        $form_search = $request->query->all();
        $nbPerPage = $this->container->getParameter('NombreDemandeParPage');

        if (!empty($form_search)) {
            if (isset($form_search['demande_search'])) {
                $form_search = $form_search['demande_search'];
            }
            // set no of results on page
            if (isset($form_search['nbResulParPage'])) {
                $nbPerPage = $form_search['nbResulParPage'];
                if ((int) $nbPerPage > 0) {
                    $nbPerPage = (int) $nbPerPage;
                } else {
                    $nbPerPage = 5000;
                }
            }
            //Si un id de demande est renseigné
            if (isset($form_search['demande_id']) && $form_search['demande_id'] != "") {
                //On redirige vers la fiche de la demande
                return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $form_search['demande_id'])));
            } else {
                if (isset($form_search['urgence']))
                    $recherche->setUrgence($em->getRepository('SuiviEtudesBundle:Urgence')->find($form_search['urgence']));
                if (isset($form_search['statut'])) {
                    foreach ($form_search['statut'] as $numStatut) {
                        $recherche->addStatut($em->getRepository('SuiviEtudesBundle:Statut')->find($numStatut));
                    }
                }
                if (isset($form_search['application']))
                    $recherche->setApplication($em->getRepository('SuiviEtudesBundle:Application')->find($form_search['application']));
                if (isset($form_search['direction']))
                    $recherche->setDirection($em->getRepository('AdministrationDirectionBundle:Direction')->find($form_search['direction']));
                if (isset($form_search['service']))
                    $recherche->setService($em->getRepository('AdministrationDirectionBundle:Service')->find($form_search['service']));
                if (isset($form_search['type']))
                    $recherche->setType($em->getRepository('SuiviEtudesBundle:Type')->find($form_search['type']));
                if (isset($form_search['demandeur']))
                    $recherche->setDemandeur($em->getRepository('UserUserBundle:User')->find($form_search['demandeur']));
                if (isset($form_search['technicien']))
                    $recherche->setTechnicien($em->getRepository('UserUserBundle:User')->find($form_search['technicien']));
                if (isset($form_search['expression']))
                    $recherche->setExpression($form_search['expression']);
            }
        }//fin if form empty
        // Si c'est un collaborateur : toutes ses recherches se fera
        // en fonction de son user_id
        if (!$this->get('security.context')->isGranted('ROLE_TECH')) {
            $recherche->setDemandeur($this->getUser());
        }

        $form = $this->createForm(new DemandeSearchType, $recherche, array(
            'action' => $this->generateUrl('suivi_etudes_liste', array('page' => 1)),
            'method' => 'GET'
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesList($form->getData(), $page);
        } else {
            $data = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesList($recherche, $page);
        }

        /* START display "demandes" based on the filters in the se_demandesearch DB table */
        $userId = $this->getUser()->getId();
        $userFilters = $this->filterDemandesForUser($userId);
        if (null == $form_search && null !== $userFilters) {
            return $this->redirect($userFilters);
        }
        /* END display "demandes" based on the filters in the se_demandesearch DB table */

        //Pagination
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $data, $request->query->getInt('page', $page)/* page number */
                , $nbPerPage/* limit per page */
        );

        // displayed columns
        $userColumns = $em->getRepository('SuiviEtudesBundle:DemandeUserColumns')->findOneBy(array('userId' => $this->getUser()->getId()));

        // write url on session so that from "demande_voir" page to be able to return with filters
        $session = $request->getSession();
        $filters = $session->set('filterRequest', $request->getUri());

        // L'appel de la vue ne change pas
        return $this->render('SuiviEtudesBundle:Demande:liste.html.twig', array(
                    'listeDemandes' => $pagination,
                    'form' => $form->createView(),
                    'page' => $page,
                    'demande_search' => $form_search,
                    'columns' => $userColumns,
                    'totalRows' => count($data)
        ));
    }

//Fin listeAction

    /**
     * @author Julien Bureau <jbureau@noz.fr>
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listeUserAction($page) {

        $filtre = array();
        //Si la personne connectée est manager tech
        if ($this->get('security.context')->isGranted('ROLE_MANAGER_TECH')) {
            $filtre['demandeur'] = $this->getUser()->getId();
        }
        //S'il est au moins Technicien : on affiche ses demandes affectées
        elseif ($this->get('security.context')->isGranted('ROLE_TECH')) {
            $filtre['technicien'] = $this->getUser()->getId();
        }
        //Sinon, (c'est un collaborateur) on affiche ses demandes créées
        else {
            $filtre['demandeur'] = $this->getUser()->getId();
        }
        return $this->redirect($this->generateUrl('suivi_etudes_liste', array('demande_search' => $filtre)));
    }

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
    public function voirAction(Request $request, $id) {

        // On récupère les EntityManager
        $em = $this->getDoctrine()->getManager();
        //$emArchi = $this->getDoctrine()->getManager('archi');
        // Pour récupérer une demande unique : on utilise find()
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        // Si la demande est n'existe pas : message d'erreur + redirection
        if ($demande === null) {
            $request->getSession()->getFlashBag()->add('erreur', "Cette demande n'existe pas.");
            return $this->redirect($this->generateUrl('suivi_etudes_liste'));
        } //fin if null
        //Récupération des PJ ($demande->getListePJ ?)
        $listePJ = $em->getRepository('SuiviEtudesBundle:PieceJointe')->getPJparDemande($id);
        //Récupération de l'extension pour afficher le logo
        $extensions = null;
        foreach ($listePJ as $pj) {
            $extension = substr(strrchr($pj->getName(), '.'), 1);
            switch ($extension) {
                case 'png' :
                case 'jpg' : $img = 'jpg_64.png';
                    break;
                case 'pdf' : $img = 'pdf_64.png';
                    break;
                case 'doc' :
                case 'docx' : $img = 'doc_64.png';
                    break;
                case 'xls' :
                case 'xlsx' :
                case 'csv' : $img = 'xls_64.png';
                    break;
                case 'txt' : $img = 'txt_64.png';
                    break;
                case 'ppt' :
                case 'pptx' : $img = 'ppt_64.png';
                    break;
                default : $img = 'txt_64.png';
                    break;
            }
            $extensions[$pj->getId()] = $img;
        }//fin foreach
        // Récupération du rôle de la personne connectée par rapport à la demande
        $demandeUser = $demande->getDemandeUser();
        $roleUserConnecte = 0;
        $contientUnValideur = false;
        foreach ($demandeUser as $row) {
            if ($this->getUser() == $row->getUser()) {
                $roleUserConnecte = $row->getType();
            }
            //S'il n'y a que des demandeurs
            if (!$row->getUser()->isGranted("ROLE_DEMANDEUR")) {
                $contientUnValideur = true;
            }
            //if ($this->get('security.context')->isGranted('ROLE_VALIDEUR', $row->getUser())) {}
        } // fin foreach
        if (!$contientUnValideur) {
            $request->getSession()->getFlashBag()->add('erreur', "Cette demande n'a pas de personne habilitée à valider les estimations de charges et la recette utilisateur. <br/>Veuillez ajouter en observateur la personne référente pour ne pas être bloqué lors de ces étapes.");
        }
        //Récupération du service
        $service = null;
        if ($demande->getService()) {
            $service = $em->getRepository('AdministrationDirectionBundle:Service')->find($demande->getService())->getLibelle();
        }

        //Affichage du (1) dans les actions, si l'utilisateur courant  en a une à effectuer.
        $actions = 0;
        $statut = $demande->getStatut()->getId();
        if ($this->get('security.context')->isGranted('ROLE_MANAGER_TECH', $this->getUser())) {
            if ($statut <= Statut::VALIDE || ($statut >= Statut::RECETTE_USER && $statut <= Statut::MISE_EN_PROD)) {
                $actions = 1;
            }
        } elseif ($roleUserConnecte != "0") {
            if ($this->get('security.context')->isGranted('ROLE_TECH', $this->getUser())) {
                if (($statut >= Statut::EN_COURS_CP && $statut <= Statut::RECETTE_INFO) || ($statut == Statut::RECETTE_USER && $demande->getRecette() == 0)) {
                    $actions = 1;
                }
            } elseif ($this->get('security.context')->isGranted('ROLE_VALIDEUR', $this->getUser()) && ($statut == Statut::EN_ATTENTE || ($statut == Statut::RECETTE_USER) && $demande->getRecette() != 1) || $statut == Statut::MISE_EN_PROD) {
                $actions = 1;
            }
        }//Fin  affichage (1) Actions.
        //récupération du cahier de spec pour savoir s'il s'agit d'une pièce jointe ou d'un lien vers le réseau.
        if (substr($demande->getSpec(), 0, 1) == '/') {
            $specPieceJointe = true;
        } else {
            $specPieceJointe = false;
        }

        //récupération de l'historique
        $historique = $em->getRepository('SuiviEtudesBundle:Modification')->getModificationsParDemande($demande);

        // get last status for standBy 
        $lastStatus = $em->createQueryBuilder('query')
                        ->select('m.ancienneValeur, m.nouvelleValeur')
                        ->from('SuiviEtudesBundle:Modification', 'm')
                        ->where('m.demande = :id')
                        ->andWhere("m.type = 'statut'")
                        ->setParameter('id', $demande)
                        ->orderBy('m.id', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery()->getOneOrNullResult();

        $demandeTech = $em->createQueryBuilder('query')
                        ->select('du.userId, sum(pc.hours) hours, sum(pc.hoursWorked) hoursWorked')
                        ->from('SuiviEtudesBundle:DemandeUser', 'du')
                        ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demandeUserId=du.id')
                        ->where('du.demandeId = :id')
                        ->andWhere('du.type=\'TECHNICIEN\'')
                        ->groupBy('du.userId')
                        ->setParameter('id', $id)
                        ->getQuery()->getResult();
        $demandeCP = $em->createQueryBuilder('query')
                        ->select('du.userId, sum(pc.hours) hours, sum(pc.hoursWorked) hoursWorked')
                        ->from('SuiviEtudesBundle:DemandeUser', 'du')
                        ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demandeUserId=du.id')
                        ->where('du.demandeId = :id')
                        ->andWhere('du.type=\'CHEF_DE_PROJET\'')
                        ->groupBy('du.userId')
                        ->setParameter('id', $id)
                        ->getQuery()->getResult();
        //Création du tableau de paramètre pour créer notre vue "voir"
        $params = array(
            'demande' => $demande,
            'service' => $service,
            'listePJ' => $listePJ,
            'extensions' => $extensions,
            'roleUserConnecte' => $roleUserConnecte,
            'actions' => $actions,
            'specPieceJointe' => $specPieceJointe,
            'historique' => $historique,
            'demandeTech' => $demandeTech,
            'demandeCP' => $demandeCP,
            'lastStatus' => $lastStatus
        );

        /* if ($demande->getStatut()->getId() == Statut::ETUDES || $demande->getStatut()->getId() == Statut::EN_ATTENTE) {
          $form = $this->createForm(new DemandeEstimationType, $demande, array(
          'action' => $this->generateUrl('modifier_statut_attente', array('id' =>  $demande->getId())),
          ));
          //Ajout du formulaire des estimations
          $params['form'] = $form->createView();
          } *///fin if etudes ou en_attente
        // form standBy
        if ($demande->getStatut()->getId() >= Statut::NOUVEAU && $demande->getStatut()->getId() <= Statut::MISE_EN_PROD) {
            $form = $this->createForm(new \Suivi\EtudesBundle\Form\StandByForm);
            $params['StabdByForm'] = $form->createView();
        }

        if ($demande->getStatut()->getId() >= Statut::PRIS_EN_COMPTE) {
            $formAffectation = $this->createForm(new DemandeAffectationType($demande, $em), $demande, array(
                'action' => $this->generateUrl('modifier_statut_affectation', array(
                    'id' => $demande->getId()
                        //'users' => $users
                )),
                'method' => 'GET'
            ));
            //Ajout du formulaire d'affectation
            $params['formAffectation'] = $formAffectation->createView();
        }

        if ($demande->getStatut()->getId() <= Statut::EN_ATTENTE) {
            //Nouveau + Etudes
            $form = $this->createForm(new EstimationEtudesType, $demande, array(
                'action' => $this->generateUrl('renseigner_estimation_etudes', array('id' => $demande->getId())),
            ));
            //Ajout du formulaire des estimations
            $params['formEtudes'] = $form->createView();

            //Etudes + en attente
            if ($demande->getStatut()->getId() > Statut::NOUVEAU && $demande->getStatut()->getId() <= Statut::VALIDE) {
                $form = $this->createForm(new DemandeEstimationType, $demande, array(
                    'action' => $this->generateUrl('modifier_statut_attente', array('id' => $demande->getId())),
                ));
                //Ajout du formulaire des estimations
                $params['form'] = $form->createView();
            }
        }//fin if avant validation
        elseif ($demande->getStatut()->getId() >= Statut::VALIDE) {
            $form = $this->createForm(new AvancementType, $demande, array(
                'action' => $this->generateUrl('renseigner_avancement_demande', array('id' => $demande->getId())),
            ));
            //Ajout du formulaire de l'avancement
            $params['formAvancement'] = $form->createView();

            if ($demande->getStatut()->getId() >= Statut::VALIDE && $demande->getStatut()->getId() <= Statut::RECETTE_USER) {
                $form = $this->createForm(new DemandeModifierDateType, $demande, array(
                    'action' => $this->generateUrl('modifier_date_livraison', array('id' => $demande->getId())),
                ));
                //Ajout du formulaire de modification des dates de livraison
                $params['formModifierDate'] = $form->createView();
            }

            if ($demande->getStatut()->getId() == Statut::EN_COURS_CP) {
                $formSpec = $this->createForm(new DemandeSpecType, $demande, array(
                    'action' => $this->generateUrl('modifier_statut_encoursdev', array(
                        'id' => $demande->getId()
                            //'users' => $users
                    )),
                    'method' => 'POST'
                ));
                //Ajout du formulaire pour le fichier de Spécification
                $params['formSpec'] = $formSpec->createView();
            }//fin if encours_cp
            elseif ($demande->getStatut()->getId() == Statut::RECETTE_USER) {
                $formRecetteUserOk = $this->createForm(new DemandeRecetteUserOkType, $demande, array(
                    'action' => $this->generateUrl('recette_user_ok', array(
                        'id' => $demande->getId(),
                    )),
                    'method' => 'POST'
                ));
                //Ajout du formulaire de validation de la recette utilisateur
                $params['formRecetteUserOk'] = $formRecetteUserOk->createView();

                $formRecetteUserNotOk = $this->createForm(new DemandeRecetteUserNotOkType, $demande, array(
                    'action' => $this->generateUrl('recette_user_nook', array(
                        'id' => $demande->getId(),
                    )),
                    'method' => 'POST'
                ));
                //Ajout du formulaire de refus de la recette utilisateur
                $params['formRecetteUserNotOk'] = $formRecetteUserNotOk->createView();

                $formChangerDateProd = $this->createForm(new DemandeChangerProdType, $demande, array(
                    'action' => $this->generateUrl('changer_date_production', array(
                        'id' => $demande->getId(),
                    )),
                    'method' => 'POST'
                ));
                //Ajout du formulaire de modification de la recette utilisateur
                $params['formChangerDateProd'] = $formChangerDateProd->createView();
            }//fin if recette user
        }//fin if après validation
        //On retourne la vue avec les paramètres nécessaires
        return $this->render('SuiviEtudesBundle:Demande:voir.html.twig', $params);
    }

//Fin function voirAction
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------

    public function ajouterAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        // $emArchi = $this->getDoctrine()->getManager('archi');

        $demande = new Demande();

        /* récupération de l'id de l'utilisateur créant la demande (et donc actuellement connecté).
          (ceci concerne les modifications des demandes pour comparer l'id du créateur et l'id de la personne connecté afin que les utilisateurs ne puissent pas modifier les demandes des autres). */
        $demandeur = $this->getUser()->getId();

        //Récupération de l'e-mail de l'utilisateur connecté.
        $userEmail = array(0 => $this->getUser()->getEmail());

        //Création du formulaire
        $form = $this->createForm(new DemandeType, $demande);

        //Récupération des catégories pour la liste déroulante
        $listeCategorie = $em->getRepository('SuiviEtudesBundle:Categorie')->findAll();
        // récuopération des mails obligatoire (l'utilisateur ne pourras pas les retirer)
        $listeMailsObligatoire = $em->getRepository('SuiviEtudesBundle:Mail')->getMailsObligatoires();
        $listeIdMail = array();
        foreach ($listeMailsObligatoire as $mail) {
            $listeIdMail[] = $mail->getId();
        }
        if ($form->handleRequest($request)->isValid()) {

            /* Enregistrement de l'id de l'utilisateur créant la demande dans demandeur_id
              (ceci concerne les modifications pour comparer l'id du créateur et l'id de la personne connecté afin que les utilisateurs ne puissent pas modifier les demandes des autres). */
            $demande->setDemandeur($demandeur);

            //Attribution du statut "nouveau" à une nouvelle demande.
            $demande->setStatut($em->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'));
            //Affectation du demandeur
            //$demande->setDemandeur($this->getUser());
            $liaisonDemandeur = new DemandeUser();
            $liaisonDemandeur->setDemande($demande);
            $liaisonDemandeur->setUser($this->getUser());
            $liaisonDemandeur->setType(DemandeUser::DEMANDEUR);
            $liaisonDemandeur->setMail($form->get('mails')->getData());
            //on enregistre la nouvelle liaison dans la table modification.

            $demande->addDemandeUser($liaisonDemandeur);
            /*
              //Récupération de la direction et du service
              if ($form->get('direction')->getData()){
              $demande->setDirection($form->get('direction')->getData()->getDirId());
              }
              if ($form->get('service')->getData()){
              $demande->setService($form->get('service')->getData()->getSrvId());
              } */

            // ## GESTION DU SUIVI ## //
            //Récupération des id utilisateurs des suivis
            $mailObs = array();
            // si l'utilisateur a coché la case suivimail, les observateurs recevront les mails sélectionnés par l'utilisateur
            $demande->setMailing($form->get('mailing')->getData());
            foreach ($form->get('suivi')->getData() as $observateur) {
                //"Réinitialisation" de l'entité DemandeUser
                $liaisonUser = new DemandeUser();
                $liaisonUser->setDemande($demande);
                $liaisonUser->setUser($observateur);
                $liaisonUser->setType(DemandeUser::OBSERVATEUR);
                $em->persist($liaisonUser);

                $mailObs[] = $observateur->getEmailCanonical();
                //Enregistrement des données.
                $demande->addDemandeUser($liaisonUser);
            }

            // multiple applications for one request
            foreach ($form->get('application')->getData() as $app) {
                $demandeApplication = new \Suivi\EtudesBundle\Entity\DemandeApplication();
                $demandeApplication->setDemande($demande);
                $demandeApplication->setApplicationId($app->getId());
                $demandeApplication->setApplication($app);
                $em->persist($demandeApplication);
            }
            $em->persist($demande);
            $em->flush();
            $em->refresh($demande);

            // ## MAIL(S) DE SUIVI ## //
            $request->getSession()->getFlashBag()->add('success', ' Votre demande a bien été enregistrée !');

            //$listeManager = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRoles('ROLE_MANAGER_TECH');
            $role = "ROLE_MANAGER_TECH";
            $query = $em->createQuery('SELECT u FROM UserUserBundle:User u WHERE u.roles LIKE :role')
                    ->setParameter('role', '%"' . $role . '"%');
            $listeManagers = $query->getResult();

            //A CHANGER POUR TEST/DEV/PROD ==> parameters.yml
            $url = $request->getScheme() . '://' . $request->getHttpHost();
            $mailManager = array();
            foreach ($listeManagers as $manager) {
                $mailManager[] = $manager->getEmailCanonical();
            }
            //on envoie le mail si le demandeur l'a mis dans la liste de ses mails à envoyer
            if (in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(1), $liaisonDemandeur->getMail()->getValues())) {
                // ## MAIL DE CONFIRMATION USER/OBSERVATEURS ## //
                //Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
                $sujet = 'Confirmation de votre demande n°' . $demande->getId() . ' : ' . $demande->getTitre();
                if ($this->container->get('kernel')->getEnvironment() != 'prod') {
                    $sujet = '[TEST]' . $sujet;
                }
                $messageSuivi = \Swift_Message::newInstance()
                        ->setSubject($sujet)
                        ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                        ->setReplyTo('poleweb@noz.fr')
                        ->setTo($this->getUser()->getEmail())
                        ->setBcc('poleweb@noz.fr')
                        ->setContentType('text/html')
                        ->setBody($this->renderView('SuiviEtudesBundle:Demande:creationDemande.html.twig', array(
                            'demande' => $demande,
                            'url' => $url,
                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                        )))
                ;
                if ($demande->getMailing()) {
                    $messageSuivi->setCc($mailObs);
                }
                //Envoi de l'e-mail de création
                $this->get('mailer')->send($messageSuivi);
            }

            // ## MAIL DE CONFIRMATION MANAGERS ## //
            //Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
            $sujet = "Création d'une demande";
            if ($this->container->get('kernel')->getEnvironment() != 'prod') {
                $sujet = '[TEST]' . $sujet;
            }
            $messageManager = \Swift_Message::newInstance()
                    ->setSubject($sujet)
                    ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                    ->setReplyTo('poleweb@noz.fr')
                    ->setTo($mailManager)
                    ->setBcc('poleweb@noz.fr')
                    ->setContentType('text/html')
                    ->setBody($this->renderView('SuiviEtudesBundle:Demande:managerDemande.html.twig', array(
                        'demande' => $demande,
                        'url' => $url,
                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                    )))
            ;

            //Envoi de l'e-mail de création
            $this->get('mailer')->send($messageManager);

            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
        }



        return $this->render('SuiviEtudesBundle:Demande:ajouter.html.twig', array(
                    'form' => $form->createView(),
                    'listeCategorie' => $listeCategorie,
                    'listeMail' => $listeIdMail
        ));
    }

//Fin méthode AjouterAction
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------

    /**
     * @author Julien Bureau<jbureau@noz.fr>
     * date : 30/10/14 -> gestion changement statut
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function modifierAction($id, Request $request) {

        //Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        //Récupération de la demande à modifier
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        // Si la demande n'existe pas, on redirige vers la liste
        if ($demande == null) {
            //message d'erreur + redirection
            $request->getSession()->getFlashBag()->add('warning', "La demande numéro " . $id . " souhaitée n'existe pas.");
            return $this->redirect($this->generateUrl('suivi_etudes_liste'));
        }
        //Récupération des PJ associés à la demande ($demande->getPieceJointes ?)
        $listePJ = $em->getRepository('SuiviEtudesBundle:PieceJointe')->getPJparDemande($id);

        /* Récupération de l'id de l'utilisateur courant et du statut manager
          (ceci concerne les modifications pour comparer l'id du créateur et l'id de la personne connecté ainsi que les statuts afin que les utilisateurs ne puissent pas modifier les demandes des autres). */
        $user = $this->getUser()->getId();

        $demandeur = $this->getDoctrine()->getRepository('SuiviEtudesBundle:DemandeUser')
                        ->getDemandeurbyDemande($demande)
                        ->getQuery()
                        ->getResult()[0];
        $listeMail = array();
        foreach ($demandeur->getMail() as $mail) {
            $listeMail[] = $mail->getId();
        }
        // récupération des mails obligatoire (l'utilisateur ne pourras pas les retirer)
        $listeMailsObligatoire = $em->getRepository('SuiviEtudesBundle:Mail')->getMailsObligatoires();
        $listeIdMail = array();
        foreach ($listeMailsObligatoire as $mail) {
            $listeIdMail[] = $mail->getId();
        }
        $userRoles = $this->getUser()->getRoles();
        $logedInUser = $this->getUser();
        /* Lecture des roles de l'utilisateur courant (modif possible pour les statuts manager et super admin)
         * (ceci concerne les modifications pour comparer l'id du créateur et l'id de la personne connecté ainsi que les statuts afin que les utilisateurs ne puissent pas modifier les demandes des autres). */
        if (false === $logedInUser->isGranted("ROLE_SUPER_ADMIN") && false === $logedInUser->isGranted("ROLE_MANAGER_TECH") && false === $logedInUser->isGranted("ROLE_TECH") && $user != $demandeur->getUser()->getId()) {
            //message d'erreur + redirection
            $request->getSession()->getFlashBag()->add('modification', "Vous ne pouvez pas modifier une demande dont vous n'êtes pas le créateur.");
            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
        }

        //Récupération de l'extension pour afficher le logo
        $extensions = null;
        foreach ($listePJ as $pj) {
            $extension = substr(strrchr($pj->getName(), '.'), 1);
            switch ($extension) {
                case 'png' :
                case 'jpg' : $img = 'jpg_64.png';
                    break;
                case 'pdf' : $img = 'pdf_64.png';
                    break;
                case 'doc' :
                case 'docx' : $img = 'doc_64.png';
                    break;
                case 'xls' :
                case 'xlsx' :
                case 'csv' : $img = 'xls_64.png';
                    break;
                case 'txt' : $img = 'txt_64.png';
                    break;
                case 'ppt' :
                case 'pptx' : $img = 'ppt_64.png';
                    break;
                default : $img = 'txt_64.png';
                    break;
            }
            $extensions[$pj->getId()] = $img;
        }

        // Et on construit le formBuilder avec cette instance
        $form = $this->createForm(new DemandeEditType($demande, $em), $demande);

        //si l'application a déja été renseignée dans la demande, on rend le champs application obligatoire.
        if ($demande->getApplication()) {
            $ApplicationObligatoire = true;
        } else {
            $ApplicationObligatoire = false;
        }

        //On regarde si l'utilisateur connecté est le demandeur
        if ($user == $demandeur->getUser()->getId()) {
            $demandeurConnecte = true;
        } else {
            $demandeurConnecte = false;
        }

        if (in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles) or ( $demandeurConnecte and $demande->getStatut()->getId() < Statut::VALIDE )) {
            $form->add('application', 'entity', array(
                'mapped' => false,
                'class' => 'SuiviEtudesBundle:Application',
                'property' => 'name',
                'empty_value' => '',
                'required' => $ApplicationObligatoire,
                'multiple' => true,
                'data' => $demande->getApplication()
            ));
        } else {
            $form->add('application', 'entity', array(
                'mapped' => false,
                'class' => 'SuiviEtudesBundle:Application',
                'property' => 'name',
                'empty_value' => '',
                'required' => $ApplicationObligatoire,
                'disabled' => true,
                'data' => $demande->getApplication()
            ));
        }
        if (in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles) or ( $demandeurConnecte and $demande->getStatut()->getId() < Statut::VALIDE )) {
            $form->add('type', 'entity', array(
                'class' => 'SuiviEtudesBundle:Type',
                'property' => 'name',
            ));
        } else {
            $form->add('type', 'entity', array(
                'class' => 'SuiviEtudesBundle:Type',
                'property' => 'name',
                'disabled' => true,
            ));
        }
        if (in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles)) {
            $form->add('titre', 'text');
        } else {
            $form->add('titre', 'text', array('read_only' => true,));
        }
        if (in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles) or ( $demandeurConnecte and $demande->getStatut()->getId() < Statut::VALIDE )) {
            $form
                    ->add('criterionFrequence', 'entity', array(
                        'class' => 'SuiviEtudesBundle:DemandeCriterionFrequence',
                        'property' => 'name',
                        'query_builder' => function($em) {
                            return $em->createQueryBuilder('table')
                                    ->orderBy('table.name', 'ASC');
                        },
                    ))
                    ->add('criterionPopulation', 'entity', array(
                        'class' => 'SuiviEtudesBundle:DemandeCriterionPopulation',
                        'property' => 'name',
                        'query_builder' => function($em) {
                            return $em->createQueryBuilder('table')
                                    ->orderBy('table.name', 'ASC');
                        },
                    ))
                    ->add('criterionOrigin', 'entity', array(
                        'class' => 'SuiviEtudesBundle:DemandeCriterionOrigin',
                        'property' => 'name',
                        'query_builder' => function($em) {
                            return $em->createQueryBuilder('table')
                                    ->orderBy('table.name', 'ASC');
                        },
            ));
        } else {
            $form->add('urgence', 'entity', array(
                'class' => 'SuiviEtudesBundle:Urgence',
                'property' => 'name',
                'disabled' => true,
            ));
        }

        if (in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles) or ( $demandeurConnecte and $demande->getStatut()->getId() < Statut::VALIDE )) {
            $form->add('description', 'textarea');
        } else {
            $form->add('description', 'textarea', array('read_only' => true,));
        }

        if (in_array("ROLE_TECH", $userRoles) or in_array("ROLE_SUPER_ADMIN", $userRoles) or in_array("ROLE_MANAGER_TECH", $userRoles)) {
            $form->add('commentaireInfo', 'textarea');
        }

        if ($demandeurConnecte) {
            $form->add('mails', 'entity', array(
                'class' => 'SuiviEtudesBundle:Mail',
                'property' => 'libelle',
                'mapped' => false,
                'multiple' => true,
                'expanded' => true
            ));

            $form->add('mailing', 'checkbox', array(
                'required' => false,
            ));
        }
        $form->handleRequest($request);

        //Validation du formulaire, et traitement
        if ($form->isValid()) {
            // remove demandeApplication
            foreach ($demande->getDemandeApplication() as $u) {
                $em->remove($u);
                $em->flush();
            }
            // add demandeApplication
            foreach ($form->get('application')->getData() as $app) {
                $demandeApplication = new \Suivi\EtudesBundle\Entity\DemandeApplication();
                $demandeApplication->setDemande($demande);
                $demandeApplication->setApplicationId($app->getId());
                $demandeApplication->setApplication($app);
                $em->persist($demandeApplication);
            }

            if ($demandeurConnecte) {
                // gestion des observateurs et du suivis par mail.
                //on enregistre la nouvelle liste de mail pour le demandeur
                $demandeur->setMail($form->get('mails')->getData());
                $em->persist($demandeur);
            }
            if ($demandeurConnecte || $this->get('security.context')->isGranted('ROLE_MANAGER_TECH')) {
                // on récupere la liste des observateurs déja enregistrés
                $listeObservateursAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getObservateursbyDemande($demande);
                // et les observateurs sélectionnés par l'utilisateurs
                $listeObservateursNouvelle = $form->get('suiviMail')->getData();
                foreach ($listeObservateursNouvelle as $nouvelObservateur) {
                    if ($listeObservateursAncienne->contains($nouvelObservateur)) {
                        //si le nouvel utilisateur est déja enregistré dans la base, on le retire de la liste des utilisateurs déja
                        // enregistrés (ainsi, il ne restera dans listeObservateursAncienne QUE les observateurs à retirer.
                        $listeObservateursAncienne->removeElement($nouvelObservateur);
                    } else {
                        // sinon on l'joute dans la base
                        $liaisonUser = new DemandeUser();
                        $liaisonUser->setDemande($demande);
                        $liaisonUser->setUser($nouvelObservateur);
                        $liaisonUser->setType(DemandeUser::OBSERVATEUR);
                        $em->persist($liaisonUser);
                    }
                }
                // on retire ensuite tout les anciens observateurs qui n'ont pas été sélectionné à nouveau par l'utilisateur
                foreach ($listeObservateursAncienne as $user) {
                    //"Réinitialisation" de l'entité DemandeUser
                    $listeObservateursAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->deleteObservateur($demande, $user);
                }
            }
            // gestion chef de projets
            // on récupere la liste des chefs de projets déja enregistrés
            $listeCDPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getChefDeProjetbyDemande($demande);
            // et les chefs de projets sélectionnés par l'utilisateurs
            $listCDPNouvelle = $form->get('chefdeProjet')->getData();
            foreach ($listCDPNouvelle as $nouveauCDP) {
                if ($listeCDPAncienne->contains($nouveauCDP)) {
                    //si le nouveau chef de projet est déja enregistré dans la base, on le retire de la liste des chef de projet déja
                    // enregistrés (ainsi, il ne restera dans listeCDPAncienne QUE les observateurs à retirer.
                    $listeCDPAncienne->removeElement($nouveauCDP);
                } else {
                    // sinon on l'zjoute dans la base
                    $liaisonUser = new DemandeUser();
                    $liaisonUser->setDemande($demande);
                    $liaisonUser->setUser($nouveauCDP);
                    $liaisonUser->setType(DemandeUser::CHEF_DE_PROJET);
                    $em->persist($liaisonUser);
                }
            }
            // on retire ensuite tout les anciens observateurs qui n'ont pas été sélectionné à nouveau par l'utilisateur
            foreach ($listeCDPAncienne as $user) {
                //"Réinitialisation" de l'entité DemandeUser
                $listeCDPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->deleteChefDeProjet($demande, $user);
            }

            // on récupere la liste des techniciens déja enregistrés
            $listeTechPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getTechniciensbyDemande($demande);
            // et les techniciens sélectionnés par l'utilisateurs
            $listeTechNouvelle = $form->get('techniciens')->getData();
            foreach ($listeTechNouvelle as $nouveauTech) {
                if ($listeTechPAncienne->contains($nouveauTech)) {
                    //si le nouveau chef de projet est déja enregistré dans la base, on le retire de la liste des chef de projet déja
                    // enregistrés (ainsi, il ne restera dans listeCDPAncienne QUE les observateurs à retirer.
                    $listeTechPAncienne->removeElement($nouveauTech);
                } else {
                    // sinon on l'ajoute dans la base
                    $liaisonUser = new DemandeUser();
                    $liaisonUser->setDemande($demande);
                    $liaisonUser->setUser($nouveauTech);
                    $liaisonUser->setType(DemandeUser::TECHNICIEN);
                    $em->persist($liaisonUser);
                }
            }
            // on retire ensuite tout les anciens observateurs qui n'ont pas été sélectionné à nouveau par l'utilisateur
            foreach ($listeTechPAncienne as $user) {
                //"Réinitialisation" de l'entité DemandeUser
                $listeTechPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->deleteTechniciens($demande, $user);
            }


            $listePJ = $em->getRepository('SuiviEtudesBundle:PieceJointe')->getPJparDemande($demande->getId());
            //on enregistre les nouvelles pièces jointes

            foreach ($form->get('piecejointes')->getData() as $piecejointe) {
                $i = 0;
                //Si la pièce jointe existe déjà
                foreach ($listePJ as $pj) {
                    if ($pj->getName() == $piecejointe->getName()) {
                        //On remplace le fichier existant, et on garde la ligne PJ existante
                        $pj->setFile($piecejointe->getFile());
                        $pj->upload();
                        $i++;
                    }
                }
                if ($i == 0) {
                    $piecejointe->setDemande($demande);
                    $em->persist($piecejointe);
                }
            }
            //On flush le tout.
            $em->flush();

            //et à la fin : avertissement pour le user +
            // On redirige vers la page de visualisation de la demande récemment modifiée
            $request->getSession()->getFlashBag()->add('success', 'Demande bien modifiée.');
            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
        }
        //Affichage du formulaire de modification de la demande.
        return $this->render('SuiviEtudesBundle:Demande:modifier.html.twig', array(
                    'demande' => $demande,
                    'PiecesJointes' => $listePJ,
                    'extensions' => $extensions,
                    'listemail' => $listeMail,
                    'listemailobligatoire' => $listeIdMail,
                    'demandeurconnecte' => $demandeurConnecte,
                    'form' => $form->createView(),
        ));
    }

//fin modifierAction
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
    public function supprimerAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'annonce $id
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        if (null === $demande) {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        }

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille
        $form = $this->createFormBuilder()->getForm();

        if ($form->handleRequest($request)->isValid()) {
            $em->remove($demande);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', "La demande a bien été supprimée.");

            return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
        }

        // Si la requête est en GET, on affiche une page de confirmation avant de supprimer
        return $this->render('SuiviEtudesBundle:Demande:supprimer.html.twig', array(
                    'demande' => $demande,
                    'form' => $form->createView()
        ));
    }

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------



    public function menuAction($type, $limit = 5) {
        $demandeRepo = $this->getDoctrine()->getManager()->getRepository('SuiviEtudesBundle:Demande');
        $filtre = array();

        switch ($type) {
            case 'manager' :
                $filtre['statut'] = array();
                $filtre['statut'][] = Statut::NOUVEAU;
                $listeDemandes = $demandeRepo->getNouvelleDemande($limit);
                break;
            case 'technicien':
                $filtre['technicien'] = $this->getUser();
                $listeDemandes = $demandeRepo->getDemandeAffectee($this->getUser(), $limit);
                break;
            case 'demandeur':
                $filtre['demandeur'] = $this->getUser();
                $listeDemandes = $demandeRepo->getDemandeCree($this->getUser(), $limit);
                break;
        }
        return $this->render('SuiviEtudesBundle:Demande:menu.html.twig', array(
                    'listDemandes' => $listeDemandes,
                    'demande_search' => $filtre,
                    'type' => $type,
        ));
    }

//Fin menuAction

    /**
     *
     * @param INT $id ID de la Demande
     * @return ARRAY $data Tableau des historique de la demande
     */
    public function HistoriqueAction($id) {
        $em = $this->getDoctrine()->getManager();
        //récupération de l'historique
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $historique = $em->getRepository('SuiviEtudesBundle:Modification')->getModificationsParDemande($demande);

        $tableauHistorique = array();
        foreach ($historique as $modification) {

            $ligneHistorique['date'] = $modification->getDate();
            if ($modification->getUser() != NULL) {
                $ligneHistorique['user'] = $modification->getUser()->getDisplayname();
            } else {
                $ligneHistorique['user'] = "Automatique";
            }
            if ($modification->getAncienneValeur() == "") {
                $modification->setAncienneValeur("__");
            }
            switch ($modification->getType()) {
                case 'statut' :
                    $ligneHistorique['type'] = "Statut";
                    $nouvelleVal = $modification->getNouvelleValeur();
                    $ancienneVal = $modification->getAncienneValeur();
                    if ($nouvelleVal == $ancienneVal) {
                        $ligneHistorique['action'] = "Demande de mise en <b>" . $nouvelleVal . "</b>";
                    } else {
                        $ligneHistorique['action'] = "Passage au statut <b>" . $modification->getNouvelleValeur() . "</b>";
                    }
                    break;
                case 'titre':
                    $ligneHistorique['type'] = "Titre";
                    $ligneHistorique['action'] = "Modification du titre : <b>" . $modification->getAncienneValeur() . "</b> devient <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'description':
                    $ligneHistorique['type'] = "Description";
                    $ligneHistorique['action'] = "Nouvelle description : <b>" . $modification->getAncienneValeur() . "</b> devient <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'dateRecette':
                    $ligneHistorique['type'] = "Date de recette";
                    $ligneHistorique['action'] = "Nouvelle date de recette : <b>" . date("d/m/Y", strtotime($modification->getAncienneValeur())) . "</b> à <b>" . date("d/m/Y", strtotime($modification->getNouvelleValeur())) . "</b>";
                    break;
                case 'dateProduction':
                    $ligneHistorique['type'] = "Date de Livraison";
                    $ligneHistorique['action'] = "Nouvelle date de mise en production: <b>" . date("d/m/Y", strtotime($modification->getNouvelleValeur())) . "</b>";
                    break;
                case 'charges':
                    $ligneHistorique['type'] = "Charge";
                    $ligneHistorique['action'] = "Nouvelle charge communiquée :  <b>" . $modification->getNouvelleValeur() . " heure(s)</b>";
                    break;
                case 'chargesCP':
                    $ligneHistorique['type'] = "Charge CP";
                    $ligneHistorique['action'] = "Nouvelle charge d'analyse : <b>" . $modification->getNouvelleValeur() . " heure(s)</b>";
                    break;
                case 'chargesDev':
                    $ligneHistorique['type'] = "Charge Dev";
                    $ligneHistorique['action'] = "Nouvelle charge de développement : <b>" . $modification->getNouvelleValeur() . " heure(s)</b>";
                    break;
                case 'chargesCPRestante':
                    $ligneHistorique['type'] = "Charge CP restante";
                    $ligneHistorique['action'] = "Nouvelle charge restante d'analyse : <b>" . $modification->getNouvelleValeur() . " heure(s)</b>";
                    break;
                case 'chargesDevRestante':
                    $ligneHistorique['type'] = "Charge Dev restante";
                    $ligneHistorique['action'] = "Nouvelle charge restante de développement : <b>" . $modification->getNouvelleValeur() . " heure(s)</b>";
                    break;
                case 'urgence':
                    $ligneHistorique['type'] = "Urgence";
                    $ligneHistorique['action'] = "Changement de l'urgence : <b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'type':
                    $ligneHistorique['type'] = "Type";
                    $ligneHistorique['action'] = "Changement du type de demande: <b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'application':
                    $ligneHistorique['type'] = "Application";
                    $ligneHistorique['action'] = "Changement de l'application : <b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'direction':
                    $ligneHistorique['type'] = "Direction";
                    $ligneHistorique['action'] = "Changement de la direction : <b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'service':
                    $ligneHistorique['type'] = "Service";
                    $ligneHistorique['action'] = "Changement du service : <b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'recette':
                    $ligneHistorique['type'] = "Recette";
                    if ($modification->getNouvelleValeur() == 1) {
                        $ligneHistorique['action'] = "Validation de la recette.";
                    } else {
                        $ligneHistorique['action'] = "Recette refusée.";
                    }
                    break;
                case 'devis':
                    $ligneHistorique['type'] = "Devis";
                    $ligneHistorique['action'] = "Devis <b>" . $modification->getNouvelleValeur() . "</b> joint à la demande.";
                    break;
                case 'motifRefus':
                    $ligneHistorique['type'] = "Estimation refusée";
                    $ligneHistorique['action'] = "Motif du refus des estimations : <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'descriptionRefus':
                    $ligneHistorique['type'] = "Estimation refusée";
                    $ligneHistorique['action'] = "Description du refus des estimations: <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'commentaireRecette':
                    $ligneHistorique['type'] = "Commentaire";
                    $ligneHistorique['action'] = "Nouveau commentaire de recette : <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'modifProd':
                    $ligneHistorique['type'] = "Date de Livraison";
                    $ligneHistorique['action'] = "Nouvelle date de mise en production: <b>" . date("d/m/Y", strtotime($modification->getNouvelleValeur())) . "</b>";
                    break;
                case 'commentaireProd':
                    $ligneHistorique['type'] = "Commentaire";
                    $ligneHistorique['action'] = "Nouveau commentaire de mise en production : <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'commentaireInfo':
                    $ligneHistorique['type'] = "Commentaire";
                    $ligneHistorique['action'] = "Changement du commentaire : <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
                case 'spec':
                    $ligneHistorique['type'] = "Cahier de Spec";
                    $ligneHistorique['action'] = "Cachier de spécification <b>" . $modification->getNouvelleValeur() . "</b> joint";
                    break;
                case 'OBSERVATEUR':
                    $ligneHistorique['type'] = "Affectation";
                    $ligneHistorique['action'] = " Ajout de <b>" . $modification->getNouvelleValeur() . "</b> en tant qu'observateur.";
                    break;
                case 'CHEF_DE_PROJET':
                    $ligneHistorique['type'] = "Affectation";
                    $ligneHistorique['action'] = " Ajout de <b>" . $modification->getNouvelleValeur() . "</b> en tant que chef de projet.";
                    break;
                case 'TECHNICIEN':
                    $ligneHistorique['type'] = "Affectation";
                    $ligneHistorique['action'] = " Ajout de <b>" . $modification->getNouvelleValeur() . "</b> en tant que technicien.";
                    break;
                case '':
                    $ligneHistorique['type'] = "";
                    $ligneHistorique['action'] = "";
                    break;
                case '':
                    $ligneHistorique['type'] = "";
                    $ligneHistorique['action'] = "";
                    break;

                default:
                    $ligneHistorique['type'] = "-- " . $modification->getType();
                    $ligneHistorique['action'] = "<b>" . $modification->getAncienneValeur() . "</b> à <b>" . $modification->getNouvelleValeur() . "</b>";
                    break;
            }
            //$ligneHistorique['action'] .= "<b>".$modification->getAncienneValeur()."</b> à <b>".$modification->getNouvelleValeur()."</b>";
            $tableauHistorique[] = $ligneHistorique;
        }



        return $this->render('SuiviEtudesBundle:Demande:historique.html.twig', array(
                    'historique2' => $tableauHistorique,
        ));
    }

    public function demandesenstandByAction($page, $onPage) {
        $request = $this->container->get('request');
//        $newOnPage = $request->query->get('onPage');
//        if (null !== $newOnPage) {
//            $onPage = $newOnPage;
//        }
        $sortArray = array();
        $sort = null !== $request->query->get('sort') && in_array($request->query->get('sort'), $sortArray) ? $request->query->get('sort') : 'sb.createdAt';
        $direction = null !== $request->query->get('direction') && $request->query->get('direction') == 'desc' ? $request->query->get('direction') : 'ASC';
        $em = $this->getDoctrine()->getManager();
        $standBy = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::STAND_BY);

        $data = $em->createQueryBuilder('query')
                        ->select('sb')
                        ->from('SuiviEtudesBundle:DemandeStandBy', 'sb')
                        ->innerJoin('SuiviEtudesBundle:Demande', 'd', 'WITH', 'd.id=sb.demande')
                        ->where('d.statut =:standBy')
                        ->orWhere('sb.validated=0')
                        ->setParameter('standBy', $standBy)
                        ->groupBy('d.id')
                        ->orderBy('sb.id', 'ASC')
//                        ->orderBy($sort, $direction)
                        ->getQuery()->getResult();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $data, $request->query->getInt('page', $page)/* page number */
                , $onPage/* limit per page */
        );

        $standByRequests = $em->createQueryBuilder('query')
                        ->select('COUNT(distinct(d)) standby')
                        ->from('SuiviEtudesBundle:DemandeStandBy', 'sb')
                        ->innerJoin('SuiviEtudesBundle:Demande', 'd', 'WITH', 'd.id=sb.demande')
                        ->where('d.statut =:standBy')
                        ->orWhere('sb.validated=0')
                        ->setParameter('standBy', $standBy)
                        ->getQuery()->getOneOrNullResult();
        $waitingStandByRequests = $em->createQueryBuilder('query')
                        ->select('COUNT(distinct(sb.demande)) waiting')
                        ->from('SuiviEtudesBundle:DemandeStandBy', 'sb')
                        ->where('sb.validated=0')
                        ->getQuery()->getOneOrNullResult();
        return $this->render('SuiviEtudesBundle:Demande:standByDemand.html.twig', array(
                    'pagination' => $pagination,
                    'onPage' => $onPage,
                    'page' => $page,
                    'standByRequests' => $standByRequests,
                    'waitingStandByRequests' => $waitingStandByRequests,
        ));
    }

    public function standbyCountAction() {
        $em = $this->getDoctrine()->getManager();
        $waitingStandBy = $em->createQueryBuilder('query')
                        ->select('COUNT(distinct(sb.demande)) waiting')
                        ->from('SuiviEtudesBundle:DemandeStandBy', 'sb')
                        ->where('sb.validated=0')
                        ->getQuery()->getOneOrNullResult();
        return $this->render('SuiviEtudesBundle:Demande:standByCount.html.twig', array(
                    'waitingStandBy' => $waitingStandBy,
        ));
    }

    public function defaultrequestcolumnsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $userPreference = $em->getRepository('SuiviEtudesBundle:DemandeUserColumns')->findOneBy(array('userId' => $userId));
        if (empty($userPreference)) {
            $userPreference = new \Suivi\EtudesBundle\Entity\DemandeUserColumns();
            $userPreference->setUserId($userId);
        }
        $form = $this->createForm(new \Suivi\EtudesBundle\Form\DemandeUserColumnsType(), $userPreference);
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($userPreference);
                $em->flush();
            }
            $message = 'Vos préférences a été sauvegardé.';
            return new \Symfony\Component\HttpFoundation\Response($message);
        }
        return $this->render('SuiviEtudesBundle:Demande:demandecolumns.form.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    protected function filterDemandesForUser($userId) {
        $request = $this->getRequest();
        $cookies = $request->cookies;
        $hasPref = $cookies->get('suiviUserFilters');
        $urlParams = $request->query->get('demande_search');
        if (null !== $hasPref) {
            $url = $this->generateUrl('suivi_etudes_liste', (array) json_decode($hasPref));
            return $url;
        }
        return null;
    }

    public function saveFiltersAction(Request $request, $userId) {
        // get demande_search form vars
        $postVars = $request->request->get('demande_search');
        $response = new Response();
        $cookieExpireDate = time() + (60 * 60 * 24 * 365); // 1 year
        $userPref = new \Symfony\Component\HttpFoundation\Cookie('suiviUserFilters', json_encode($postVars), $cookieExpireDate);
        $response->headers->setCookie($userPref);
        $response->send();

        return new Response(json_encode($postVars));
    }

    public function deleteFiltersAction($userId) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $searchPreferences = $em->getRepository('SuiviEtudesBundle:DemandeSearch')->findOneBy(
                array('user_id' => $userId)
        );
        if (null !== $searchPreferences) {
            $em->remove($searchPreferences);
        }
        $response = new Response();
        $response->headers->clearCookie('suiviUserFilters');
        $response->send();

        return new Response('Votre filtres ont été supprimés.');
    }

    public function technicalDocumentAction(Request $request, $demandeId) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($demandeId);
        if (null !== $demande && $this->get('security.context')->isGranted('ROLE_TECH') && $demande->getStatut()->getId() >= Statut::VALIDE && $demande->getStatut()->getId() < Statut::CLOS) {
            $docs = $em->getRepository('SuiviEtudesBundle:DemandeDocs')->findOneBy(array('demande' => $demande));
            if (null === $docs) {
                $docs = new \Suivi\EtudesBundle\Entity\DemandeDocs();
            }
            $fileExist = file_exists($this->container->getParameter('kernel.root_dir') . '/../web/bundles/suivietudes/uploads/' . $demande->getId() . '/' . $docs->getName());
            $form = $this->createForm(new \Suivi\EtudesBundle\Form\DemandeDocsType($demande));
            $form->handleRequest($request);
            if ($request->isMethod('POST')) {
                // do modifs if request is not closed
                if ($form->isValid()) {
                    $path = $form['path']->getData();
                    $name = $form['name']->getData();
                    $type = $request->request->get('demandedocs')['type'];

                    if ((empty($path) && empty($name)) == true) {
                        $request->getSession()->getFlashBag()->add('erreur', 'Vous avez oublié d\'ajouter un fichier ou un chemin.');
                    } else {
                        $uploadDir = $this->container->getParameter('kernel.root_dir') . '/../web/bundles/suivietudes/uploads/' . $demande->getId();

                        // delete file + directory from hdd 
                        if ($fileExist == true) {
                            unlink($uploadDir . '/' . iconv("UTF-8", "iso-8859-2", $docs->getName()));
                            rmdir($uploadDir);
                        }
                        // upload file only if one was uploaded
                        if (null !== $name) {
                            // encode file name for french accents
                            $fileName = iconv("UTF-8", "iso-8859-2", $name->getClientOriginalName());
                            if ($type == 'upload') {
                                $fileName = str_replace("#", "", $fileName);
                            }
                            $name->move($uploadDir, $fileName);
                        }

                        // database operation
                        $docs->setDemande($demande);
                        if (null !== $name) {
                            $docs->setPath($_SERVER['BASE'] . '/bundles/suivietudes/uploads/' . $demande->getId() . '/' . $fileName);
                            $docs->setName($fileName);
                        } else {
                            $docs->setPath($path);
                            $docs->setName($path);
                        }
                        $em->persist($docs);
                        $em->flush();
                    }
                }
                return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demandeId)));
            }
            return $this->render('SuiviEtudesBundle:Demande:form.demandeDocs.html.twig', array(
                        'form' => $form->createView(),
                        'demandeId' => $demandeId,
            ));
        }
        return new Response('No demande found.');
    }

}
