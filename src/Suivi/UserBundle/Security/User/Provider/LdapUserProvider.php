<?php

namespace Suivi\UserBundle\Security\User\Provider;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException,
    Symfony\Component\Security\Core\Exception\UnsupportedUserException,
    Symfony\Component\Security\Core\User\UserProviderInterface,
    Symfony\Component\Security\Core\User\UserInterface/*,
    Symfony\Component\DependencyInjection\ContainerInterface*/;

use IMAG\LdapBundle\Manager\LdapManagerUserInterface,
    IMAG\LdapBundle\User\LdapUserInterface;
use FOS\UserBundle\Model\UserManagerInterface;

class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var \IMAG\LdapBundle\Manager\LdapManagerUserInterface
     */
    private $ldapManager;

    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     */
    protected $userManager;

    /**
     * @var \Symfony\Component\Validator\Validator
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param LdapManagerUserInterface $ldapManager
     * @param UserManagerInterface     $userManager
     * @param Validator                $validator
     */
    public function __construct(LdapManagerUserInterface $ldapManager, UserManagerInterface $userManager, $validator)
    {
        $this->ldapManager = $ldapManager;
        $this->userManager = $userManager;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        // Throw the exception if the username is not provided.
        if (empty($username)) {
            throw new UsernameNotFoundException('The username is not provided.');
        }

        // check if the user is already know to us
        $user = $this->userManager->findUserBy(array("username" => $username));

        // Throw an exception if the username is not found.
        if(empty($user) && !$this->ldapManager->exists($username)) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found', $username));
        }

        $lm = $this->ldapManager
            ->setUsername($username)
            ->doPass();

        
        // create user if no exists
        if (empty($user)) {
            $user = $this->userManager->createUser();
            $user->setRoles($lm->getRoles());

            $user
                ->setUsername($lm->getUsername())
                ->setPassword("")
                ->setDn($lm->getDn())
                ->setEmail($lm->getEmail())
                ->setGivenName($lm->getGivenName())
                ->setSurname($lm->getSurname())
                ->setDisplayName($lm->getDisplayName())
            	->setDn($lm->getDn())
                ->setMatricule($lm->getAttributes()['employeeid']); //Récupération du matricule de l'utilisateur
                       
            //Si la chaine est présente dans la chaine Dn, on supprime le role par defaut et on ajoute TECH
            if (strstr($lm->getDn(),',OU=Departement Etudes,OU=Informatique,')) {
            	$user->removeRole('ROLE_USER_DEFAULT');
            	$user->addRole('ROLE_TECH');
            }
            
            /*
             * Boucles pour l'affectation de role
             * A modifier si vous souhaitez vous affecter un role en particulier
             */
            
            //SUPER ADMIN
                // A modifier lors de la mise en production : ROLE_USER_DEFAULT
            if ($user->getUsername() == "bureau_ju" ||
                    $user->getUsername() == "allard_pi" ||
                        $user->getUsername() == "lolive_mi") {
                $user->addRole('ROLE_SUPER_ADMIN');
            }
            //MANAGERS TECHNIQUES
            if ($user->getUsername() == "danjou_gi" ||
                    $user->getUsername() == "lemeur_ra" ||
                        $user->getUsername() == "robin_ol" ||
                            $user->getUsername() == "gilles_gw") {
            	$user->addRole('ROLE_MANAGER_TECH');
            }
            
            //LISTE DES DEMANDEUR
            $listeDemandeur = array(
            //Contrôle de gestion
                "ntrebon@noz.fr",
                "htatin@noz.fr",
                "aroldan@noz.fr",
                "jtouchard@noz.fr",
                "mbouvier@noz.fr",
                "gkapraras@noz.fr",
                "jdeslandes@noz.fr",
                "jhemery@noz.fr",
                "fbienaime@noz.fr",
                
                "mayach@noz.fr",
                "sbeauclair@noz.fr",
                "bcinier@noz.fr",
                "aaliev@noz.fr",
                "jhoussemagne@noz.fr",
                "tguegan@noz.fr",
            //Comptabilité    
                "abryon@noz.fr",
                "esimon@noz.fr",
                "cragaigne@noz.fr",
                "arobin@noz.fr",
                
            //DV
                "pavram@veo-finances.com",
                "ebazimon@noz.fr",
            );
            if (in_array(strtolower($user->getEmail()), $listeDemandeur)) {
            	$user->removeRole('ROLE_USER_DEFAULT');
            	$user->addRole('ROLE_DEMANDEUR');
            }
            
            
            //LISTE DES VALIDEUR
            $listeValideur = array(
                //Contrôle de gestion
                "gwilpart@noz.fr",
                "sdesille@noz.fr",
                "mnobilleau@noz.fr",
                
                "bnguyen@noz.fr",
                
                "ldumortier@noz.fr",
                
                "jcharrot@noz.fr",
                
                "mlelasseux@noz",
                //Demandeur ?
                "viordache@veo-finances.com",
                "aursache@veo-finances.com",
                "cdanescu@veo-finances.com",
                "eandronie@veo-finances.com",
                // **
                
                "narnaud@noz.fr",
                "krossignol@noz.fr",
                "fchable@noz.fr",
                "sndiaye@noz.fr",
                "ttoquet@noz.fr",
                "vhelaine@noz.fr",
                "abellanger@noz.fr",
                "fndjoulani@noz.fr",
                "elebris@noz.fr",
                "pboutin@noz.fr",
                "dbrochard@noz.fr",
                "mbroudin@noz.fr",
                "ggilles@noz.fr",
                "specastaing@noz.fr",
                
            );
        
                
            if (in_array(strtolower($user->getEmail()), $listeValideur)) {
            	$user->removeRole('ROLE_USER_DEFAULT');
                $user->addRole('ROLE_VALIDEUR');
            }
            
            
                
            //LISTE DU COPIL
            $listeCOPIL = array(
                //Contrôle de gestion
                "lsalaun@noz.fr",
                "mdurieux@noz.fr",
                "tcarlucci@noz.fr",
                "jadrion@noz.fr",
                "vbertho@noz.fr",
                "pchedor@noz.fr",
                "radrion@noz.fr",
            );
            
                
            if (in_array(strtolower($user->getEmail()), $listeCOPIL)) {
            	$user->removeRole('ROLE_USER_DEFAULT');
                $user->addRole('ROLE_COPIL');
            }
            
            
            $this->userManager->updateUser($user);

        }
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $this->userManager->supportsClass($class);
    }
}
