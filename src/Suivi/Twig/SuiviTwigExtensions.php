<?php

namespace Suivi\Twig;

class SuiviTwigExtensions extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('html_decode', array($this, 'html_decodeFilter')),
        );
    }

    public function html_decodeFilter($value) {
        return html_entity_decode($value);
    }

    public function getName() {
        return 'twig_extension';
    }

}
