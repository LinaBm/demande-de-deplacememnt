<?php

namespace Indicateurs\DAMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;


class EquipeController extends Controller
{
	
	public function affichageTVAction(){
		/* récupération des chiffres globaux */
		$page = "";
		$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
		$fp = fopen($url."alpha_var.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objDAM=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$data['objectif_DAM']=$objDAM;
		$realisation=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$data['obj_dossier_DAM']=(int)ltrim($arr1[3]);
		$data['dossier_DAM']=(int)trim($arr1[4]);
		$realisation = (Double)$realisation;
		$neg=stripos($realisation, "-");
		if ($neg!=false)
		{
			$arr2 = explode("-",$realisation);
			$realisation=$arr2[0]*-1;
		}
		$data['realisation_DAM']=intval($realisation);
		$data['objectif_dam']=intval($objDAM-$realisation);
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE UK ***************/
		$page="";
		$fp = fopen($url."TERUK.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$data['objectif_UK']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$data['objectif_terrain']=$data['objectif_UK'];
		
		$uk =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($uk, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$uk);
			$uk=$arr2[0]*-1;
		}
		$data['realisation_UK']=$uk;
		$data['objectif_terrain']=$data['objectif_UK'];
		$data['realisation_terrain']=$uk;
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE UK ***************/

		$page = "";
		$page = file($url."acheteurs_TERUK.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);		
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_UK']=$total_dossier_deposes;
		$data['obj_dossier_UK']=$total_objectif_dossier;
		
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ALLEMAGNE ***************/
		$page="";
		$fp = fopen($url."TERALLEMAGNE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$data['objectif_ALL']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$allemagne =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($allemagne, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$allemagne);
			$allemagne=$arr2[0]*-1;
		}
		$data['realisation_ALL']=$allemagne;
		$data['objectif_terrain']=$data['objectif_terrain'] + $data['objectif_ALL'];
		$data['realisation_terrain']=$data['realisation_terrain']+$allemagne;		
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE ALLEMAGNE ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERALLEMAGNE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes + (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_ALL']=$total_dossier_deposes;
		$data['obj_dossier_ALL']=$total_objectif_dossier;
		
		
		
		/*****************************************************************************/
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ESPAGHNE ***************/
		
		$page="";
		$fp = fopen($url."TERESPAGNE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$data['objectif_esp']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$espagne =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($espagne, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$espagne);
			$espagne=$arr2[0]*-1;
		}
		$data['realisation_esp']=$espagne;
		$data['objectif_terrain']=$data['objectif_terrain'] + $data['objectif_esp'];
		$data['realisation_terrain']=$data['realisation_terrain']+$espagne;
		/*****************************************************************************/

		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE ALLEMAGNE ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERESPAGNE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_ESP']=$total_dossier_deposes;
		$data['obj_dossier_ESP']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ITALIE ***************/
		
		$page="";
		$fp = fopen($url."TERITALIE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objita=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$italie =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($italie, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$italie);
			$italie=$arr2[0]*-1;
		}
		$data['objectif_ita']=$objita;
		$data['realisation_ita']=$italie;
		$data['objectif_terrain']=$data['objectif_terrain'] + $objita;
		$data['realisation_terrain']=$data['realisation_terrain']+$italie;		
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE italie ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERITALIE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_ITA']=$total_dossier_deposes;
		$data['obj_dossier_ITA']=$total_objectif_dossier;
		
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE OUEST ***************/
		$page="";
		$fp = fopen($url."FRANCEOUEST.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfro=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_ouest =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_ouest, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_ouest);
			$france_ouest=$arr2[0]*-1;
		}
		$data['objectif_fro']=$objfro;
		$data['realisation_fro']=$france_ouest;
		$data['objectif_terrain']=$data['objectif_terrain'] + $objfro;
		$data['realisation_terrain']=$data['realisation_terrain']+$france_ouest;
		/*****************************************************************************/

		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE OUEST ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCEOUEST.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_FRAOUEST']=$total_dossier_deposes;
		$data['obj_dossier_FRAOUEST']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE EST ***************/
		$page="";
		$fp = fopen($url."FRANCEEST.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfre=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_est =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_est, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_est);
			$france_est=$arr2[0]*-1;
		}
		$data['objectif_fre']=$objfre;
		$data['realisation_fre']=$france_est;
		$data['objectif_terrain']=$data['objectif_terrain'] + $objfre;
		$data['realisation_terrain']=$data['realisation_terrain']+$france_est;
		
		/*****************************************************************************/
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE EST ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCEEST.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_FRAEST']=$total_dossier_deposes;
		$data['obj_dossier_FRAEST']=$total_objectif_dossier;		
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE SUD ***************/
		$page="";
		$fp = fopen($url."FRANCESUD.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfrs=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_sud =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_sud, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_sud);
			$france_sud=$arr2[0]*-1;
		}
		$data['objectif_frs']=$objfrs;
		$data['realisation_frs']=$france_sud;
		$data['objectif_terrain']=$data['objectif_terrain'] + $objfrs;
		$data['realisation_terrain']=$data['realisation_terrain']+$france_sud;
		/*****************************************************************************/

		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE SUD ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCESUD.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_FRASUD']=$total_dossier_deposes;
		$data['obj_dossier_FRASUD']=$total_objectif_dossier;
				
		/*************** RECUPERATION DES DONNEES SEDENTAIRE MARKETING ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIRETEXTILE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objmark=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$marketing =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($marketing, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$marketing);
			$marketing=$arr2[0]*-1;
		}
		$data['objectif_textile']=$objmark;
		$data['realisation_textile']=$marketing;
		$data['objectif_sedentaire']=$objmark;
		$data['realisation_sedentaire']=$marketing;
		
		/*****************************************************************************/

		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE MARKETING ***************/
		
		$page = "";
		$page = file($url."acheteurs_SEDENTAIRETEXTILE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_textile']=$total_dossier_deposes;
		$data['obj_dossier_textile']=$total_objectif_dossier;		
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRES GC ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREGC.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objgc=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$grandcompte =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($grandcompte, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$grandcompte);
			$grandcompte=$arr2[0]*-1;
		}
		$data['objectif_gc']=$objgc;
		$data['realisation_gc']=$grandcompte;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objgc;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $grandcompte;
		/*****************************************************************************/

		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE GRAND COMPTE ***************/
		
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREGC.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_GC']=$total_dossier_deposes;
		$data['obj_dossier_GC']=$total_objectif_dossier;
		
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRES BELGIQUE ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREBELGIQUE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$belgique =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($belgique, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$belgique);
			$belgique=$arr2[0]*-1;
		}
		$data['objectif_bel']=$objbelg;
		$data['realisation_bel']=$belgique;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objbelg;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $belgique;
		
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREBELGIQUE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_BEL']=$total_dossier_deposes;
		$data['obj_dossier_BEL']=$total_objectif_dossier;

		/*************** RECUPERATION DES DONNEES SEDENTAIRES FRANCE ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREFRANCE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france);
			$france=$arr2[0]*-1;
		}
		$data['objectif_bel']=$objbelg;
		$data['realisation_bel']=$france;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objbelg;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $france;
		
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREBELGIQUE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_FR']=$total_dossier_deposes;
		$data['obj_dossier_FR']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRES BELGIQUE ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREMOGADOR.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$belgique =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($belgique, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$belgique);
			$belgique=$arr2[0]*-1;
		}
		$data['objectif_mog']=$objbelg;
		$data['realisation_mog']=$belgique;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objbelg;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $belgique;
		
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREMOGADOR.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_mog']=$total_dossier_deposes;
		$data['obj_dossier_mog']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe1 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE1.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur1=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$eur1 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur1, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur1);
			$eur1=$arr2[0]*-1;
		}
		$data['objectif_eur1']=$objeur1;
		$data['realisation_eur1']=$eur1;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objeur1;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $eur1;		
		/*****************************************************************************/		
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE1.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_EURO1']=$total_dossier_deposes;
		$data['obj_dossier_EURO1']=$total_objectif_dossier;
		
		
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe2 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE2.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur2=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$eur2 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur2, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur2);
			$eur2=$arr2[0]*-1;
		}
		$data['objectif_eur2']=$objeur2;
		$data['realisation_eur2']=$eur2;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objeur2;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $eur2;
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE2.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_EURO2']=$total_dossier_deposes;
		$data['obj_dossier_EURO2']=$total_objectif_dossier;
		/*****************************************************************************/
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe3 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE3.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur3=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$eur3 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur3, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur3);
			$eur3=$arr2[0]*-1;
		}
		$data['objectif_eur3']=$objeur3;
		$data['realisation_eur3']=$eur3;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objeur3;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $eur3;
		/*****************************************************************************/
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE3.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_EURO3']=$total_dossier_deposes;
		$data['obj_dossier_EURO3']=$total_objectif_dossier;
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe4 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE4.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur4=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$eur4 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur4, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur4);
			$eur4=$arr2[0]*-1;
		}
		$data['objectif_eur4']=$objeur4;
		$data['realisation_eur4']=$eur4;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objeur4;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $eur4;
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE4.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_EURO4']=$total_dossier_deposes;
		$data['obj_dossier_EURO4']=$total_objectif_dossier;
		/*****************************************************************************/
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe5 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE5.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur5=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$eur5 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		$neg=stripos($eur5, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur5);
			$eur5=$arr2[0]*-1;
		}
		$data['objectif_eur5']=$objeur5;
		$data['realisation_eur5']=$eur5;
		$data['objectif_sedentaire']=$data['objectif_sedentaire'] + $objeur5;
		$data['realisation_sedentaire']=$data['realisation_sedentaire'] + $eur5;
		/*****************************************************************************/
		
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE5.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$data['dossier_EURO5']=$total_dossier_deposes;
		$data['obj_dossier_EURO5']=$total_objectif_dossier;
		return $this->render('IndicateursDAMBundle:TvDam:tv.html.twig',array(
				'data'=>$data
		));
	}

	public function voirRoumanieAction($equipe)
	{
		switch($equipe) {
			case 'sedallemagne_bv':
				$titre = "Sédentaire Allemagne BV";
				break;
			case 'sedallemagne_kt':
				$titre = "Sédentaire Allemagne KT";
				break;
			case 'sedcoachdare':
				$titre = "Sédentaire Coach Dare";
				break;
			case 'seddanemark_is':
				$titre = "Sédentaire Danemark";
				break;
			case 'sedeuropeest_is':
				$titre = "Sédentaire Europe de l'Est";
				break;
			case 'sedhongrie_bv':
				$titre = "Sédentaire Hongrie";
				break;
			case 'sedmediterrane_bv':
				$titre = "Sédentaire Méditerranée BV";
				break;
			case 'sedmediterrane_ct':
				$titre = "Sédentaire Méditerranée CT";
				break;		
			case 'sedmediterrane_is':
				$titre = "Sédentaire Méditerranée IS";
				break;
			case 'sednorvege_is':
				$titre = "Sédentaire Norvege IS";
				break;
			case 'sedroumanie_is':
				$titre = "Sédentaire Roumanie";
				break;
			case 'sedturquie1_ct':
				$titre = "Sédentaire Turquie 1";
				break;
			case 'sedturquie2_ct':
				$titre = "Sédentaire Turquie 2";
				break;
		}
		return  $this->voir($titre, $equipe);
	}
	
    public function voirSedentaireAction($equipe)
    {
        switch($equipe) {
            case 'sedentaireeurope1':
                $titre = "Sédentaire Europe 1";
                break;
            case 'sedentaireeurope2':
                $titre = "Sédentaire Europe 2";
                break;
            case 'sedentaireeurope3':
                $titre = "Sédentaire Europe 3";
                break;
            case 'sedentaireeurope4':
                $titre = "Sédentaire Europe 4";
                break;
            case 'sedentaireeurope5':
                $titre = "Sédentaire Europe 5";
                break;
            case 'sedentairefrance':
                $titre = "Sédentaire France";
            break;
            case 'sedentairebelgique':
            	$titre = "Sédentaire Belgique";
            	break;
            case 'sedentairegc':
                $titre = "Sédentaire Grand Comptes";
                break;
            case 'sedentairemogador':
                $titre = "Sédentaire Mogador";
            	break;
            case 'sedentairetextile':
              	$titre = "Sédentaire Textile";
            break;
        }
		return  $this->voir($titre, $equipe);
    }
    

    public function voirTerrainAction($equipe)
    {
    	switch($equipe) {
    		case 'teruk':
    			$titre = "Terrain Royaume-Unis";
    			break;
    		case 'terallemagne':
    			$titre = "Terrain Allemagne";
    			break;
    		case 'terespagne':
    			$titre = "Terrain Espagne";
    			break;
    		case 'teritalie':
    			$titre = "Terrain Italie";
    			break;
    		case 'franceest':
    			$titre = "France Est";
    			break;
    		case 'franceouest':
    			$titre = "France Ouest";
    			break;
    		case 'francesud':
    			$titre = "France Sud";
    			break;
    	}
    	return  $this->voir($titre, $equipe);
    }
    
    // indicateurs/dam/equipe/{equipe}
    public function voirEquipeAction($equipes){
    	//chemin vers le répertoire des fichiers TXT provenant d'SAP.
    	$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
    	 
    	/***********************************************************************************************************/
    	$page_global = "";
    	$file_global = fopen($url."alpha_var.txt","r"); //lecture du fichier
    	while (!feof($file_global)) //on parcourt toutes les lignes
    	{
    		$page_global .= fgets($file_global, 4096)."<br>";  // lecture du contenu de la ligne
    	}
    	fclose($file_global);
    	 
    	$arr1 = explode(":",$page_global); //premiere division de la ligne
    	 
    	$objectif_global = substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " ")); //Récupération de l'objectif
    	$realise_global  = ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6)); //récupération du réalisé
    	if (count($arr1)> 3) {
	    	$dossierDAM=(int)ltrim($arr1[4]);
	    	$objdossierDAM=(int)trim($arr1[3]);
    	} else {
    		$dossierDAM=0;
    		$objdossierDAM=0;   		
    	}
    	
    	$neg_global = stripos($realise_global, "-");
    	if ($neg_global != false)
    	{
    		$arr2 = explode("-",$realise_global);
    		$realise_global = $arr2[0]* - 1;
    	}
    	$pourcentage_global = round((($realise_global/$objectif_global) * 100), 0);
    	$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
    	 
    	if ($equipes=='sedentaires'){
			$data = array(
					array('titre'=> 'Sédentaire Europe 1', 'equipe' =>'sedentaireeurope1'),
					array('titre'=> 'Sédentaire Europe 2', 'equipe' =>'sedentaireeurope2'),
					array('titre'=> 'Sédentaire Europe 3', 'equipe' =>'sedentaireeurope3'),
					array('titre'=> 'Sédentaire Europe 4', 'equipe' =>'sedentaireeurope4'),
					array('titre'=> 'Sédentaire Europe 5', 'equipe' =>'sedentaireeurope5'),
					array('titre'=> 'Sédentaire France', 'equipe' =>'sedentairefrance'),
					array('titre'=> 'Sédentaire Belgique', 'equipe' =>'sedentairebelgique'),
					array('titre'=> 'Sédentaire Grand Compte', 'equipe' =>'sedentairegc'),
					array('titre'=> 'Sédentaire Textile', 'equipe' =>'sedentairetextile'),
					array('titre'=> 'Sédentaire Mogador', 'equipe' =>'sedentairemogador'),
			);
    	} else if ($equipes=='terrains'){
    		$data = array(
    				array('titre'=> 'Terrain Royaume-Unis', 'equipe' =>'teruk'),
    				array('titre'=> 'Terrain Allemagne', 'equipe' =>'terallemagne'),
    				array('titre'=> 'Terrain Espagne', 'equipe' =>'terespagne'),
    				array('titre'=> 'Terrain Italie', 'equipe' =>'teritalie'),
    				array('titre'=> 'France Est', 'equipe' =>'franceest'),
    				array('titre'=> 'France Ouest', 'equipe' =>'franceouest'),
    				array('titre'=> 'France Sud', 'equipe' =>'francesud'),  				
    		);    		
    	} else if ($equipes=='roumanie'){
    		$data = array(
    				array('titre'=> 'Sédentaire Allemagne', 'equipe' =>'sedallemagne_bv'),
    				array('titre'=> 'Sédentaire Allemagne', 'equipe' =>'sedallemagne_kt'),
    				array('titre'=> 'Sédentaire Coach Dare', 'equipe' =>'sedcoachdare'),
    				array('titre'=> 'Sédentaire Danemark', 'equipe' =>'seddanemark_is'),
    				array('titre'=> 'Sédentaire Europe de l\'Est', 'equipe' =>'sedeuropeest_is'),
    				array('titre'=> 'Sédentaire Hongrie', 'equipe' =>'sedhongrie_bv'),
    				array('titre'=> 'Sédentaire Méditerranée BV', 'equipe' =>'sedmediterrane_bv'),
    				array('titre'=> 'Sédentaire Méditerranée CT', 'equipe' =>'sedmediterrane_ct'),
    				array('titre'=> 'Sédentaire Méditerranée IS', 'equipe' =>'sedmediterrane_is'),
    				array('titre'=> 'Sédentaire Norvege IS', 'equipe' =>'sednorvege_is'),
    				array('titre'=> 'Sédentaire Roumanie', 'equipe' =>'sedroumanie_is'),	
    				array('titre'=> 'Sédentaire Turquie 1', 'equipe' =>'sedturquie1_ct'),
    				array('titre'=> 'Sédentaire Turquie 2', 'equipe' =>'sedturquie2_ct'),
    		);
    	}
    	$realise_tt =0;
    	$objectif_tt =0;
    	$total_objectif_dossier_equipes= 0;
    	$total_dossier_deposes_equipes= 0;
    	$liste_equipe=array();
    	foreach ($data as $equipe){
	    	$page = "";
	    	$fp = fopen($url.$equipe['equipe'].".txt","r"); //lecture du fichier
	    	while (!feof($fp)) { //on parcourt toutes les lignes
	    		$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
	    	}
	    	fclose($fp);
	    	$arr2 = explode(":",$page); //premiere division de la ligne
	    	$objectif_eq = substr(ltrim($arr2[1]),0,stripos(ltrim($arr2[1]), " "));
	    	$realise_eq = substr(ltrim($arr2[2]),0,stripos(ltrim($arr2[2]), " "));
	    	$neg = stripos($realise_eq, "-");
	    	//echo $neg;
	    	if ($neg != false)
	    	{
	    		$arr2 = explode("-",$realise_eq);
	    		$realise_eq = $arr2[0]*-1;
	    	}
	    	$realise_tt= $realise_tt + $realise_eq;
	    	$objectif_tt = $objectif_tt + $objectif_eq;
	    		    	
	    	//récupération des dossiers
	    	$page = "";
	    	$page = file($url."acheteurs_".$equipe['equipe'].".txt");
	    	$total_dossier_deposes=0;
	    	$total_objectif_dossier=0;
	    	//pour toutes lignes du tableaux
	    	foreach ($page as $line_num => $line) {
	    		// création d'un tableau
	    		$arr1 = explode(":",$line);
	    		if (count($arr1) == 5) {
	    			$dossier_objectif=(int)ltrim($arr1[3]);
	    			$total_objectif_dossier= $total_objectif_dossier +  $dossier_objectif;
	    			$dossier_deposses=(int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
	    			$total_dossier_deposes= $total_dossier_deposes+ $dossier_deposses;	    			
	    		} //fin if
	    	}//fin foreach
	    	if ($objectif_eq){
	    		$pourcentage = round((($realise_eq/$objectif_eq) *100),0);
	    	}else {
	    		$pourcentage = 0;
	    	}
	    	$liste_equipe[] = array(
	    			'titre'=> $equipe['titre'],
	    			'equipe' =>$equipe['equipe'],
	    			'objectif'=>$objectif_eq,
	    			'realise'=>$realise_eq,
	    			'dossier_deposes'=>$total_dossier_deposes,
	    			'dossier_objectif'=>$total_objectif_dossier,
	    			'pourcentage'=>$pourcentage,
	    			'equipes'=>$equipes
	    	);
	    	$total_objectif_dossier_equipes= $total_objectif_dossier_equipes +  $total_objectif_dossier;
	    	$total_dossier_deposes_equipes= $total_dossier_deposes_equipes+ $total_dossier_deposes;	    	
    	}
    	// Trie du tableau
    	foreach ($liste_equipe as $key => $row) {
    		$rang[$key]  = $row['pourcentage'];
    	}
    	array_multisort($rang, SORT_DESC, $liste_equipe);
    	
    	$pourcentage_tt = round((($realise_tt/$objectif_tt) *100),0);
    	return $this->render('IndicateursDAMBundle:Equipe:equipeGeneral.html.twig',array(
    			'titre' => $equipes,
    			'realise_global' => $realise_global,
    			'objectif_global' => $objectif_global,
    			'pourcentage_global' => $pourcentage_global,
    			'dossierdam' => $dossierDAM,
    			'objdossierdam' => $objdossierDAM,
    			'realise_tt' => $realise_tt,
    			'objectif_tt' => $objectif_tt,
    			'pourcentage_tt' => $pourcentage_tt,
    			'total_dossier_deposes' => $total_dossier_deposes_equipes,
    			'total_objectif_dossier' => $total_objectif_dossier_equipes,
    			'listeEquipe' => $liste_equipe
		));
    }
    
    
    
    public function voir($titre, $equipe){
    	//chemin vers le répertoire des fichiers TXT provenant d'SAP.
    	$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
    	
    	/***********************************************************************************************************/
    	$page_global = "";
    	$file_global = fopen($url."alpha_var.txt","r"); //lecture du fichier
    	while (!feof($file_global)) //on parcourt toutes les lignes
    	{
    		$page_global .= fgets($file_global, 4096)."<br>";  // lecture du contenu de la ligne
    	}
    	fclose($file_global);
    	
    	$arr1 = explode(":",$page_global); //premiere division de la ligne
    	
    	$objectif_global = substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " ")); //Récupération de l'objectif
    	$realise_global  = ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6)); //récupération du réalisé
    	$objdossierDAM=(int)ltrim($arr1[3]);
    	$dossierDAM=(int)trim($arr1[4]);
    	
    	$neg_global = stripos($realise_global, "-");
    	if ($neg_global != false)
    	{
    		$arr2 = explode("-",$realise_global);
    		$realise_global = $arr2[0]* - 1;
    	}
    	$pourcentage_global = round((($realise_global/$objectif_global) * 100), 0);
    	
    	/***********************************************************************************************************/
    	
    	$page = "";
    	$fp = fopen($url.$equipe.".txt","r"); //lecture du fichier
    	while (!feof($fp)) { //on parcourt toutes les lignes
    		$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
    	}
    	fclose($fp);
    	$arr2 = explode(":",$page); //premiere division de la ligne
    	$objectif_eq = substr(ltrim($arr2[1]),0,stripos(ltrim($arr2[1]), " "));
    	
    	$realise_eq = substr(ltrim($arr2[2]),0,stripos(ltrim($arr2[2]), " "));
    	$neg = stripos($realise_eq, "-");
    	//echo $neg;
    	if ($neg != false)
    	{
    		$arr2 = explode("-",$realise_eq);
    		$realise_eq = $arr2[0]*-1;
    	}

    	if ($objectif_eq>0){
    	$pourcentage_eq = round((($realise_eq/$objectif_eq) *100),0);
    	} else {
    		$pourcentage_eq=0;
    	}
    	/***********************************************************************************************************/
    	
    	//lecture du fichier + création d'un tableau des lignes de celui ci
    	$lines = file($url."acheteurs_".$equipe.".txt");
    	$total_dossier_deposes=0;
    	$total_objectif_dossier=0;
    	$listeAcheteurs = array();
    	//pour toutes lignes du tableaux
    	foreach ($lines as $line_num => $line) {
    		
    		// création d'un tableau
    		$arr1 = explode(":",$line);
    		
    		if (count($arr1) == 5) {
    			//on ne garde que les 20 premier caractère + on supprime les espaces en début et fin de lignes
    			$nom = trim(substr($arr1[0],0,19));
    	
    			//création d'un tableau, pour le nom
    			$nom_detail = explode(" ",$nom);
    			// si le début du nom commence par le préfixe 'ACH'
    			if ($nom_detail[0] == "ACH") {
    				//on supprime ce préfixe
    				unset($nom_detail[0]);
    				//récupération de la dernière valeur du tableau (prénom)
    				$prenom = end($nom_detail);
    				//on ne garde que la première lettre
    				$prenom = substr($prenom,0,1).".";
    				//on supprime le prénom
    				unset($nom_detail[sizeof($nom_detail)]);
    				//on rajoute la première lettre
    				$nom_detail[] = $prenom;
    			}
    			else {
    				//récupération de la dernière valeur du tableau (prénom)
    				$prenom = end($nom_detail);
    				//on ne garde que la première lettre
    				$prenom = substr($prenom,0,1).".";
    				//on supprime le prénom
    				unset($nom_detail[sizeof($nom_detail)-1]);
    				//on rajoute la première lettre
    				$nom_detail[] = $prenom;
    			}
    			//on rassemble nom + prénom
    			$nom = implode(" ",$nom_detail);
    			//récupération de l'objectif
    			$objectif_indiv=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
    			//récupération du réalisé
    			$realise_indiv=substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
    			//récupération du nombre de dossier déposé   
    			$nb_dossier_obj = (int)ltrim($arr1[3]);
    			$total_objectif_dossier= $nb_dossier_obj + $total_objectif_dossier;
    			//récupération de l'objectif de dossiers à déposer
    			$nb_dossier_depose = (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
    			$total_dossier_deposes=$total_dossier_deposes + $nb_dossier_depose;
    			//Recherche s'il y a un nombre négatif
    			$neg=stripos($realise_indiv, "-");
    			//s'il y en a
    			if ($neg == true)  {
    				//on ne garde que le nombre à droite du signe "-"
    				$arr2 = explode("-",$realise_indiv);
    				$realise_indiv = $arr2[0]* -1;
    			}
    			//passage en float
    			$realise_indiv = (double)$realise_indiv;
    	
    			// ajout des valeurs dans des tableaux spécifiques
    			$noms[] 				= $nom;
    			$objs[] 				= $objectif_indiv;
    			$realisations[]         = $realise_indiv;
    			$nbdossier[]			= $nb_dossier_depose;
    			$dossierobj[]			= $nb_dossier_obj;    			 
    			
    			//création du pourcentage de réalisation
    			if ($objectif_indiv != 0) {
    				$pourcentages[] = number_format(($realise_indiv/$objectif_indiv*100),0, '.', '');
    			}
    			else {
    				$pourcentages[] = number_format($objectif_indiv,0);
    			}
    			//tri des tableaux en fonction du pourcentage (premier tableau de donner) par ordre décroissant
    			array_multisort($pourcentages, SORT_DESC, $realisations, $objs, SORT_DESC, $noms,$nbdossier,$dossierobj);
    			for($i=0;$i<count($noms); $i++) {
    				$listeAcheteurs[$i]['nom'] = $noms[$i];
    				$listeAcheteurs[$i]['objectif'] = $objs[$i];
    				$listeAcheteurs[$i]['realise'] = $realisations[$i];
    				$listeAcheteurs[$i]['pourcentage'] = $pourcentages[$i];
    				$listeAcheteurs[$i]['dossiers'] = $nbdossier[$i];
    				$listeAcheteurs[$i]['dossiersobj'] = $dossierobj[$i];
    			}
    		} //fin if
    	}//fin foreach
    	
    	//regroupement des tableaux en un seul, pour l'affichage
   	
    	
    	return $this->render('IndicateursDAMBundle:Equipe:equipe.html.twig',array(
    			'equipe' => $equipe,
    			'realise_global' => $realise_global,
    			'objectif_global' => $objectif_global,
    			'pourcentage_global' => $pourcentage_global,
    			'total_dossier' => $total_dossier_deposes,
    			'obj_dossiers' => $total_objectif_dossier,
    			'realise_eq' => $realise_eq,
    			'objectif_eq' => $objectif_eq,
    			'pourcentage_eq' => $pourcentage_eq,
    			'dossierdam' => $dossierDAM,
    			'objdossierdam' => $objdossierDAM,    	
    			'listeAcheteurs' => $listeAcheteurs,
    			'titre' => $titre
    	
    	));
    }
    
    
    //page d'acceuil indicateurs/dam/equipe/acceuil
    public function accueilAction(){
    	//chemin vers le répertoire des fichiers TXT provenant d'SAP.
    	$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
    
    	/***********************************************************************************************************/
    	$page_global = "";
    	$file_global = fopen($url."alpha_var.txt","r"); //lecture du fichier
    	while (!feof($file_global)) //on parcourt toutes les lignes
    	{
    		$page_global .= fgets($file_global, 4096)."<br>";  // lecture du contenu de la ligne
    	}
    	fclose($file_global);
    
    	$arr1 = explode(":",$page_global); //premiere division de la ligne
    
    	$objectif_global = substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " ")); //Récupération de l'objectif
    	$realise_global  = ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6)); //récupération du réalisé
    	if (count($arr1)> 3) {
    		$dossierDAM=(int)ltrim($arr1[4]);
    		$objdossierDAM=(int)trim($arr1[3]);
    	} else {
    		$dossierDAM=0;
    		$objdossierDAM=0;
    	}
    
    	$neg_global = stripos($realise_global, "-");
    	if ($neg_global != false)
    	{
    		$arr2 = explode("-",$realise_global);
    		$realise_global = $arr2[0]* - 1;
    	}
    	$pourcentage_global = round((($realise_global/$objectif_global) * 100), 0);
    	$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";

     	return $this->render('IndicateursDAMBundle:Equipe:acceuil.html.twig',array(
    			'realise_global' => $realise_global,
    			'objectif_global' => $objectif_global,
    			'pourcentage_global' => $pourcentage_global,
    			'dossierdam' => $dossierDAM,
    			'objdossierdam' => $objdossierDAM,
    	));
    }    
}
