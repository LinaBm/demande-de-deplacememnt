<?php
namespace Direction\DirectionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ApplicationType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('direction', 'entity', array(
				'em' =>'archi',
				'class'    => 'DirectionDirectionBundle:DirectionDir',
				'property' => 'dirLibelle',
				'empty_value' => '',
				'mapped' => false,
		))
		 
		->add('service', 'entity', array(
				'em' =>'archi',
				'class'    => 'DirectionDirectionBundle:ServiceSrv',
				'property' => 'srvLibelle',
				'empty_value' => '',
				'mapped' => false,
		))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Direction\DirectionBundle\Entity\ServiceSrv'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'direction_directionbundle_servicesrv';
	}
}
